package com.drivermshwar;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
// import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.facebook.react.ReactApplication;
import com.github.yamill.orientation.OrientationPackage;
import com.kishanjvaghela.cardview.RNCardViewPackage;
import com.zmxv.RNSound.RNSoundPackage;
import com.horcrux.svg.SvgPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.transistorsoft.rnbackgroundfetch.RNBackgroundFetchPackage;
// import com.imagepicker.ImagePickerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.learnium.RNDeviceInfo.RNDeviceInfo;



import io.fabric.sdk.android.Fabric;
import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new RNBackgroundFetchPackage(),
          new MainReactPackage(),
          new OrientationPackage(),
          new RNCardViewPackage(),
          new RNSoundPackage(),
          new SvgPackage(),
          new PickerPackage(),
          new VectorIconsPackage(),
          new MapsPackage(),
          new RNDeviceInfo()
          // new ImagePickerPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Fabric.with(this, new Crashlytics());
    // , new CrashlyticsNdk());
    SoLoader.init(this, /* native exopackage */ false);
  }
}
