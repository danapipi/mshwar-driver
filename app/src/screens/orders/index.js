/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-10-29 12:34:17
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-27 16:17:40
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  Dimensions,
  Platform,
  Alert,
  StyleSheet,
  Easing,
  Animated,
  Vibration
} from "react-native";
import MapView, { Marker } from "react-native-maps";
import Assets from "../../../assets";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h
} from "../../../config/layout";
// import { STATUS_ORDER } from "../../../config"
import CountdownCircle from "react-native-countdown-circle";
import {
  AnimatedCircularProgress,
  CircularProgress
} from "react-native-circular-progress";
import Slider from "react-native-slider";
import Permissions from "react-native-permissions";
import { themes } from "../../../config/themes";
import { getData, removeData } from "../../../utils/storage";
import { keys } from "../../../config/keys";
import { name as appName } from "../../../../app/app.json";
import Sound from "react-native-sound";
import Orientation from 'react-native-orientation';


const { width, height } = Dimensions.get("window");
const LATITUDE_DELTA = 0.0222;
const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);
const map = null;
const audio = new Sound("short_tone.mp3", Sound.MAIN_BUNDLE, error => {
  if (error) {
    console.log("failed to load the sound", error);
    return;
  }
  // loaded successfully
  console.log(
    "duration in seconds: " +
      audio.getDuration() +
      "number of channels: " +
      audio.getNumberOfChannels()
  );
});
const PATTERN = [1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000];

export default class OrderScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: {},
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0
      },
      markerPosition: {
        latitude: 0,
        longitude: 0
      },
      statusDriver: "inactive",
      location: "",
      sliderMinValue: 3,
      sliderMaxValue: 20,
      labelSlide: "SWIPE TO ACCEPT ORDER",
      fill: 100,
      audio: true
    };
  }

  _labelChange = value => {
    if (this.state.labelSlide.length > value) {
      const string = this.state.labelSlide;
      const changeLabel = string.slice(
        Math.floor(value) + 3,
        this.state.labelSlide.length
      );
      this.setState({ labelSlide: changeLabel });
      console.log("label", changeLabel);
      console.log("TES", value, this.state.labelSlide.length);
    }
    // else {
    //   this.setState({ labelSlide: "SWIPE TO ACCEPT ORDER" });
    // }
  };

  _valueChange = value => {
    console.log("value", value, this.state.labelSlide.length);
    if (value === this.state.sliderMaxValue) {
      const params = {
        status: "accept"
      };
      this.props.actions.swipeStatusOrder(
        params,
        this._swipeStatusOrderSuccess,
        this._swipeStatusOrderFailed
      );
      this.setState({
        labelSlide: "SWIPE TO ACCEPT ORDER"
      });
    } else {
      this.setState({
        labelSlide: ""
      });
      this.props.actions.swipeStatusOrderUpdate(Math.floor(value));
    }
  };

  _completeChange = value => {
    if (Math.floor(value) < this.state.sliderMaxValue) {
      this.setState({
        labelSlide: "SWIPE TO ACCEPT ORDER"
      });
      this.props.actions.swipeStatusOrderUpdate(this.state.sliderMinValue);
    }
    Vibration.cancel();
  };

  _swipeStatusOrderSuccess = data => {
    console.log("_swipeStatusOrderSuccess", data);
    this.props.actions.swipeStatusOrderUpdate(this.state.sliderMaxValue);
    try {
      getData(keys.activeOrder).then(res => {
        console.log("res", res);
        if (res !== "") {
          // if empty order_id in storage
          this.props.actions.fetchOrderbyId(
            res,
            this._fetchOrderByIdSuccess,
            this._fetchOrderByIdFailed
          );
        } else {
          Alert.alert(
            appName,
            'Order has been canceled order',
            [{ text: "OK", onPress: () => {
              const { navigate } = this.props.navigation;
              navigate("Home");
            } }],
            { cancelable: false }
          );
        }
      });
    } catch (error) {
      console.log("get data storage :", error);
    }
  };

  _swipeStatusOrderFailed = error => {
    console.log("_swipeStatusOrderFailed", error);
    this.props.actions.swipeStatusOrderUpdate(this.state.sliderMinValue);
  };

  _fetchOrderByIdSuccess = data => {
    const { navigate } = this.props.navigation;
    if(Object.keys(data).length === 0) { // if empty object
      console.log('data _fetchOrderByIdSuccess : ', 'empty');
      Alert.alert(
        appName,
        'Order has been canceled',
        [{ text: "OK", onPress: () => {
          navigate("Home");
        } }],
        { cancelable: false }
      );
    } else {
      console.log('data _fetchOrderByIdSuccess : ', data);
      navigate("OrderDetail");
      this.props.actions.swipeStatusOrderUpdate(this.state.sliderMinValue);
    }
  };

  _fetchOrderByIdFailed = error => {
    try {
      removeData(keys.activeOrder).then(res => {
        Alert.alert(
          appName,
          'Order has been canceled',
          [{ text: "OK", onPress: () => {
            const { navigate } = this.props.navigation;
            navigate("Home");
          } }],
          { cancelable: false }
        );
      });
    } catch (error) {
      console.log("error remove storage", error);
    }
  };

  // _getCurrentPosition() {
  //   try {
  //     navigator.geolocation.getCurrentPosition(
  //       position => {
  //         // console.log('Position current position : ',position)
  //         var lat = parseFloat(position.coords.latitude);
  //         var long = parseFloat(position.coords.longitude);

  //         var initialRegion = {
  //           latitude: lat,
  //           longitude: long,
  //           latitudeDelta: LATITUDE_DELTA,
  //           longitudeDelta: LONGITUDE_DELTA
  //         };

  //         this.setState({ initialPosition: initialRegion });
  //         this.setState({ markerPosition: initialRegion });
  //       },
  //       error => {
  //         // console.log('Error current position : ',error)
  //         switch (error.code) {
  //           case 1:
  //             if (Platform.OS === "ios") {
  //               Alert.alert("", "Permite apps to access your location");
  //             } else {
  //               Alert.alert("", "Permite apps to access your location");
  //             }
  //             break;
  //           default:
  //             Alert.alert(JSON.stringify(error));
  //         }
  //       },
  //       { enabledHighAccuracy: true, timeout: 200000 }
  //     );

  //     // this.watchID = navigator.geolocation.watchPosition(position => {
  //     //   var lat = parseFloat(position.coords.latitude);
  //     //   var long = parseFloat(position.coords.longitude);

  //     //   var lastRegion = {
  //     //     latitude: lat,
  //     //     longitude: long,
  //     //     latitudeDelta: LATITUDE_DELTA,
  //     //     longitudeDelta: LONGITUDE_DELTA
  //     //   };

  //     //   this.setState({ initialPosition: lastRegion });
  //     //   this.setState({ markerPosition: lastRegion });
  //     // });
  //   } catch (e) {
  //     alert(e.message || "");
  //   }
  // }

  async componentDidMount() {
    Orientation.lockToPortrait();

    this.props.actions.swipeStatusOrderUpdate(this.state.sliderMinValue);
    this.circularProgress.animate(100, 10000, Easing.quad);
    Vibration.vibrate(PATTERN);
    Permissions.request("location", {
      rationale: {
        title: "Location Permission",
        message:
          "There was a problem getting your permission. " +
          "Please enable it from settings."
      }
    }).then(response => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      // this._getCurrentPosition();
      this.setState({ location: response });
    });
    // await setTimeout(this._navigateToHome(),10000)

    try {
      getData(keys.activeOrder).then(res => {
        console.log("Storage active order finish : ", res);
        if (res !== "") {
          const result = JSON.parse(res);
          if (result) {
            console.log("Storage active order finish inside : ", result);
          }
        }
      });
    } catch (error) {
      console.log("Error get data storage : ", JSON.stringify(error));
    }
  }

  _navigateToHome = () => {
    const params = {
      message: "decline"
    };
    this.props.actions.clickToCancelOrder(
      params,
      this._clickToCancelOrderSuccess,
      this._clickToCancelOrderFailed
    );
  };

  _clickToCancelOrderSuccess = data => {
    console.log("decline", data);
    try {
      removeData(keys.activeOrder).then(res => {
        if (res) {
          const { navigate } = this.props.navigation;
          navigate("Home");
        }
      });
    } catch (error) {
      console.log("error remove storage", error);
    }
  };

  _clickToCancelOrderFailed = error => {
    console.log("Error _clickToCancelOrderFailed : ", error);
  };

  _playSound = () => {
    audio.play();
  };

  _stopSound = () => {
    audio.stop();
  };

  render() {
    // console.log("sound");
    {
      this.state.audio ? this._playSound() : this._stopSound();
    }

    if(Object.keys(this.props.orders).length == 0) {
      try {
        removeData(keys.activeOrder).then(res => {
          if (res) {
            const { navigate } = this.props.navigation;
            navigate("Home");
          }
        });
      } catch (error) {
        console.log("error remove storage", error);
      }

      return null;
    } else {
      return (
        <View
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            alignItems: "center"
          }}
        >
          <StatusBar hidden={true} />
          <MapView
            ref={map => (this.map = map)}
            region={{
              latitude: parseFloat(this.props.location.lat),
              longitude: parseFloat(this.props.location.lng),
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA
            }}
            showsMyLocationButton={false}
            style={styles.maps}
            zoomEnabled={true}
            loadingEnabled={true}
            loadingIndicatorColor="#606060"
            loadingBackgroundColor="#FFFFFF"
            moveOnMarkerPress={false}
            showsUserLocation={false}
            followUserLocation={
              false
            } /* This only works if showsUserLocation is true */
            showsCompass={true}
            showsPointsOfInterest={false}
            showsScale={true}
            minZoomLevel={15}
            maxZoomLevel={20}
            provider="google"
          >
            <MapView.Marker
              image={Assets.pin}
              key="1"
              title=""
              description=""
              coordinate={{
                latitude: parseFloat(this.props.location.lat),
                longitude: parseFloat(this.props.location.lng),
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
              }}
            />
          </MapView>
          <ImageBackground
            source={Assets.background}
            // style={{ width: wp("100%"), height: hp("100%") }}
            style={{
              flex: 1,
              width: width
            }}
          >
            <View style={{
                    paddingTop: Platform.OS === 'ios' ? 24 : 0,
            }}>
              <TouchableOpacity
                style={{ flexDirection: "row", justifyContent: "flex-end" }}
                onPress={() => {
                  this._navigateToHome();
                  this.setState({ audio: false });
                  Vibration.cancel();
                }}
              >
                <Text
                  style={{
                    marginTop: hp(h(20)),
                    marginRight: wp(w(10)),
                    fontFamily: "Helvetica-Light",
                    fontSize: hp(h(12)),
                    color: themes.white
                  }}
                >
                  DECLINE
              </Text>
                <Image
                  source={Assets.cancelLogin}
                  style={{
                    height: 24,
                    width: 24,
                    marginTop: hp(h(17)),
                    marginRight: wp(w(13))
                  }}
                />
              </TouchableOpacity>
              {/* <Text
              style={{
                fontFamily: "Helvetica-Bold",
                fontSize: hp(h(12)),
                color: themes.red,
                justifyContent: "flex-end",
                textAlign: "right",
                marginRight: wp(w(45))
              }}
            >
              2 ORDERS DECLINED
            </Text> */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: hp(h(35))
                }}
              >
                <Image
                  source={Assets.photoDriver}
                  style={{ width: 39, height: 39 }}
                />
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: hp(h(24)),
                    color: themes.white,
                    marginLeft: wp(w(15))
                  }}
                >
                  {this.props.orders.name}
                </Text>
              </View>
              <View
                style={{
                  marginTop: hp(h(17)),
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: hp(h(18)),
                    color: themes.white
                  }}
                >
                  A customer looking for
              </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: hp(h(18)),
                    color: themes.white
                  }}
                >
                  Food Delivery
              </Text>
              </View>
              <View
                style={{
                  // paddingBottom: hp(h(35))
                  paddingBottom: hp(h(15))
  
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: hp(h(34)),
                    alignItems: "center",
                    marginLeft: wp(w(60))
                  }}
                >
                  <View style={{ flexDirection: "column" }}>
                    <Image
                      source={Assets.restoSymbol}
                      style={{ width: 14, height: 14, marginTop: hp(h(10)) }}
                    />
                    <Image
                      source={Assets.line}
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        marginTop: hp(h(5)),
                        marginLeft: wp(w(5)),
                        // height: hp(h(30)),
                        marginBottom: hp(h(5))
                      }}
                    />
                    <Image
                      source={Assets.location}
                      style={{
                        tintColor: "white",
                        width: 14,
                        height: 14,
                        // marginTop: hp(h(5)),
                        marginBottom: hp(h(55))
                      }}
                    />
                  </View>
                  <View style={{ flexDirection: "column", marginLeft: wp(w(20)), height: hp(h(130)) }}>
                    <Text
                      style={{
                        fontFamily: "Roboto-Medium",
                        fontSize: 18,
                        color: themes.white,
                      }}
                    >
                      {this.props.orders.merchant_data.name}
                    </Text>
                    <Text
                      style={{
                        fontFamily: "Helvetica-Light",
                        fontSize: hp(h(14)),
                        color: themes.white,
                        marginRight: wp(w(20))
                      }}
                    >
                      {this.props.orders.merchant_data.address}
                    </Text>
                    <View style={{ flex: 1, 
                      }}>
                      <Text
                        style={{
                          fontFamily: "Roboto-Medium",
                          fontSize: hp(h(18)),
                          color: themes.white,
                          marginTop: hp(h(13)),
                          marginRight: wp(w(40))
                        }}
                      >
                        {this.props.orders.address}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={{ marginTop: hp(h(20)) }}>
                  <View
                    style={{
                      backgroundColor: themes.white,
                      borderRadius: 15,
                      // width: wp(w(78)),
                      position: "absolute",
                      left: 0,
                      bottom: 0,
                      paddingHorizontal: wp(w(10)),
                      marginTop: hp(h(17)),
                      marginLeft: wp(w(60))
                    }}
                  >
                    <Text
                      style={{ color: "rgb(255,101,120)", textAlign: "center" }}
                    >
                      {this.props.orders.distance}
                      miles
                  </Text>
                  </View>
                </View>
              </View>
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: hp(h(10))
                }}
              >
                {/* <CountdownCircle
                seconds={10}
                radius={60}
                borderWidth={8}
                color="transparent"
                bgColor="rgb(255,101,120)"
                textStyle={{ fontSize: 50, color: "rgb(255,255,255)" }}
                shadowColor="rgb(255,255,255)"
              /> */}
                <AnimatedCircularProgress
                  ref={ref => (this.circularProgress = ref)}
                  size={hp(h(140))}
                  width={12}
                  fill={this.state.fill}
                  // prefill={100}
                  tintColor={themes.white}
                  onAnimationComplete={() => {
                    // this.setState({ audio: false });
                    console.log("onAnimationComplete");
                  }}
                  duration={1000}
  
                // backgroundColor={themes.mainColor}
                >
                  {fill => (
                    <View
                      style={{
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text
                        style={{
                          color: themes.white,
                          fontWeight: "bold"
                        }}
                      >
                        {Math.floor(fill / 10)}
                      </Text>
                      <Text
                        style={{
                          color: themes.white,
                          fontWeight: "bold"
                        }}
                      >
                        SECONDS
                    </Text>
                    </View>
                  )}
                </AnimatedCircularProgress>
              </View>
              <View
                style={{
                  marginTop: hp(h(41)),
                  marginHorizontal: wp(w(34)),
                  backgroundColor: themes.white,
                  borderRadius: 15
                  // height: hp(h(50)),
                }}
              >
                <Slider
                  value={this.props.valueSliderAccept}
                  thumbImage={Assets.swipeArrow}
                  thumbStyle={{ width: 58, height: 39, marginVertical: hp(h(10)) }}
                  thumbTintColor="transparent"
                  minimumTrackTintColor="transparent"
                  maximumTrackTintColor="transparent"
                  minimumValue={this.state.sliderMinValue}
                  maximumValue={this.state.sliderMaxValue}
                  onValueChange={value => {
                    // console.log('value accept :', value);
                    this._valueChange(value);
                    // this._labelChange(value);
                  }}
                  onSlidingComplete={value => {
                    this._completeChange(value);
                    this.setState({ audio: false });
                  }}
                  style={{
                    marginVertical: hp(h(5)),
                    marginHorizontal: wp(w(5)),
                    zIndex: 10
                  }}
                />
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    height: "100%",
                    position: "absolute",
                    width: wp(w(280)),
                    right: 0
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Helvetica-Light",
                      fontSize: hp(h(12)),
                      color: themes.gray_70
                    }}
                  >
                    {this.state.labelSlide}
                  </Text>
                </View>
              </View>
              <View>
                <Text
                  style={{
                    fontFamily: "Roboto-Regular",
                    fontSize: hp(h(13)),
                    color: themes.white,
                    marginTop: hp(h(13)),
                    textAlign: "center"
                  }}
                >
                  Transaction Number : {this.props.orders.id}
                </Text>
              </View>
            </View>
          </ImageBackground>
        </View>
      );
    }
    
    
  }
}

const styles = StyleSheet.create({
  maps: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1
  }
});
