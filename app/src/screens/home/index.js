/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-10-29 12:34:17
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-30 14:35:25
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Platform,
  Alert
} from "react-native";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import Assets from "../../../assets";
import IonIcon from "react-native-vector-icons/Ionicons";
import Permissions from "react-native-permissions";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h,
  isIphoneX
} from "../../../config/layout";
import { themes } from "../../../config/themes";
import DriverStatus from "../../../components/DriverBar";
import { name as appName } from "../../../../app/app.json";
import { setData, getData, removeData } from "../../../utils/storage";
import { keys } from "../../../config/keys";
import Sound from "react-native-sound";
import BackgroundFetch from "react-native-background-fetch";
import Orientation from "react-native-orientation";

const { width, height } = Dimensions.get("window");
const LATITUDE_DELTA = 0.0222;
const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);
const map = null;

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: {},
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0
      },
      markerPosition: {
        latitude: 0,
        longitude: 0
      },
      currentAddress: null,
      statusDriver: "inactive",
      location: "",
      sliderMinValue: 3
    };

    this.watchID = null;
  }

  _homeBtn = () => {
    const { navigate } = this.props.navigation;
    navigate("HomeTab");
  };

  _clickToProfile = () => {
    this._clearWatchPosition();
    const { navigate } = this.props.navigation;
    navigate("Profile");
  };

  _getCurrentPosition() {
    try {
      navigator.geolocation.getCurrentPosition(
        position => {
          // console.log('Position current position : ',position)
          var lat = parseFloat(position.coords.latitude);
          var long = parseFloat(position.coords.longitude);

          var initialRegion = {
            latitude: lat,
            longitude: long,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          };

          this.setState({ initialPosition: initialRegion });
          this.setState({ markerPosition: initialRegion });
        },
        error => {
          // console.log('Error current position : ',error)
          switch (error.code) {
            case 1:
              if (Platform.OS === "ios") {
                Alert.alert("", "Permite apps to access your location");
              } else {
                Alert.alert("", "Permite apps to access your location");
              }
              break;
            default:
              Alert.alert(JSON.stringify(error));
          }
        },
        { enabledHighAccuracy: true, timeout: 200000 }
      );
    } catch (e) {
      alert(e.message || "");
    }
  }

  _watchPosition = () => {
    var options = {
      // timeout: 5000
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
      distanceFilter: 0
    };

    this.watchID = navigator.geolocation.watchPosition(
      this._showLocation,
      this._errorLocation,
      options
    );
  };

  _clearWatchPosition = () => {
    navigator.geolocation.clearWatch(this.watchID); // clear watch position after cancel
  };

  _showLocation = position => {
    var lat = parseFloat(position.coords.latitude);
    var long = parseFloat(position.coords.longitude);

    const params = {
      lat: lat,
      lng: long
    };
    this.props.actions.watchLocation(
      params,
      this._watchLocationSuccess,
      this._watchLocationError
    );

    var lastRegion = {
      latitude: lat,
      longitude: long,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA
    };

    this.setState({ initialPosition: lastRegion });
    this.setState({ markerPosition: lastRegion });
  };

  _errorLocation = error => {
    if (error.code == 1) {
      Alert.alert(appName, "Access is denied!");
    } else if (error.code == 2) {
      Alert.alert(appName, "Position is unavailable!");
    }
  };

  _soundOrder = () => {
    var audio = new Sound("short_tone.mp3", Sound.MAIN_BUNDLE, error => {
      if (error) {
        console.log("failed to load the sound", error);
        return;
      }
      // loaded successfully
      console.log(
        "duration in seconds: " +
          audio.getDuration() +
          "number of channels: " +
          audio.getNumberOfChannels()
      );
      audio.play();
    });
  };

  _watchLocationSuccess = data => {
    // Alert.alert(appName, 'Location has been changed');
    const active_order = data.active_order; // comment this line for testing
    // const active_order = 17; // for testing purpose
    console.log("active_order", active_order);
    if (active_order !== null) {
      try {
        getData(keys.activeOrder).then(res => {
          console.log("res", res);
          if (res === "") {
            // if empty order_id in storage
            this.props.actions.fetchOrderbyId(
              active_order,
              this._fetchOrderByIdSuccess,
              this._fetchOrderByIdFailed
            );
          } else {
            console.log("res", res);
          }
        });
      } catch (error) {
        console.log("get data storage :", error);
      }
    } else {
      // remove local storage if exist and redirect to home
      try {
        getData(keys.activeOrder).then(res => {
          if (res !== "") {
            try {
              removeData(keys.activeOrder).then(res => {
                if (res) {
                  console.log("order has been canceled");
                }
              });
            } catch (error) {
              Alert.alert(
                appName,
                "Order Failed",
                [
                  {
                    text: "OK",
                    onPress: () => {
                      console.log("order failed");
                    }
                  }
                ],
                { cancelable: false }
              );
            }
          }
        });
      } catch (error) {}
    }
  };

  _watchLocationError = error => {
    console.log("_watchLocationError :", error);
  };

  _fetchOrderByIdSuccess = data => {
    if (Object.keys(data).length > 0) {
      // if exists
      // console.log('_fetchOrderByIdSuccess :', data)

      try {
        getData(keys.activeOrder).then(res => {
          if (res !== "") {
            console.log("ada data di storage : ", res);
            const result = JSON.parse(res);
            if (result) {
              console.log("Order PickUp : ", result);
            }
          } else {
            // if driver haven't order
            try {
              console.log("empty data di storage");
              setData(keys.activeOrder, data.id).then(async res => {
                if (res) {
                  await this._soundOrder();

                  const { navigate } = this.props.navigation;
                  this.props.actions.swipeStatusOrderUpdate(
                    this.state.sliderMinValue
                  );
                  await navigate("Order");
                }
              });
            } catch (err) {
              console.log("Error save data storage : ", JSON.stringify(err));
            }
          }
        });
      } catch (error) {
        console.log("Error get data storage : ", JSON.stringify(error));
      }
    }
  };

  _fetchOrderByIdFailed = error => {
    console.log("_fetchOrderByIdFailed :", error);
  };

  _backgroundFetch = data => {
    
  };

  componentDidMount() {
    Orientation.lockToPortrait();

    Permissions.request("location", {
      rationale: {
        title: "Location Permission",
        message:
          "There was a problem getting your permission. " +
          "Please enable it from settings."
      }
    }).then(response => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      console.log("response", response);

      const params = {
        status: this.props.userInfo.status
      };
      this._getCurrentPosition();
      this._actionSetActive(params);
    });

    // BackgroundFetch.configure({
    //   minimumFetchInterval: 15,
    //   stopOnTerminate: false,
    //   startOnBoot: true,
    //   enableHeadless: true,
    //   forceReload: false
    // }, () => {

    //   console.log("[js] Received background-fetch current position  : initial ");
    //   try {
    //     navigator.geolocation.getCurrentPosition(
    //       position => {
    //         // console.log('Position current position : ',position)
    //         console.log("[js] Received background-fetch current position  : success ", position);
    //         var lat = parseFloat(position.coords.latitude);
    //         var long = parseFloat(position.coords.longitude);
    //         const params = {
    //           lat: lat,
    //           lng: long
    //         };
    //         this.props.actions.watchLocation(
    //           params,
    //           this._watchLocationSuccess,
    //           this._watchLocationError
    //         );
    //       },
    //       error => {
    //         console.log("[js] Received background-fetch current position  : error ", error);
    //       },
    //       { enabledHighAccuracy: true, timeout: 200000 }
    //     );
    //   } catch (e) {
    //     console.log("[js] Received background-fetch current position  : e ", e.message);
    //   }

    //   // Required: Signal completion of your task to native code
    //   // If you fail to do this, the OS can terminate your app
    //   // or assign battery-blame for consuming too much background-time
    //   BackgroundFetch.finish(BackgroundFetch.FETCH_RESULT_NEW_DATA);
    // }, (error) => {
    //   console.log("[js] RNBackgroundFetch failed to start");
    // });

    // Optional: Query the authorization status.
    BackgroundFetch.status(status => {
      switch (status) {
        case BackgroundFetch.STATUS_RESTRICTED:
          console.log("BackgroundFetch restricted");
          break;
        case BackgroundFetch.STATUS_DENIED:
          console.log("BackgroundFetch denied");
          break;
        case BackgroundFetch.STATUS_AVAILABLE:
          console.log("BackgroundFetch is enabled");
          BackgroundFetch.configure(
            {
              minimumFetchInterval: 15,
              stopOnTerminate: false,
              startOnBoot: true,
              enableHeadless: true,
              forceReload: false
            },
            () => {
              console.log(
                "[js] Received background-fetch current position  : initial "
              );
              try {
                navigator.geolocation.getCurrentPosition(
                  position => {
                    // console.log('Position current position : ',position)
                    console.log(
                      "[js] Received background-fetch current position  : success ",
                      position
                    );
                    var lat = parseFloat(position.coords.latitude);
                    var long = parseFloat(position.coords.longitude);
                    const params = {
                      lat: lat,
                      lng: long
                    };
                    this.props.actions.watchLocation(
                      params,
                      this._watchLocationSuccess,
                      this._watchLocationError
                    );
                  },
                  error => {
                    console.log(
                      "[js] Received background-fetch current position  : error ",
                      error
                    );
                  },
                  { enabledHighAccuracy: true, timeout: 200000 }
                );
              } catch (e) {
                console.log(
                  "[js] Received background-fetch current position  : e ",
                  e.message
                );
              }

              // Required: Signal completion of your task to native code
              // If you fail to do this, the OS can terminate your app
              // or assign battery-blame for consuming too much background-time
              BackgroundFetch.finish(BackgroundFetch.FETCH_RESULT_NEW_DATA);
            },
            error => {
              console.log("[js] RNBackgroundFetch failed to start");
            }
          );
          break;
      }
    });
  }

  componentWillMount = () => {
    // if(this.watchID != null) {
    //   navigator.geolocation.clearWatch(this.watchID);
    //   this.watchID = null;
    // }
  };

  _actionSetActive = params => {
    this.props.actions.setActive(
      params,
      this._changeStatusSuccess,
      this._changeStatusFailed
    );
  };

  _changeStatusSuccess = data => {
    // this._backgroundFetch(data);
    if (data.status == "1") {
      this._watchPosition();
    } else {
      this._clearWatchPosition(); // clear watch position after cancel
    }
    console.log("_changeStatusSuccess : ", data);
  };

  _changeStatusFailed = error => {
    console.log("_changeStatusFailed : ", error);
  };

  render() {
    return (
      <View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          alignItems: "center"
        }}
      >
        <MapView
          ref={map => (this.map = map)}
          region={this.state.initialPosition}
          showsMyLocationButton={false}
          style={styles.maps}
          zoomEnabled={true}
          loadingEnabled={true}
          loadingIndicatorColor="#606060"
          loadingBackgroundColor="#FFFFFF"
          moveOnMarkerPress={false}
          showsUserLocation={false}
          followUserLocation={
            false
          } /* This only works if showsUserLocation is true */
          showsCompass={true}
          showsPointsOfInterest={false}
          showsScale={true}
          minZoomLevel={15}
          maxZoomLevel={20}
          provider="google"
        >
          <MapView.Marker
            image={Assets.pin}
            key="1"
            title=""
            description=""
            coordinate={this.state.markerPosition}
            // onPress={()=> this._clickOrder()}
          />
        </MapView>

        <View
          style={{
              paddingTop: Platform.OS === 'ios' ? 24 : 0,
              marginTop: hp(h(35))
            }}
        >
          <DriverStatus
            handleSetActive={this._actionSetActive}
            handleProfile={this._clickToProfile}
            userInfo={this.props.userInfo}
            // successStatusDriver={this.props.successStatusDriver}
            valueSliderAccept={this.props.valueSliderAccept}
            swipeStatusOrderUpdate={this.props.actions.swipeStatusOrderUpdate}
            // clearWatchPosition={this._clearWatchPosition}
            // data={(this.props.data && this.props.data.length) ? this.props.data : {'status': this.props.userInfo.status}}
          />
        </View>

        <View
          style={{
            justifyContent: "space-between",
            top: 0,
            marginTop: hp(h(400))
          }}
        >
          {/* <TouchableOpacity
            style={{
              backgroundColor: "rgb(255,255,255)",
              width: wp("90%"),
              height: hp(h(100)),
              borderRadius: 15,
              justifyContent: "center"
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View
                style={{
                  flex: 2,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    backgroundColor: themes.purple_50,
                    alignItems: "center",
                    justifyContent: "center",
                    borderRadius: hp(h(36)),
                    width: wp(w(36)),
                    height: hp(h(36))
                  }}
                >
                  <IonIcon
                    name="md-wallet"
                    color={themes.white}
                    borderRadius={30}
                    size={29}
                    style={{}}
                  />
                </View>
              </View>
              <View style={{ flexDirection: "column", flex: 6 }}>
                <Text
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: hp(h(12)),
                    color: themes.gray
                  }}
                >
                  YOUR BALANCE
                </Text>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <Text
                    style={{
                      fontFamily: "Helvetica-Light",
                      fontSize: hp(h(12)),
                      color: themes.gray_50,
                      marginTop: hp(h(8))
                    }}
                  >
                    QR
                  </Text>
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      fontSize: hp(h(20)),
                      color: themes.mainColor,
                      marginLeft: hp(h(10))
                    }}
                  >
                    {this.props.userInfo.driver_balance}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 2
                }}
              >
                <Text
                  style={{
                    color: themes.blueSea,
                    fontFamily: "Helvetica",
                    fontSize: hp(h(10)),
                    marginTop: hp(h(40))
                  }}
                >
                  MORE
                </Text>
              </View>
            </View>
          </TouchableOpacity> */}
        </View>
        <View
          style={{
            flexDirection: "row",
            // marginTop: hp(h(26)),
            marginTop: hp(h(86)) // using above margin top and comment this line if balance card is shown
          }}
        >
          {/* <TouchableOpacity>
            <Image source={Assets.homeBtn} />
          </TouchableOpacity>
          <TouchableOpacity style={{ marginHorizontal: wp(w(60)) }}>
            <Image source={Assets.performanceBtn} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image source={Assets.helpBtn} />
          </TouchableOpacity> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  maps: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1
  }
});
