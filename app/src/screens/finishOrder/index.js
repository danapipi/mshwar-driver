/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-10-29 12:34:17
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-29 12:46:46
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  StyleSheet,
  Dimensions,
  Alert,
  Platform
} from "react-native";
import MapView, { Marker, ProviderPropType } from "react-native-maps";
import Assets from "../../../assets";
import Slider from "react-native-slider";
import { themes } from "../../../config/themes";
import Communications from "react-native-communications";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h
} from "../../../config/layout";
import Permissions from "react-native-permissions";
import ModalDetailOrder from "../../../components/detailOrder";
import MapViewDirections from "react-native-maps-directions";
import { keys } from "../../../config/keys";
import { getData, removeData } from "../../../utils/storage";
import { name as appName } from "../../../../app/app.json";
import Orientation from "react-native-orientation";
import HeaderCustomer from "../../../components/headerCustomer";
import DeviceInfo from "react-native-device-info";


const { width, height } = Dimensions.get("window");
const LATITUDE_DELTA = 0.0222;
const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);
const map = null;
const GOOGLE_MAPS_APIKEY = keys.googleApi;

const markerIDs = ["Marker1", "Marker2"];
const timeout = 500;
let animationTimeout;
const DEFAULT_PADDING = { top: 40, right: 40, bottom: 40, left: 40 };
const brand = DeviceInfo.getBrand();



export default class FinishScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: {},
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0
      },
      markerPosition: {
        latitude: 0,
        longitude: 0
      },
      location: "",
      statusDriver: "inactive",
      cameraStatus: false,
      photo: Assets.ImageBackground,
      sendOther: false,
      labelSlide: "ORDER DELIVERED",
      modalVisible: false,
      statusOrder: "READY TO DELIVER",
      restoDestination: {
        latitude: 0,
        longitude: 0
      },
      destination: {
        latitude: 0,
        longitude: 0
      },
      sliderMinValue: 3,
      sliderMaxValue: 20
    };

    this.watchIDFinish = null;
  }

  // _getCurrentPosition() {
  //   try {
  //     navigator.geolocation.getCurrentPosition(
  //       position => {
  //         // console.log('Position current position : ',position)

  //         // comment this variable for testing direction
  //         var lat = parseFloat(position.coords.latitude);
  //         var long = parseFloat(position.coords.longitude);

  //         var initialRegion = {
  //           latitude: lat,
  //           longitude: long,
  //           latitudeDelta: LATITUDE_DELTA,
  //           longitudeDelta: LONGITUDE_DELTA
  //         };

  //         // var restoPosition = {
  //         //   latitude: parseFloat(this.props.orders.merchant_data.lat),
  //         //   longitude: parseFloat(this.props.orders.merchant_data.lng)
  //         // };

  //         // var destinationPosition = {
  //         //   latitude: parseFloat(this.props.orders.lat),
  //         //   longitude: parseFloat(this.props.orders.lng)
  //         // };

  //         this.setState({ initialPosition: initialRegion });
  //         this.setState({ markerPosition: initialRegion });
  //         // this.setState({ restoDestination: restoPosition });
  //         // this.setState({ destination: destinationPosition });
  //       },
  //       error => {
  //         // console.log('Error current position : ',error)
  //         switch (error.code) {
  //           case 1:
  //             if (Platform.OS === "ios") {
  //               Alert.alert("", "Permite apps to access your location");
  //             } else {
  //               Alert.alert("", "Permite apps to access your location");
  //             }
  //             break;
  //           default:
  //             Alert.alert(JSON.stringify(error));
  //         }
  //       },
  //       { enabledHighAccuracy: true, timeout: 200000 }
  //     );

  //     this.watchIDFinish = navigator.geolocation.watchPosition(position => {
  //       var lat = parseFloat(position.coords.latitude);
  //       var long = parseFloat(position.coords.longitude);

  //       var lastRegion = {
  //         latitude: lat,
  //         longitude: long,
  //         latitudeDelta: LATITUDE_DELTA,
  //         longitudeDelta: LONGITUDE_DELTA
  //       };

  //       this.setState({ initialPosition: lastRegion });
  //       this.setState({ markerPosition: lastRegion });
  //     });
  //   } catch (e) {
  //     alert(e.message || "");
  //   }
  // }

  _getDriverPosition = () => {
    var options = {
      // timeout: 5000
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
      distanceFilter: 0
    };
    this.watchIDFinish = navigator.geolocation.watchPosition(
      this._showLocation,
      this._errorLocation,
      options
    );
  };

  _showLocation = position => {
    var lat = parseFloat(position.coords.latitude);
    var long = parseFloat(position.coords.longitude);

    const params = {
      lat: lat,
      lng: long
    };
    this.props.actions.watchLocation(
      params,
      this._watchLocationSuccess,
      this._watchLocationError
    );

    // var lastRegion = {
    //   latitude: lat,
    //   longitude: long,
    //   latitudeDelta: LATITUDE_DELTA,
    //   longitudeDelta: LONGITUDE_DELTA
    // };

    // this.setState({ initialPosition: lastRegion });
    // this.setState({ markerPosition: lastRegion });
  };

  _errorLocation = error => {
    if (error.code == 1) {
      Alert.alert(appName, "Access is denied!");
    } else if (error.code == 2) {
      Alert.alert(appName, "Position is unavailable!");
    }
  };

  _watchLocationSuccess = data => {
    console.log("finish order", data.active_order);
    const { navigate } = this.props.navigation;
    if (data.active_order === null) {
      navigator.geolocation.clearWatch(this.watchIDFinish); // clear watch position
      Alert.alert(
        appName,
        "Order has been canceled",
        [
          {
            text: "OK",
            onPress: () => {
              navigate("Home");
            }
          }
        ],
        { cancelable: false }
      );
    }
    console.log("_watchLocationSuccess : ", data);
  };

  _watchLocationError = error => {
    console.log("_watchLocationError :", error);
  };

  // _slideComplete = () => {
  //   const { navigate } = this.props.navigation;
  //   navigate("KeyCustomer");
  // };

  _valueChange = value => {
    if (value === this.state.sliderMaxValue) {
      const params = {
        status: "delivered"
      };
      this.props.actions.swipeStatusOrder(
        params,
        this._swipeStatusOrderSuccess,
        this._swipeStatusOrderFailed
      );
      this.setState({
        labelSlide: "ORDER DELIVERED"
      });
    } else {
      this.setState({
        labelSlide: ""
      });
      this.props.actions.swipeStatusOrderUpdate(Math.floor(value));
    }
  };

  _completeChange = value => {
    if (Math.floor(value) < this.state.sliderMaxValue) {
      this.setState({
        labelSlide: "ORDER DELIVERED"
      });
      this.props.actions.swipeStatusOrderUpdate(this.state.sliderMinValue);
    }
  };

  _swipeStatusOrderSuccess = data => {
    // console.log('_swipeStatusOrderSuccess', data);
    this.props.actions.swipeStatusOrderUpdate(this.state.sliderMaxValue);
    try {
      getData(keys.activeOrder).then(res => {
        console.log("res", res);
        if (res !== "") {
          // if empty order_id in storage
          this.props.actions.fetchOrderbyId(
            res,
            this._fetchOrderByIdSuccess,
            this._fetchOrderByIdFailed
          );
        } else {
          Alert.alert(
            appName,
            "Order has been canceled",
            [
              {
                text: "OK",
                onPress: () => {
                  navigator.geolocation.clearWatch(this.watchIDFinish); // clear watch position
                  const { navigate } = this.props.navigation;
                  navigate("Home");
                }
              }
            ],
            { cancelable: false }
          );
        }
      });
    } catch (error) {
      console.log("get data storage :", error);
    }
  };

  _swipeStatusOrderFailed = error => {
    // console.log('_swipeStatusOrderFailed', error);
    this.props.actions.swipeStatusOrderUpdate(this.state.sliderMinValue);
  };

  _fetchOrderByIdSuccess = async data => {
    const { navigate } = this.props.navigation;
    if (Object.keys(data).length === 0) {
      await navigator.geolocation.clearWatch(this.watchIDFinish); // clear watch position
      await Alert.alert(
        appName,
        "Order has been canceled",
        [
          {
            text: "OK",
            onPress: () => {
              navigate("Home");
            }
          }
        ],
        { cancelable: false }
      );
    } else {
      await navigator.geolocation.clearWatch(this.watchIDFinish);
      await this.props.actions.swipeStatusOrderUpdate(
        this.state.sliderMinValue
      );
      await navigate("KeyCustomer");
    }
  };

  _fetchOrderByIdFailed = error => {
    try {
      removeData(keys.activeOrder).then(res => {
        Alert.alert(
          appName,
          "Order has been canceled",
          [
            {
              text: "OK",
              onPress: () => {
                navigator.geolocation.clearWatch(this.watchIDFinish);
                const { navigate } = this.props.navigation;
                navigate("Home");
              }
            }
          ],
          { cancelable: false }
        );
      });
    } catch (error) {
      console.log("error remove storage", error);
    }
  };

  _clickOrderDetail = () => {
    this.setState({
      modalVisible: true
    });
  };

  _closeModal = () => {
    this.setState({
      modalVisible: false
    });
  };

  componentDidMount = async() => {
    await Orientation.lockToPortrait();

    // animationTimeout = setTimeout(() => {
    //   this.focus1();
    // }, timeout);
    await this.onLayout()

    await Permissions.request("location", {
      rationale: {
        title: "Location Permission",
        message:
          "There was a problem getting your permission. " +
          "Please enable it from settings."
      }
    }).then(response => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      // this._getCurrentPosition();
      this._getDriverPosition();
      this.setState({ location: response });
    });

  }

  // focusMap(markers, animated) {
  //   if (Platform.OS !== "ios") {
  //     this.map.fitToSuppliedMarkers(
  //       markers,
  //       { edgePadding: { top: 20, right: 20, bottom: 20, left: 20 } },
  //       animated
  //     );
  //   }
  // }

  // focus1() {
  //   animationTimeout = setTimeout(() => {
  //     this.focusMap([markerIDs[0], markerIDs[1]], true);
  //   }, timeout);
  // }

  onLayout = () => {
    setTimeout(() => {
      this.map.fitToCoordinates(
        [
          {
            latitude: parseFloat(this.props.location.lat),
            longitude: parseFloat(this.props.location.lng)
          },
          {
            latitude: parseFloat(this.props.orders.lat),
            longitude: parseFloat(this.props.orders.lng)
          }
        ],
        { edgePadding: DEFAULT_PADDING, animated: true }
      );
    }, 500);
  };

  render() {
    return (
      <View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          alignItems: "center"
        }}
      >
        <StatusBar hidden={true} />
        <MapView
          provider={this.props.provider}
          ref={map => (this.map = map)}
          region={{
            latitude: parseFloat(this.props.location.lat),
            longitude: parseFloat(this.props.location.lng),
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          }}
          showsMyLocationButton={false}
          style={styles.maps}
          zoomEnabled={true}
          loadingEnabled={true}
          loadingIndicatorColor="#606060"
          loadingBackgroundColor="#FFFFFF"
          moveOnMarkerPress={false}
          showsUserLocation={false}
          followUserLocation={
            false
          } /* This only works if showsUserLocation is true */
          showsCompass={true}
          showsPointsOfInterest={false}
          showsScale={true}
          // minZoomLevel={15}
          // maxZoomLevel={20}
          provider="google"
          // onLayout={this.onLayout()}
        >
          <Marker
            identifier="Marker1"
            coordinate={{
              latitude: parseFloat(this.props.location.lat),
              longitude: parseFloat(this.props.location.lng),
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA
            }}
            // pinColor={"rgb(102,0,153)"}
            image={Assets.pin}
            key="1"
            title=""
            description=""
          />
          <Marker
            identifier="Marker2"
            coordinate={{
              latitude: parseFloat(this.props.orders.lat),
              longitude: parseFloat(this.props.orders.lng)
            }}
            image={Assets.home}
            pinColor={"rgb(102,0,153)"}
            key="2"
            title=""
            description=""
          />
          <MapViewDirections
            // origin={this.state.restoDestination}
            origin={{
              latitude: parseFloat(this.props.location.lat),
              longitude: parseFloat(this.props.location.lng),
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA
            }}
            destination={{
              latitude: parseFloat(this.props.orders.lat),
              longitude: parseFloat(this.props.orders.lng)
            }}
            apikey={GOOGLE_MAPS_APIKEY}
            strokeWidth={3}
            strokeColor="rgb(102,0,153)"
          />
        </MapView>
        <View 
          style={{
              paddingTop: Platform.OS === 'ios' ? 24 : 0,
        }}>
          <HeaderCustomer data={this.props.orders} />
        </View>

        {this.state.sendOther ? (
          <View
            style={{
              width: wp("90%"),
              // height: 288,
              // marginHorizontal: wp(w(45)),
              borderRadius: 15,
              backgroundColor: "rgb(255,255,255)",
              marginTop: hp(h(250)),
              marginBottom: hp(h(10))
            }}
          >
            <View
              style={{
                height: hp(h(50)),
                backgroundColor: "rgb(249,249,249)",
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica-Light",
                  fontSize: hp(h(12)),
                  color: "rgb(0,153,204)",
                  marginTop: hp(h(5))
                }}
              >
                DELIVERING FOOD
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginTop: hp(h(17)),
                marginBottom: hp(h(17))
              }}
            >
              <Image
                source={Assets.location}
                style={{
                  width: 23,
                  height: 23,
                  marginHorizontal: wp(w(24)),
                  marginTop: hp(h(10)),
                  justifyContent: "center",
                  alignItems: "center"
                }}
              />
              <View style={{ flexDirection: "column" }}>
                <Text
                  style={{
                    fontFamily: "Roboto-Medium",
                    fontSize: hp(h(18)),
                    color: themes.gray
                  }}
                >
                  {this.props.orders.address}
                </Text>
                {/* <Text
                  style={{
                    fontFamily: "Helvetica-Light",
                    fontSize: hp(h(14)),
                    color: themes.gray_50
                  }}
                >
                  Tebet dalam, Soo faraway
                </Text> */}
              </View>
            </View>
            <View
              style={{
                borderWidth: 0.5,
                borderColor: themes.border,
                borderLeftColor: "transparent",
                borderRightColor: "transparent",
                flexDirection: "row",
                marginVertical: hp(h(5))
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica-Light",
                  fontSize: hp(h(12)),
                  color: themes.gray,
                  flex: 1,
                  marginVertical: hp(h(5)),
                  marginLeft: wp(w(30)),
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                TO
              </Text>
              <View
                style={{
                  flex: 6
                }}
              >
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: hp(h(14)),
                    color: themes.gray,
                    marginVertical: hp(h(5)),
                    textAlign: "left"
                  }}
                >
                  Granny
                </Text>
              </View>
              <TouchableOpacity>
                <Text
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: hp(h(10)),
                    color: themes.blueSea,
                    flex: 3,
                    marginVertical: hp(h(7)),
                    marginRight: wp(w(39)),
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  CALL
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={{
                alignItems: "center",
                justifyContent: "center",
                marginTop: hp(h(12)),
                paddingVertical: hp(h(10))
              }}
              onPress={() => {
                this._clickOrderDetail();
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica",
                  fontSize: hp(h(10)),
                  color: "rgb(74,144,226)",
                  textAlign: "center",
                  marginHorizontal: wp(w(60))
                }}
              >
                DETAIL ORDER
              </Text>
            </TouchableOpacity>
            <View
              style={{
                marginHorizontal: wp(w(15)),
                marginTop: hp(h(15)),
                marginBottom: hp(h(20)),
                backgroundColor: "rgb(235,235,235)",
                borderRadius: 15
                // height: hp(h(40))
                //   flexDirection: "row"
              }}
            >
              <Slider
                value={this.props.valueSliderAccept}
                thumbImage={Assets.swipeArrow}
                thumbStyle={{
                  width: 58,
                  height: 39,
                  marginVertical: hp(h(10))
                }}
                thumbTintColor="transparent"
                minimumTrackTintColor="transparent"
                maximumTrackTintColor="transparent"
                minimumValue={this.state.sliderMinValue}
                maximumValue={this.state.sliderMaxValue}
                onValueChange={value => {
                  this._valueChange(value);
                }}
                onSlidingComplete={value => {
                  this._completeChange(value);
                }}
                style={{
                  marginVertical: hp(h(5)),
                  marginHorizontal: wp(w(5)),
                  zIndex: 10
                  // position: "absolute"
                }}
              />
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  height: "100%",
                  position: "absolute",
                  width: wp(w(280)),
                  right: 0
                }}
              >
                <Text
                  style={{
                    fontFamily: "Helvetica-Light",
                    fontSize: hp(h(12)),
                    color: themes.gray_70
                  }}
                >
                  {this.state.labelSlide}
                </Text>
              </View>
            </View>
            <View
              style={{
                borderWidth: 0.5,
                borderTopColor: "rgb(151,151,151)",
                borderLeftColor: "transparent",
                borderRightColor: "transparent",
                borderBottomColor: "transparent",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                paddingVertical: hp(h(5))
              }}
            >
              <Text
                style={{
                  fontFamily: "Roboto-Light",
                  fontSize: hp(h(12)),
                  color: "rgb(74,74,74)"
                }}
              >
                Transaction Number : {this.props.orders.id}
              </Text>
            </View>
          </View>
        ) : (
          <View
            style={{
              width: wp("90%"),
              // height: 288,
              // marginHorizontal: wp(w(45)),
              borderRadius: 15,
              backgroundColor: themes.white,
              marginTop: brand === 'Apple' ? hp(h(300)) : hp(h(320)),
              // marginBottom: hp(h(10))
            }}
          >
            <View
              style={{
                height: hp(h(50)),
                backgroundColor: "rgb(249,249,249)",
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica-Light",
                  fontSize: hp(h(12)),
                  color: "rgb(0,153,204)",
                  marginTop: hp(h(5))
                }}
              >
                DELIVERING FOOD
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginTop: hp(h(17)),
                marginBottom: hp(h(17)),
                width: wp("90%"),
                paddingRight: wp(w(10))
              }}
            >
              <Image
                source={Assets.location}
                style={{
                  width: 23,
                  height: 23,
                  marginHorizontal: wp(w(24)),
                  marginTop: hp(h(10)),
                  justifyContent: "center",
                  alignItems: "center"
                }}
              />
              <View style={{ 
                      flexDirection: "column", 
                      flex: 1, 
                      height: hp(h(80)) }}>
                <Text
                  style={{
                    fontFamily: "Roboto-Medium",
                    fontSize: hp(h(18)),
                    color: "rgb(74,74,74)"
                  }}
                >
                  {this.props.orders.address}
                </Text>
                {/* <Text
                  style={{
                    fontFamily: "Helvetica-Light",
                    fontSize: hp(h(14)),
                    color: "rgb(155,155,155)"
                  }}
                >
                  Doha, Qatar
                </Text> */}
                <TouchableOpacity
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: hp(h(12)),
                    paddingVertical: hp(h(10)),
                    width: wp("55%")
                  }}
                  onPress={() => {
                    this._clickOrderDetail();
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Helvetica",
                      fontSize: hp(h(10)),
                      color: themes.blueSea,
                      textAlign: "center"
                      // marginHorizontal: wp(w(60))
                    }}
                  >
                    DETAIL ORDER
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                marginHorizontal: wp(w(15)),
                marginTop: hp(h(15)),
                marginBottom: hp(h(10)),
                backgroundColor: "rgb(235,235,235)",
                borderRadius: 15
                // height: hp(h(40))
                //   flexDirection: "row"
              }}
            >
              <Slider
                value={this.props.valueSliderAccept}
                thumbImage={Assets.swipeArrow}
                thumbStyle={{
                  width: 58,
                  height: 39,
                  marginVertical: hp(h(10))
                }}
                thumbTintColor="transparent"
                minimumTrackTintColor="transparent"
                maximumTrackTintColor="transparent"
                minimumValue={this.state.sliderMinValue}
                maximumValue={this.state.sliderMaxValue}
                onValueChange={value => {
                  this._valueChange(value);
                }}
                onSlidingComplete={value => {
                  this._completeChange(value);
                }}
                style={{
                  marginVertical: hp(h(5)),
                  marginHorizontal: wp(w(5)),
                  zIndex: 10
                  // position: "absolute"
                }}
              />
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  height: "100%",
                  position: "absolute",
                  width: wp(w(280)),
                  right: wp(w(25))
                }}
              >
                <Text
                  style={{
                    fontFamily: "Helvetica-Light",
                    fontSize: hp(h(12)),
                    color: themes.gray_70
                  }}
                >
                  {this.state.labelSlide}
                </Text>
              </View>
            </View>
            <View
              style={{
                borderWidth: 0.5,
                borderTopColor: themes.border,
                borderLeftColor: "transparent",
                borderRightColor: "transparent",
                borderBottomColor: "transparent",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                paddingVertical: hp(h(5))
              }}
            >
              <Text
                style={{
                  fontFamily: "Roboto-Light",
                  fontSize: hp(h(12)),
                  color: themes.gray
                }}
              >
                Transaction Number : {this.props.orders.id}
              </Text>
            </View>
          </View>
        )}

        <ModalDetailOrder
          modalVisible={this.state.modalVisible}
          handleClose={this._closeModal}
          userInfo={this.props.userInfo}
          data={this.props.orders}
          statusOrder={this.state.statusOrder}
        />
      </View>
    );
  }
}

FinishScreen.propTypes = {
  provider: ProviderPropType
};

const styles = StyleSheet.create({
  maps: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1
  }
});
