/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-19 20:01:53 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-20 05:30:08
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  Alert,
  TouchableOpacity,
} from "react-native";
import CardView from 'react-native-cardview'
import { NavigationActions, StackActions } from 'react-navigation'
import Assets from "../../../assets";
import IonIcon from "react-native-vector-icons/Ionicons";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { percentageWidth as w, percentageHeight as h } from "../../../config/layout";
import { themes } from "../../../config/themes";
import StatusDriver from "../../../components/StatusDriver";
import { getToken, clearToken } from "../../../utils/storage";
import { name as appName } from "../../../../app/app.json";
import Orientation from 'react-native-orientation';


export default class ProfileScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
          
        };
    }

    componentDidMount = () => {
        Orientation.lockToPortrait();
    
      }

    _actionSetActive = params => {
        this.props.actions.setActive(
          params,
          this._changeStatusSuccess,
          this._changeStatusFailed
        );
    };

    _changeStatusSuccess = data => {
        console.log("_changeStatusSuccess : ", data);
    };
    
    _changeStatusFailed = error => {
        console.log("_changeStatusFailed : ", error);
    };

    _actionSetLogout = (params) => {
        this.props.actions.setActive(
            params,
            this._actionSetLogoutSuccess,
            this._actionSetLogoutFailed
          );
    }

    _actionSetLogoutSuccess = data => {
        console.log("_actionSetLogoutSuccess : ", data);
        const { navigation } = this.props
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'Login' })
            ]
        })
        navigation.dispatch(resetAction)
    };
    
    _actionSetLogoutFailed = error => {
        console.log("_actionSetLogoutFailed : ", error);
    };

    _clickLogout = () => {
        try {
            getToken().then((token) => {
                if(token && token.length > 0) {
                    // clearToken();
                    const params = {
                        status: 0 // set to InActive
                    }
                    if(clearToken()) {
                        this._actionSetLogout(params);
                    }
                }   
            })
          } catch (error) {
            console.log("get data storage :", error);
          }
    }

    render() {
        return (
            <View style={{
                    flex: 1,
            }}>
                <View style={{
                        // header
                }}>
                    <View style={{
                            height: hp(h(20)),
                            backgroundColor: themes.purple_70
                    }}></View>
                    <View style={{
                            backgroundColor: themes.mainColor,
                            flexDirection: 'row',
                            
                    }}>
                        <TouchableOpacity 
                            onPress={() => {
                                const { goBack } = this.props.navigation;
                                goBack();
                            }}
                            style={{
                                paddingTop: hp(h(15)),
                                paddingLeft: wp(w(15)),
                                paddingBottom: hp(h(36)),
                                paddingRight: wp(w(33)),
                        }}>
                            <IonIcon
                                name="md-arrow-back"
                                color={themes.white}
                                size={25}
                            />
                        </TouchableOpacity>
                        <View style={{
                                flex: 1
                        }}>
                            <View style={{
                                    paddingBottom: hp(h(10)),
                                    paddingTop: hp(h(15)),
                            }}>
                                <Text style={{
                                        fontSize: hp(h(18)),
                                        fontFamily: 'Roboto-Medium',
                                        fontWeight: 'bold',
                                        color: themes.white
                                }}>
                                    My Profile
                                </Text>
                            </View>
                            <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    marginBottom: hp(h(10))
                            }}>
                                <Text style={{
                                        fontSize: hp(h(11)),
                                        fontFamily: 'Helvetica-Bold',
                                        fontWeight: 'bold',
                                        color: themes.gray_50
                                }}>YOU ARE CURRENTLY</Text>
                                <View style={{
                                        position: 'absolute',
                                        right: 0
                                }}>
                                    <StatusDriver
                                        handleSetActive={this._actionSetActive}
                                        userInfo={this.props.userInfo}
                                        // successStatusDriver={this.props.successStatusDriver}
                                        valueSliderAccept={this.props.valueSliderAccept}
                                        swipeStatusOrderUpdate={this.props.actions.swipeStatusOrderUpdate}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{
                        
                }}>
                    <CardView 
                        cardElevation={2}
                        cardMaxElevation={2}
                        cornerRadius={24}
                        style={{
                            marginHorizontal: wp(w(20)),
                            marginTop: hp(h(45)),
                            borderWidth: 1,
                            borderColor: themes.border,
                    }}>
                        <View style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingVertical: hp(h(25)),
                        }}>
                            <Image 
                                source={Assets.photoDriver} 
                                style={{
                                    width: 81,
                                    height: 81
                                }}/>
                        </View>
                        <View style={{
                                borderBottomWidth: 1,
                                borderBottomColor: themes.border,
                                paddingLeft: wp(w(17)),
                                paddingBottom: hp(h(5)),
                                paddingTop: hp(h(5)),
                                marginHorizontal: wp(w(34))
                        }}>
                            <Text style={{
                                    fontSize: hp(h(14)),
                                    fontFamily: 'Helvetica',
                                    color: themes.gray
                            }}>{this.props.userInfo.name}</Text>
                        </View>
                        <View style={{
                                borderBottomWidth: 1,
                                borderBottomColor: themes.border,
                                paddingLeft: wp(w(17)),
                                paddingBottom: hp(h(5)),
                                paddingTop: hp(h(5)),
                                marginHorizontal: wp(w(34))
                        }}>
                            <Text style={{
                                    fontSize: hp(h(14)),
                                    fontFamily: 'Helvetica',
                                    color: themes.gray
                            }}>{this.props.userInfo.email}</Text>
                        </View>
                        <View style={{
                                paddingLeft: wp(w(17)),
                                paddingBottom: hp(h(5)),
                                paddingTop: hp(h(5)),
                                marginHorizontal: wp(w(34))
                        }}>
                            <Text style={{
                                    fontSize: hp(h(14)),
                                    fontFamily: 'Helvetica',
                                    color: themes.gray
                            }}>{this.props.userInfo.phone}</Text>
                        </View>
                        <View style={{
                                marginHorizontal: wp(w(16)),
                                marginVertical: hp(h(25)),
                        }}>
                            <Text style={{
                                    fontSize: hp(h(10)),
                                    color: themes.blueSea,
                                    fontFamily: 'Helvetica',
                                    textAlign: 'right',
                                    // EDIT PROFILE
                            }}></Text>
                        </View>
                    </CardView>
                    <TouchableOpacity 
                        onPress={() => {
                            Alert.alert(
                                appName,
                                'Do you want to logout ?',
                                [
                                    {text: 'NO', onPress: () => console.log('NO Pressed')},
                                    {text: 'YES', onPress: () => this._clickLogout()},
                                ]
                            )
                        }}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: hp(h(20)),
                            paddingVertical: hp(h(15))
                    }}>
                        <Text style={{
                            fontSize: hp(h(14)),
                            color: themes.red,
                            fontFamily: 'Helvetica',
                            textAlign: 'right',
                        }}>LOGOUT</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}