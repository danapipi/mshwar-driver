import React, { Component } from "react";
import {
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  StatusBar
} from "react-native";
import Assets from "../../../../assets";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import Orientation from 'react-native-orientation';


export default class OtpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timer: 30
    };
  }

  _submit = () => {
    const { navigate } = this.props.navigation;
    navigate("Home");

  };

  _backToLogin = () => {
    const { navigate } = this.props.navigation;
    navigate("Login");
  };

  componentDidMount = () => {
    Orientation.lockToPortrait();

  }


  render() {
    return (
      <View>
        <StatusBar hidden={true}/>
        <ImageBackground
          source={Assets.background}
          style={{
            width: wp("100%"),
            height: hp("100%")
          }}
          resizeMode="cover"
        >
          {/* <View style={{}}> */}
          <TouchableOpacity
            onPress={() => this._backToLogin()}
            style={{ flexDirection: "row", justifyContent: "flex-end" }}
          >
            <Text
              style={{
                marginTop: 20,
                marginRight: 10,
                fontFamily: "Helvetica-Light",
                fontSize: 12,
                color: "rgb(255,255,255)"
              }}
            >
              CANCEL
            </Text>
            <Image
              source={Assets.cancelLogin}
              style={{
                height: 24,
                width: 24,
                marginTop: 17,
                marginRight: 13
              }}
            />
          </TouchableOpacity>
          {/* </View> */}
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: 49
            }}
          >
            <Image source={Assets.logo} style={{ height: 22, width: 141 }} />
            <Text
              style={{
                fontFamily: "Helvetica-Bold",
                fontSize: 13,
                color: "rgb(255,255,255)",
                marginTop: 9
              }}
            >
              DRIVER COMPANION
            </Text>
          </View>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Text
              style={{
                fontFamily: "Helvetica",
                fontSize: 18,
                color: "rgb(255,255,255)",
                marginTop: 48
              }}
            >
              Please enter confirmation code
            </Text>
          </View>
          <View
            style={{
              color: "rgb(255,255,255)",
              marginLeft: 75,
              marginRight: 69,
              flexDirection: "row"
            }}
          >
            <TextInput
              keyboardType="number-pad"
              style={{
                marginTop: 35,
                backgroundColor: "rgb(255,255,255)",
                width: 53,
                height: 66,
                borderRadius: 15,
                fontSize: 18,
                fontFamily: "Helvetica",
                textAlign: "center"
              }}
            />
            <TextInput
              keyboardType="number-pad"
              style={{
                marginTop: 35,
                backgroundColor: "rgb(255,255,255)",
                width: 53,
                height: 66,
                borderRadius: 15,
                marginLeft: 17,
                fontSize: 18,
                fontFamily: "Helvetica",
                textAlign: "center"
              }}
            />
            <TextInput
              keyboardType="number-pad"
              style={{
                marginTop: 35,
                backgroundColor: "rgb(255,255,255)",
                width: 53,
                height: 66,
                borderRadius: 15,
                marginLeft: 17,
                fontSize: 18,
                fontFamily: "Helvetica",
                textAlign: "center"
              }}
            />
            <TextInput
              keyboardType="number-pad"
              style={{
                marginTop: 35,
                backgroundColor: "rgb(255,255,255)",
                width: 53,
                height: 66,
                borderRadius: 15,
                marginLeft: 17,
                fontSize: 18,
                fontFamily: "Helvetica",
                textAlign: "center"
              }}
            />
          </View>
          <TouchableOpacity
            style={{
              marginTop: 43
            }}
          >
            <Text
              style={{
                fontFamily: "Helvetica",
                fontSize: 16,
                color: "rgb(255,255,255)",
                textAlign: "center"
              }}
            >
              Resend code in 00 : 30
            </Text>
          </TouchableOpacity>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <TouchableOpacity
              style={{
                borderRadius: 15,
                width: 280,
                height: 46,
                backgroundColor: "rgb(235 ,235, 235)",
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
                marginTop: 100,
                marginBottom: 44
              }}
              onPress={() => this._submit()}
            >
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: 14,
                  color: "rgb(0,153,204)"
                }}
              >
                SUBMIT
              </Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
