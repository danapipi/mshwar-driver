import React, { Component } from "react";
import {
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Alert,
  Dimensions,
  Animated,
  Keyboard,
  Platform
} from "react-native";
import Assets from "../../../../assets";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { percentageWidth as w, percentageHeight as h } from "../../../../config/layout";
import { saveToken } from "../../../../utils/storage";
import { displayName as appName } from '../../../../../app/app.json';
import { NavigationActions, StackActions } from "react-navigation";
import { themes } from '../../../../config/themes';
import Orientation from 'react-native-orientation';


const { width, height } = Dimensions.get("window");

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      email: "",
      password: "", // Password123
      showKeyboard: false,
      labelLogin: 'SIGN IN',
      marginTopLoginInfo: hp(h(48))
    };

  }

  _handleKeyboardShow = event =>  {
    this.setState({
          showKeyboard: true,
          marginTopLoginInfo: hp(h(10)) });
  }

  _handleKeyboardHide = event => {
    this.setState({
          showKeyboard: false,
          marginTopLoginInfo: hp(h(48))});
  }

  componentDidMount = () => {
    Orientation.lockToPortrait();

  }

  componentWillMount() {
    this.keyboardWillShowSub = Keyboard.addListener(
      "keyboardDidShow",
      this._handleKeyboardShow
    );
    this.keyboardWillHideSub = Keyboard.addListener(
      "keyboardDidHide",
      this._handleKeyboardHide
    );
  }

  componentWillUnmount = () => {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  _inputEmail = text => {
    this.setState({ email: text });
    console.log("email", this.state.email);
  };

  _inputPassword = text => {
    this.setState({ password: text });
    console.log("pass", this.state.password);
  };

  _signInSubmit = () => {
    const params = {
      email: this.state.email,
      password: this.state.password
    } 
    this.setState({ 
          isLoading: this.props.loading,
          labelLogin: 'LOADING ...' });
    this.props.actions.login(params, this._onSuccess, this._onError)
    // const { navigate } = this.props.navigation;
    // navigate("Otp");
  };

  _onError = error => {
    this.setState({ 
          isLoading: this.props.loading,
          labelLogin: 'SIGN IN' });
    console.log("Error Login", error);
  };

  _actionSetActive = params => {
    this.props.actions.setActive(
      params,
      this._changeStatusSuccess,
      this._changeStatusFailed
    );
  };

  _changeStatusSuccess = data => {
    console.log("_changeStatusSuccess : ", data);
    const { navigation } = this.props;
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: "Home" })
      ]
    });
    navigation.dispatch(resetAction);
  };

  _changeStatusFailed = error => {
    console.log("_changeStatusFailed : ", error);
  };

  _onSuccess = data => {
    this.setState({ 
          isLoading: this.props.loading,
          labelLogin: 'SIGN IN' });
    const { navigation } = this.props;
    // console.log('data login', data);
    if (data.status === 201) {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "Otp" })]
      });
      navigation.dispatch(resetAction);
    } else {
      const res = data.data;
      if (res.success && res.token && res.token.length > 0) {
        saveToken(res.token).then(isSuccess => {
          if (isSuccess) {
            const params = {
              status: 1 // set to active
            }
            this._actionSetActive(params);
          }
        });
      } else {
        const message = res.message;
        Alert.alert(
          appName, 
          message, 
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
      }
    }
  };


  render() {
    return (
      <View>
        <StatusBar hidden={true} />
        <ImageBackground
          source={Assets.background}
          style={{
            width: wp("100%"),
            height: hp("100%")
          }}
          resizeMode="cover"
        >
          <View style={{
                  marginBottom: hp(h(44)),
                  paddingTop: Platform.OS === "ios" ? 24 : 0
          }}>
            <TouchableOpacity 
              onPress={() => {

              }}
              style={{ 
                    flexDirection: "row", 
                    marginTop: hp(h(17)),
                    marginRight: wp(w(13)),
                    justifyContent: "flex-end" }}>
              {/* <Image
                source={Assets.helpWhite}
                style={{
                  height: 24,
                  width: 24,
                }}
              /> */}
            </TouchableOpacity>
            {!this.state.showKeyboard && (
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: hp(h(49))
                  }}
                >
                  <Image source={Assets.logo} style={{ height: 22, width: 141 }} />
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      fontSize: hp(h(13)),
                      color: "rgb(255,255,255)",
                      marginTop: hp(h(9))
                    }}
                  >
                    DRIVER COMPANION
                    </Text>
                </View>
            )}
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <Text
                  style={[{
                    fontFamily: "Helvetica",
                    fontSize: hp(h(18)),
                    color: "rgb(255,255,255)",
                  }, { marginTop: this.state.marginTopLoginInfo }]}
                >
                  Please fill your login information
                    </Text>
              </View>
              <View
                style={{
                  color: "rgb(255,255,255)",
                  marginLeft: wp(w(55)),
                  marginRight: wp(w(69))
                }}
              >
                <TextInput
                  ref={ref => {
                    this.txtInputEmail = ref;
                  }}
                  placeholder="Email"
                  placeholderTextColor="rgb(255,255,255)"
                  keyboardType="email-address"
                  value={this.state.email}
                  style={{
                    color: "rgb(255,255,255)",
                    borderBottomWidth: 1,
                    borderBottomColor: "rgb(255,255,255)",
                    marginTop: hp(h(35))
                  }}
                  onChangeText={text => this._inputEmail(text)}
                />
                <TextInput
                  ref={ref => {
                    this.txtInputPassword = ref;
                  }}
                  placeholder="Password"
                  placeholderTextColor="rgb(255,255,255)"
                  secureTextEntry={true}
                  value={this.state.password}
                  style={{
                    color: "rgb(255,255,255)",
                    borderBottomWidth: 1,
                    borderBottomColor: "rgb(255,255,255)",
                    marginTop: hp(h(35))
                  }}
                  onChangeText={text => this._inputPassword(text)}
                />
              </View>
              <TouchableOpacity
                style={{
                  marginTop: hp(h(43))
                }}
              >
                {/* <Text
                  style={{
                    fontFamily: "Roboto-Regular",
                    fontSize: hp(h(16)),
                    color: "rgb(255,101,120)",
                    textAlign: "center"
                  }}
                >
                  FORGOT PASSWORD
                  </Text> */}
              </TouchableOpacity>
              <View style={{
                justifyContent: "center",
                alignItems: "center"
              }}>
                <TouchableOpacity
                  style={[{
                    borderRadius: 15,
                    width: wp(w(280)),
                    height: hp(h(46)),
                    backgroundColor: themes.gray_30,
                    justifyContent: "center",
                    alignItems: "center"
                  }, (this.state.showKeyboard) ?
                    { marginTop: hp(h(10)) } :
                    { marginTop: hp(h(100)) }]}
                  onPress={() => {
                    if(!this.state.isLoading){
                      this._signInSubmit()
                    }
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      fontSize: hp(h(14)),
                      color: themes.blueSea
                    }}
                  >
                    {this.state.labelLogin}
                  </Text>
                </TouchableOpacity>
              </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
