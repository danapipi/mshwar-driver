/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-10-29 12:34:17
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-30 10:58:19
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  Animated,
  Alert,
  StyleSheet,
  Dimensions,
  Platform,
  ScrollView,
  FlatList
} from "react-native";
import MapView, { Marker, Polyline } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import Assets from "../../../assets";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h,
  isIphoneX
} from "../../../config/layout";
import Permissions from "react-native-permissions";
import { themes } from "../../../config/themes";
import Slider from "react-native-slider";
import SlidingUpPanel from "rn-sliding-up-panel";
import ModalDetailOrder from "../../../components/detailOrder";
import ModalCancelOrder from "../../../components/cancelOrder";
import ModalCancelFinishOrder from "../../../components/cancelOrder/finishCancel";
import Communications from "react-native-communications";
import FlatListItem from "../../../components/detailOrder/FlatListItem";
import { setData, getData, removeData } from "../../../utils/storage";
import { keys } from "../../../config/keys";
import { name as appName } from "../../../../app/app.json";
import Orientation from "react-native-orientation";
import HeaderCustomer from "../../../components/headerCustomer";
import DeviceInfo from "react-native-device-info";

const { width, height } = Dimensions.get("window");
const LATITUDE_DELTA = 0.0222;
const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);
const map = null;
const GOOGLE_MAPS_APIKEY = keys.googleApi;

const DEFAULT_PADDING = { top: 40, right: 40, bottom: 40, left: 40 };
const brand = DeviceInfo.getBrand();

const markerIDs = ["Marker1", "Marker2"];
const timeout = 500;
let animationTimeout;

export default class OrderScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: {},
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0
      },
      markerPosition: {
        latitude: 0,
        longitude: 0
      },
      location: "",
      visible: false,
      allowDragging: false,
      modalVisible: false,
      modalVisibleCancel: false,
      modalCancelValue: 0,
      modalVisibleCancelFinish: false,
      labelSlide: "FOOD READY",
      statusOrder: "PICKING UP ORDER",
      restoDestination: {
        latitude: 0,
        longitude: 0
      },
      destination: {
        latitude: 0,
        longitude: 0
      },
      sliderMinValue: 3,
      sliderMaxValue: 20
    };

    this.watchIDOrder = null;
  }

  static defaultProps = {
    draggableRange: {
      top: hp(h(720)),
      bottom: hp(h(0))
    }
  };

  _draggedValue = new Animated.Value(-120);

  _slideComplete = () => {
    const { navigate } = this.props.navigation;
    navigate("Camera");
  };

  _clickOrderDetail = () => {
    this.setState({
      modalVisible: true
    });
  };

  _closeModal = () => {
    this.setState({
      modalVisible: false
    });
  };

  _clickCancelOrder = () => {
    this.setState({
      modalVisibleCancel: true
    });
  };

  _closeModalCancel = () => {
    this.setState({
      modalVisibleCancel: false,
      modalVisibleCancelFinish: false
    });
  };

  _acceptModalCancel = value => {
    if (value === this.state.sliderMaxValue) {
      navigator.geolocation.clearWatch(this.watchIDOrder); // clear watch position after cancel
      this.props.actions.swipeToCancel(this.props.selectedRadio);
      this.setState({
        modalVisibleCancel: false,
        modalVisibleCancelFinish: true
      });
    } else {
      this.props.actions.swipeStatusOrderUpdate(Math.floor(value));
    }
  };

  _changeModalCancelValue = value => {
    if (Math.floor(value) < this.state.sliderMaxValue) {
      this.props.actions.swipeStatusOrderUpdate(this.state.sliderMinValue);
    }
  };

  _closeModalCancelFinish = async () => {
    const params = {
      message: this.props.selectedRadio.name
    };

    await this.props.actions.clickToCancelOrder(
      params,
      this._clickToCancelOrderSuccess,
      this._clickToCancelOrderFailed
    );
  };

  _clickToCancelOrderSuccess = data => {
    try {
      removeData(keys.activeOrder).then(async res => {
        if (res) {
          this.setState({
            modalVisibleCancelFinish: false
          });

          const { navigate } = this.props.navigation;
          await navigate("Home");
        }
      });
    } catch (error) {
      console.log("error remove storage", error);
    }
  };

  _clickToCancelOrderFailed = error => {
    console.log("Error _clickToCancelOrderFailed : ", error);
  };

  _valueChange = value => {
    if (value === this.state.sliderMaxValue) {
      const params = {
        status: "ready"
      };
      this.props.actions.swipeStatusOrder(
        params,
        this._swipeStatusOrderSuccess,
        this._swipeStatusOrderFailed
      );
      this.setState({
        labelSlide: "FOOD READY"
      });
    } else {
      this.setState({
        labelSlide: ""
      });
      this.props.actions.swipeStatusOrderUpdate(Math.floor(value));
    }
  };

  _completeChange = value => {
    if (Math.floor(value) < this.state.sliderMaxValue) {
      this.setState({
        labelSlide: "FOOD READY"
      });
      this.props.actions.swipeStatusOrderUpdate(this.state.sliderMinValue);
    }
  };

  _swipeStatusOrderSuccess = data => {
    console.log("_swipeStatusOrderSuccess", data);
    this.props.actions.swipeStatusOrderUpdate(this.state.sliderMaxValue);
    try {
      getData(keys.activeOrder).then(res => {
        console.log("res", res);
        if (res !== "") {
          // if not empty order_id in storage
          this.props.actions.fetchOrderbyId(
            res,
            this._fetchOrderByIdSuccess,
            this._fetchOrderByIdFailed
          );
        } else {
          navigator.geolocation.clearWatch(this.watchIDOrder); // clear watch position after cancel
          const { navigate } = this.props.navigation;
          navigate("Home");
        }
      });
    } catch (error) {
      console.log("get data storage :", error);
    }
  };

  _swipeStatusOrderFailed = error => {
    console.log("_swipeStatusOrderFailed", error);
    this.props.actions.swipeStatusOrderUpdate(this.state.sliderMinValue);
  };

  _fetchOrderByIdSuccess = async data => {
    const { navigate } = this.props.navigation;
    if (Object.keys(data).length === 0) {
      // if empty object
      await navigator.geolocation.clearWatch(this.watchIDOrder); // clear watch position after cancel
      await Alert.alert(
        appName,
        "Order has been canceled",
        [
          {
            text: "OK",
            onPress: () => {
              navigate("Home");
            }
          }
        ],
        { cancelable: false }
      );
    } else {
      await navigator.geolocation.clearWatch(this.watchIDOrder);
      await this.props.actions.swipeStatusOrderUpdate(
        this.state.sliderMinValue
      );
      await navigate("Camera");
    }
  };

  _fetchOrderByIdFailed = error => {
    try {
      removeData(keys.activeOrder).then(res => {
        Alert.alert(
          appName,
          "Order has been canceled failed",
          [
            {
              text: "OK",
              onPress: () => {
                navigator.geolocation.clearWatch(this.watchIDOrder);
                const { navigate } = this.props.navigation;
                navigate("Home");
              }
            }
          ],
          { cancelable: false }
        );
      });
    } catch (error) {
      console.log("error remove storage", error);
    }
  };

  _renderFlatList = () => {
    return (
      <FlatList
        // data={listData}
        data={this.props.orders.product_data}
        numColumns={1}
        extraData={this.state}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => {
          return <FlatListItem items={item} indexs={index} />;
        }}
      />
    );
  };

  _renderSlideUp = () => {
    return (
      <SlidingUpPanel
        visible={true}
        allowDragging={this.state.allowDragging}
        startCollapsed={false}
        showBackdrop={false}
        ref={c => (this._panel = c)}
        draggableRange={this.props.draggableRange}
        onDrag={v => this._draggedValue.setValue(v)}
      >
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
            marginBottom: hp(h(50)),
            marginTop: hp(h(205))
          }}
        >
          <View
            style={{
              width: wp("90%"),
              height: hp(h(540)),
              // marginHorizontal: wp(w(45)),
              borderRadius: 15,
              backgroundColor: "rgb(255,255,255)",
              marginTop: hp(h(10)),
              marginBottom: hp(h(10))
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.setState({ visible: false });
                this.onLayout();
              }}
            >
              <View
                style={{
                  height: hp(h(50)),
                  backgroundColor: "rgb(249,249,249)",
                  borderTopLeftRadius: 15,
                  borderTopRightRadius: 15,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Image
                  source={Assets.arrowDown}
                  style={{
                    marginTop: hp(h(2)),
                    width: wp(w(20)),
                    height: hp(h(10))
                  }}
                />
                <Text
                  style={{
                    fontFamily: "Helvetica-Light",
                    fontSize: hp(h(12)),
                    color: "rgb(0,153,204)",
                    marginTop: hp(h(5))
                  }}
                >
                  PICKING UP FOOD
                </Text>
              </View>
            </TouchableOpacity>
            <View
              style={{
                flexDirection: "row",
                marginTop: hp(h(17)),
                marginBottom: hp(h(17))
              }}
            >
              <Image
                source={Assets.restoPurple}
                style={{
                  width: brand === "Apple" ? wp(w(31)) : wp(w(29)),
                  height: brand === "Apple" ? hp(h(25)) : hp(h(27)),
                  // tintColor: "rgb(102,0,153)",
                  marginLeft: wp(w(24)),
                  marginRight: wp(w(15)),
                  marginTop: hp(h(10)),
                  justifyContent: "center",
                  alignItems: "center",
                  paddingVertical: hp(h(5))
                }}
              />
              <View
                style={{
                  flexDirection: "column",
                  flex: 2,
                  marginRight: wp(w(20))
                }}
              >
                <Text
                  style={{
                    fontFamily: "Roboto-Medium",
                    fontSize: hp(h(18)),
                    color: "rgb(74,74,74)"
                  }}
                >
                  {this.props.orders.merchant_data.name}
                </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica-Light",
                    fontSize: hp(h(14)),
                    color: "rgb(155,155,155)"
                  }}
                >
                  {this.props.orders.merchant_data.address}
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    Communications.phonecall(
                      this.props.orders.merchant_data.phone,
                      true
                    )
                  }
                >
                  <Text
                    style={{
                      fontFamily: "Helvetica",
                      fontSize: hp(h(10)),
                      color: "rgb(74,144,226)",
                      marginTop: hp(h(5))
                    }}
                  >
                    CALL VENDOR
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                borderWidth: 0.3,
                borderTopColor: "rgb(151,151,151)",
                borderLeftColor: "transparent",
                borderRightColor: "transparent",
                borderBottomColor: "rgb(151,151,151)",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  fontFamily: "Roboto-Light",
                  fontSize: hp(h(12)),
                  color: "rgb(74,74,74)",
                  marginVertical: hp(h(5))
                }}
              >
                Transaction Number : {this.props.orders.id}
              </Text>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View
                style={{
                  marginLeft: wp(w(20)),
                  marginRight: wp(w(15)),
                  marginTop: hp(h(10))
                }}
              >
                {this._renderFlatList()}
              </View>
            </ScrollView>
            <View>
              <TouchableOpacity
                onPress={() => {
                  this._clickOrderDetail();
                }}
              >
                <Text
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: hp(h(10)),
                    color: "rgb(74,144,226)",
                    marginTop: hp(h(12)),
                    textAlign: "center"
                  }}
                >
                  DETAIL ORDER
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                marginTop: hp(h(41)),
                marginHorizontal: wp(w(24)),
                backgroundColor: "rgb(235,235,235)",
                borderRadius: 15
                // height: hp(h(45))
                //   flexDirection: "row"
              }}
            >
              <Slider
                value={this.props.valueSliderAccept}
                thumbImage={Assets.swipeArrow}
                thumbStyle={{
                  width: 58,
                  height: 39,
                  marginVertical: hp(h(10))
                }}
                thumbTintColor="transparent"
                minimumTrackTintColor="transparent"
                maximumTrackTintColor="transparent"
                minimumValue={this.state.sliderMinValue}
                maximumValue={this.state.sliderMaxValue}
                onValueChange={value => {
                  // console.log('value accept :', value);
                  this._valueChange(value);
                }}
                onSlidingComplete={value => {
                  this._completeChange(value);
                }}
                style={{
                  marginVertical: hp(h(5)),
                  marginHorizontal: wp(w(5)),
                  zIndex: 10
                  // position: "absolute"
                }}
              />
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  height: "100%",
                  position: "absolute",
                  width: wp(w(280)),
                  right: 0
                }}
              >
                <Text
                  style={{
                    fontFamily: "Helvetica-Light",
                    fontSize: hp(h(12)),
                    color: themes.gray_70
                  }}
                >
                  {this.state.labelSlide}
                </Text>
              </View>
            </View>
            <View
              style={{
                marginTop: hp(h(18)),
                backgroundColor: "rgb(249,249,249)",
                flexDirection: "row",
                height: hp(h(50)),
                borderBottomLeftRadius: 15,
                borderBottomRightRadius: 15
              }}
            >
              <View
                style={{
                  flexDirection: "column",
                  flex: 1,
                  marginLeft: wp(w(30))
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this._clickCancelOrder();
                  }}
                >
                  <Image
                    source={Assets.cancel}
                    style={{ marginTop: hp(h(5)), marginLeft: wp(w(25)) }}
                  />
                  <Text
                    style={{
                      fontFamily: "Helvetica",
                      fontSize: hp(h(10)),
                      color: "rgb(74,144,226)",
                      marginLeft: wp(w(20)),
                      marginTop: hp(h(4))
                    }}
                  >
                    CANCEL
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: "column",
                  flex: 1,
                  alignItems: "flex-end",
                  marginRight: wp(w(30))
                }}
              >
                <TouchableOpacity>
                  <Image
                    source={Assets.delay}
                    style={{ marginTop: hp(h(5)), marginLeft: wp(w(10)) }}
                  />
                  <Text
                    style={{
                      fontFamily: "Helvetica",
                      fontSize: hp(h(10)),
                      color: "rgb(74,144,226)",
                      marginRight: wp(w(20)),
                      marginTop: hp(h(4))
                    }}
                  >
                    DELAYED
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </SlidingUpPanel>
    );
  };

  _getDriverPosition = () => {
    var options = {
      // timeout: 5000
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
      distanceFilter: 0
    };
    this.watchIDOrder = navigator.geolocation.watchPosition(
      this._showLocation,
      this._errorLocation,
      options
    );
  };

  _showLocation = position => {
    var lat = parseFloat(position.coords.latitude);
    var long = parseFloat(position.coords.longitude);

    const params = {
      lat: lat,
      lng: long
    };
    this.props.actions.watchLocation(
      params,
      this._watchLocationSuccess,
      this._watchLocationError
    );
  };

  _errorLocation = error => {
    if (error.code == 1) {
      Alert.alert(appName, "Access is denied!");
    } else if (error.code == 2) {
      Alert.alert(appName, "Position is unavailable!");
    }
  };

  _watchLocationSuccess = data => {
    const { navigate } = this.props.navigation;
    if (data.active_order === null) {
      navigator.geolocation.clearWatch(this.watchIDOrder); // clear watch position after cancel
      Alert.alert(
        appName,
        "Order has been canceled location",
        [
          {
            text: "OK",
            onPress: () => {
              navigate("Home");
            }
          }
        ],
        { cancelable: false }
      );
    }
    console.log("_watchLocationSuccess : ", data);
  };

  _watchLocationError = error => {
    console.log("_watchLocationError :", error);
  };

  componentDidMount = async () => {
    await Orientation.lockToPortrait();

    // animationTimeout = setTimeout(() => {
    // this.focus1();
    // }, timeout);

    await this.onLayout();
    await Permissions.request("location", {
      rationale: {
        title: "Location Permission",
        message:
          "There was a problem getting your permission. " +
          "Please enable it from settings."
      }
    }).then(response => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      // this._getCurrentPosition();
      this._getDriverPosition();
      this.setState({ location: response });
    });
  };

  // focusMap = (markers, animated) => {
  //   if (Platform.OS !== "ios") {
  //     this.map.fitToSuppliedMarkers(
  //       markers,
  //       { edgePadding: { top: 20, right: 20, bottom: 20, left: 20 } },
  //       animated
  //     );
  //   }
  // };

  // focus1 = () => {
  //   animationTimeout = setTimeout(() => {
  //     this.focusMap([markerIDs[0], markerIDs[1]], true);
  //   }, timeout);
  // };

  onLayout = () => {
    setTimeout(() => {
      this.map.fitToCoordinates(
        [
          {
            latitude: parseFloat(this.props.location.lat),
            longitude: parseFloat(this.props.location.lng)
          },
          {
            latitude: parseFloat(this.props.orders.merchant_data.lat),
            longitude: parseFloat(this.props.orders.merchant_data.lng)
          }
        ],
        { edgePadding: DEFAULT_PADDING, animated: true }
      );
    }, 500);
  };

  render() {
    // console.log("CHECK :", this.props.orders.name);

    return (
      <View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          alignItems: "center"
        }}
      >
        <StatusBar hidden={true} />
        <MapView
          ref={map => (this.map = map)}
          region={{
            latitude: parseFloat(this.props.location.lat),
            longitude: parseFloat(this.props.location.lng),
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          }}
          showsMyLocationButton={false}
          style={styles.maps}
          zoomEnabled={true}
          loadingEnabled={true}
          loadingIndicatorColor="#606060"
          loadingBackgroundColor="#FFFFFF"
          moveOnMarkerPress={false}
          showsUserLocation={false}
          followUserLocation={
            false
          } /* This only works if showsUserLocation is true */
          showsCompass={true}
          showsPointsOfInterest={false}
          showsScale={true}
          // minZoomLevel={10}
          // maxZoomLevel={20}
          provider="google"
        >
          <Marker
            identifier="Marker1"
            coordinate={{
              latitude: parseFloat(this.props.location.lat),
              longitude: parseFloat(this.props.location.lng),
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA
            }}
            image={Assets.pin}
            key="1"
            title=""
            description=""
          />
          <Marker
            identifier="Marker2"
            coordinate={{
              latitude: parseFloat(this.props.orders.merchant_data.lat),
              longitude: parseFloat(this.props.orders.merchant_data.lng)
            }}
            image={Assets.restoPurple}
            key="2"
            title=""
            description=""
          />
          <MapViewDirections
            origin={{
              latitude: parseFloat(this.props.location.lat),
              longitude: parseFloat(this.props.location.lng),
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA
            }}
            // destination={this.state.restoDestination}
            destination={{
              latitude: parseFloat(this.props.orders.merchant_data.lat),
              longitude: parseFloat(this.props.orders.merchant_data.lng)
            }}
            apikey={GOOGLE_MAPS_APIKEY}
            strokeWidth={3}
            strokeColor="rgb(102,0,153)"
            resetOnChange={false}
            onError={errorMessage => {
              console.log("GOT AN ERROR", errorMessage);
            }}
          />
        </MapView>

        {!this.state.modalVisibleCancel &&
          !this.state.modalVisibleCancelFinish && (
            <View
              style={{
                paddingTop: Platform.OS === "ios" ? 24 : 0
              }}
            >
              <HeaderCustomer data={this.props.orders} />
            </View>
          )}

        {this.state.visible &&
        (!this.state.modalVisibleCancel &&
          !this.state.modalVisibleCancelFinish) ? (
          this._renderSlideUp()
        ) : (
          <View
            style={{
              width: wp("90%"),
              // height: hp(h(149)),
              marginHorizontal: wp(w(45)),
              borderRadius: 15,
              backgroundColor: themes.white,
              marginTop: hp(h(395)),
              marginBottom: hp(h(10))
            }}
          >
            {!this.state.modalVisibleCancel &&
              !this.state.modalVisibleCancelFinish && (
                <View style={{}}>
                  <TouchableOpacity
                    onPress={() => this.setState({ visible: true })}
                  >
                    <View
                      style={{
                        height: hp(h(50)),
                        backgroundColor: themes.white_10,
                        borderTopLeftRadius: 15,
                        borderTopRightRadius: 15,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Image
                        source={Assets.arrowUp}
                        style={{
                          marginTop: 2
                        }}
                      />
                      <Text
                        style={{
                          fontFamily: "Helvetica-Light",
                          fontSize: 12,
                          color: "rgb(0,153,204)",
                          marginTop: 5
                        }}
                      >
                        PICKING UP FOOD
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: 17,
                      marginBottom: 17
                    }}
                  >
                    <Image
                      source={Assets.restoPurple}
                      style={{
                        width: 23,
                        height: 23,
                        // tintColor: "rgb(102,0,153)",
                        marginHorizontal: 24,
                        marginTop: 10,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    />
                    <View
                      style={{
                        flexDirection: "column",
                        flex: 2,
                        marginRight: wp(w(20))
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: "Roboto-Medium",
                          fontSize: 18,
                          color: "rgb(74,74,74)"
                        }}
                      >
                        {this.props.orders.merchant_data.name}
                      </Text>
                      <Text
                        style={{
                          fontFamily: "Helvetica-Light",
                          fontSize: 14,
                          color: "rgb(155,155,155)"
                        }}
                      >
                        {this.props.orders.merchant_data.address}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      borderWidth: 0.5,
                      borderTopColor: "rgb(151,151,151)",
                      borderLeftColor: "transparent",
                      borderRightColor: "transparent",
                      borderBottomColor: "transparent",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                      paddingVertical: hp(h(5))
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Roboto-Light",
                        fontSize: 12,
                        color: "rgb(74,74,74)"
                      }}
                    >
                      Transaction Number : {this.props.orders.id}
                    </Text>
                  </View>
                </View>
              )}
          </View>
        )}
        <ModalDetailOrder
          modalVisible={this.state.modalVisible}
          handleClose={this._closeModal}
          userInfo={this.props.userInfo}
          data={this.props.orders}
          statusOrder={this.state.statusOrder}
        />

        <ModalCancelOrder
          modalVisibleCancel={this.state.modalVisibleCancel}
          userInfo={this.props.userInfo}
          value={this.props.valueSliderAccept}
          handleCloseCancel={this._closeModalCancel}
          handleAcceptCancel={this._acceptModalCancel}
          handleChangeCancelValue={this._changeModalCancelValue}
          data={this.props.orders}
          selectedRadioCancel={this.props.actions.selectedRadioCancel}
          selectedRadio={this.props.selectedRadio}
          sliderMaxValue={this.state.sliderMaxValue}
          sliderMinValue={this.state.sliderMinValue}
        />

        <ModalCancelFinishOrder
          modalVisibleCancelFinish={this.state.modalVisibleCancelFinish}
          userInfo={this.props.userInfo}
          handleCloseCancelFinish={this._closeModalCancelFinish}
          data={this.props.orders}
          selectedRadio={this.props.selectedRadio}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  maps: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1
  }
});
