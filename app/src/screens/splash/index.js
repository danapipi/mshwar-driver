/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-06 11:26:43 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-29 11:24:01
 */

import React, { Component } from 'react'
import {
    View,
    Text,
    ImageBackground
} from 'react-native'
import Assets from "../../../assets"
import { getToken, clearToken } from '../../../utils/storage'
import { NavigationActions, StackActions } from 'react-navigation'
import { setData, getData, removeData } from '../../../utils/storage'
import { keys } from '../../../config/keys'
import { STATUS_ORDER } from '../../../config'
import Orientation from 'react-native-orientation';


const { width, height } = require('Dimensions').get('window');

export default class SplashScreen extends Component {
    constructor(props){
        super(props);
    }

    componentDidMount = () => {
        // setTimeout(() => {
            this._checkLoginStatus();
            Orientation.lockToPortrait();

        // }, 500);
    }

    _onSuccess = data => {
        // console.log('data success : ',data)
        const { navigation } = this.props
        let routeName = 'Login'
        if(data){
            try {
                getData(keys.activeOrder)
                    .then((res) => {
                        if (res !== '') {
                            if(this.props.orders.status_text.toLowerCase() === STATUS_ORDER.asign) {
                                routeName = 'Order'
                            } else if (this.props.orders.status_text.toLowerCase() === STATUS_ORDER.accept) {
                                routeName = 'OrderDetail'
                            } else if (this.props.orders.status_text.toLowerCase() === STATUS_ORDER.ready) {
                                routeName = 'Camera'
                            } else if (this.props.orders.status_text.toLowerCase() === STATUS_ORDER.delivering) {
                                routeName = 'FinishOrder'
                            } else if (this.props.orders.status_text.toLowerCase() === STATUS_ORDER.delivered) {
                                routeName = 'KeyCustomer'
                            } else {
                                routeName = 'Order'
                            }
                        } else { // if driver haven't order
                            routeName = 'Home'
                        };

                        setTimeout(() => {
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [
                                    NavigationActions.navigate({routeName})
                                ]
                            })
                            navigation.dispatch(resetAction)
                        }, 300) 
                    })
            } catch (error) {
                console.log('Error get data storage : ', JSON.stringify(error))
            }
        }
            
           
    }

    _onError = error => {
        // console.log('error splash :',error)
        try {
            clearToken();
            const { navigation } = this.props
            const resetAction = StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({routeName: 'Login'})
                ]
            })
            navigation.dispatch(resetAction)
        } catch (err) {
            this.setState({

            });
        }
    }

    _checkLoginStatus = () => {
        getToken().then((token) => {
            const params = {
                token   : token
            }
            if(token && token.length > 0) {
                this.props.actions.fetchUserInfo(params, this._onSuccess, this._onError)
            } else {
                const { navigation } = this.props
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({routeName: 'Login'})
                    ]
                })
                navigation.dispatch(resetAction)
            }
        })
    }

    render(){
        return(
            <ImageBackground
                    source={Assets.background}
                    style={{ 
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1
                    }}
                >
            </ImageBackground>
        )
    }
}