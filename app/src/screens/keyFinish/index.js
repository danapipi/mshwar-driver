/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-10-29 12:34:17
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-30 15:09:08
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  TextInput,
  Dimensions,
  Alert,
  Keyboard,
  Platform
} from "react-native";
import MapView, { Marker } from "react-native-maps";
import Assets from "../../../assets";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h
} from "../../../config/layout";
import { keys } from "../../../config/keys";
import { removeData } from "../../../utils/storage";
import Permissions from "react-native-permissions";
import { themes } from "../../../config/themes";
import { name as appName } from "../../../../app/app.json";
import Orientation from 'react-native-orientation';


const { width, height } = Dimensions.get("window");
const LATITUDE_DELTA = 0.0222;
const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);
const map = null;

export default class KeyScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: {},
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0
      },
      markerPosition: {
        latitude: 0,
        longitude: 0
      },
      location: "",
      currentAddress: null,
      showKeyboard: false,
      statusDriver: "inactive",
      key1: "",
      key2: "",
      key3: "",
      key4: ""
    };
  }

  _submit = () => {
    const pinCustomer = `${this.state.key1}${this.state.key2}${
      this.state.key3
    }${this.state.key4}`;
    const params = {
      pin: pinCustomer
    };

    if (
      this.state.key1 == "" ||
      this.state.key2 == "" ||
      this.state.key3 == "" ||
      this.state.key4 == ""
    ) {
      Alert.alert(appName, "Input can not be blank");
    } else {
      this.props.actions.submitPinCustomer(
        params,
        this._submitSuccess,
        this._submitFailed
      );
    }
  };

  _submitSuccess = data => {
    if (data.success) {
      try {
        removeData(keys.activeOrder).then(async (res) => {
          if (res) {
            const { navigate } = this.props.navigation;
            await Alert.alert(
              appName,
              data.message,
              [{ text: "OK", onPress: () => navigate("Home") }],
              { cancelable: false }
            );
          }
        });
      } catch (error) {
        console.log("error remove storage", error);
      }
    } else {
      Alert.alert(
        appName,
        data.message,
        [{ text: "OK", onPress: () => console.log("OK") }],
        { cancelable: false }
      );
    }
  };

  _submitFailed = error => {
    console.log("Error : ", error);
  };

  _backTo = () => {
    const { navigate } = this.props.navigation;
    navigate("FinishOrder");
  };

  _handleKeyboardShow = event => {
    this.setState({
      showKeyboard: true
    });
  };

  _handleKeyboardHide = event => {
    this.setState({
      showKeyboard: false
    });
  };

  componentDidMount =  () => {
    Orientation.lockToPortrait();

    // set default focus
    setTimeout(() => {
      this.key1.focus();
      if(Platform.OS === 'ios') {
        this.keyboardWillShowSub =  Keyboard.addListener(
          "keyboardWillShow",
          this._handleKeyboardShow
        );
        this.keyboardWillHideSub =  Keyboard.addListener(
          "keyboardWillHide",
          this._handleKeyboardHide
        );
      } else {
        this.keyboardWillShowSub =  Keyboard.addListener(
          "keyboardDidShow",
          this._handleKeyboardShow
        );
        this.keyboardWillHideSub =  Keyboard.addListener(
          "keyboardDidHide",
          this._handleKeyboardHide
        );
      }
    }, 1000);   
  };

  componentWillUnmount =  () => {
    // if(Platform.OS === 'android') {
      setTimeout(() => {
         this.keyboardWillShowSub.remove();
         this.keyboardWillHideSub.remove();
      }, 1000);  
    // }
  };

  render() {
    return (
      <View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          alignItems: "center"
        }}
      >
        <StatusBar hidden={true} />
        <MapView
          ref={map => (this.map = map)}
          region={{
            latitude: parseFloat(this.props.location.lat),
            longitude: parseFloat(this.props.location.lng),
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          }}
          showsMyLocationButton={false}
          style={styles.maps}
          zoomEnabled={true}
          loadingEnabled={true}
          loadingIndicatorColor="#606060"
          loadingBackgroundColor="#FFFFFF"
          moveOnMarkerPress={false}
          showsUserLocation={false}
          followUserLocation={
            false
          } /* This only works if showsUserLocation is true */
          showsCompass={true}
          showsPointsOfInterest={false}
          showsScale={true}
          minZoomLevel={15}
          maxZoomLevel={20}
          provider="google"
        >
          <MapView.Marker
            image={Assets.pin}
            key="1"
            title=""
            description=""
            coordinate={{
              latitude: parseFloat(this.props.location.lat),
              longitude: parseFloat(this.props.location.lng),
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA
            }}
          />
        </MapView>
        <ImageBackground
          source={Assets.background}
          style={{
            width: wp("100%"),
            height: hp("100%")
          }}
        >
          <View
            style={{
              flex: 1,
              paddingTop: Platform.OS === 'ios' ? 24 : 0,
              // borderWidth: 1
            }}
          >
            <TouchableOpacity
              onPress={() => this._backTo()}
              style={{ flexDirection: "row", justifyContent: "flex-end" }}
            >
              <Text
                style={{
                  marginTop: hp(h(20)),
                  marginRight: wp(w(10)),
                  fontFamily: "Helvetica-Light",
                  fontSize: hp(h(12)),
                  color: themes.white
                }}
              >
                CANCEL
              </Text>
              <Image
                source={Assets.cancelLogin}
                style={{
                  height: 24,
                  width: 24,
                  marginTop: hp(h(17)),
                  marginRight: wp(w(13))
                }}
              />
            </TouchableOpacity>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                marginTop: hp(h(35))
              }}
            >
              <Image
                source={Assets.photoDriver}
                style={{ width: 39, height: 39 }}
              />
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: hp(h(24)),
                  color: "rgb(255,255,255)",
                  marginLeft: wp(w(15))
                }}
              >
                {this.props.orders.name}
              </Text>
            </View>
            <View
              style={{
                marginTop: hp(h(17)),
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica-Light",
                  fontSize: hp(h(18)),
                  color: themes.white
                }}
              >
                PLEASE ENTER YOUR
              </Text>
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: hp(h(18)),
                  color: themes.white
                }}
              >
                UNIQUE PIN
              </Text>
            </View>
            <View
              style={[
                {
                  color: themes.white,
                  marginLeft: wp(w(75)),
                  marginRight: wp(w(69)),
                  flexDirection: "row"
                },
                this.state.showKeyboard
                  ? { marginTop: hp(h(10)) }
                  : { marginTop: hp(h(109)) }
              ]}
            >
              <TextInput
                ref={ref => {
                  this.key1 = ref;
                }}
                maxLength={1}
                onChangeText={text => {
                  if (text) {
                    this.setState({ key1: text });
                    this.key2.focus();
                  }
                }}
                onBlur={() => {}}
                keyboardType="number-pad"
                style={{
                  marginTop: hp(h(35)),
                  backgroundColor: themes.white,
                  width: wp(w(53)),
                  height: hp(h(66)),
                  borderRadius: 15,
                  fontSize: hp(h(18)),
                  fontFamily: "Helvetica",
                  textAlign: "center"
                }}
              />
              <TextInput
                ref={ref => {
                  this.key2 = ref;
                }}
                maxLength={1}
                onChangeText={text => {
                  if (text) {
                    this.setState({ key2: text });
                    this.key3.focus();
                  }
                }}
                onKeyPress={({ nativeEvent }) => {
                  if (nativeEvent.key === "Backspace") {
                    this.key1.focus();
                  }
                }}
                onBlur={() => {}}
                keyboardType="number-pad"
                style={{
                  marginTop: hp(h(35)),
                  backgroundColor: themes.white,
                  width: wp(w(53)),
                  height: hp(h(66)),
                  borderRadius: 15,
                  marginLeft: wp(w(17)),
                  fontSize: hp(h(18)),
                  fontFamily: "Helvetica",
                  textAlign: "center"
                }}
              />
              <TextInput
                ref={ref => {
                  this.key3 = ref;
                }}
                maxLength={1}
                onChangeText={text => {
                  if (text) {
                    this.setState({ key3: text });
                    this.key4.focus();
                  }
                }}
                onKeyPress={({ nativeEvent }) => {
                  if (nativeEvent.key === "Backspace") {
                    this.key2.focus();
                  }
                }}
                onBlur={() => {}}
                keyboardType="number-pad"
                style={{
                  marginTop: hp(h(35)),
                  backgroundColor: themes.white,
                  width: wp(w(53)),
                  height: hp(h(66)),
                  borderRadius: 15,
                  marginLeft: wp(w(17)),
                  fontSize: hp(h(18)),
                  fontFamily: "Helvetica",
                  textAlign: "center"
                }}
              />
              <TextInput
                ref={ref => {
                  this.key4 = ref;
                }}
                maxLength={1}
                onChangeText={text => {
                  if (text) {
                    this.setState({ key4: text });
                  }
                }}
                onKeyPress={({ nativeEvent }) => {
                  console.log("nativeEvent : ", nativeEvent);
                  if (nativeEvent.key === "Backspace") {
                    this.key3.focus();
                  }
                }}
                onBlur={() => {}}
                keyboardType="number-pad"
                style={{
                  marginTop: hp(h(35)),
                  backgroundColor: themes.white,
                  width: wp(w(53)),
                  height: hp(h(66)),
                  borderRadius: 15,
                  marginLeft: wp(w(17)),
                  fontSize: hp(h(18)),
                  fontFamily: "Helvetica",
                  textAlign: "center"
                }}
              />
            </View>
            <View
              style={[
                {
                  justifyContent: "center",
                  alignItems: "center"
                },
                this.state.showKeyboard
                  ? { marginTop: hp(h(20)) }
                  : { marginTop: hp(h(180)) }
              ]}
            >
              <TouchableOpacity
                style={{
                  borderRadius: 15,
                  width: wp(w(280)),
                  height: hp(h(46)),
                  backgroundColor: themes.gray_30,
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: "center"
                  // borderWidth: 1
                }}
                onPress={() => {
                  if (!this.props.loading) {
                    this._submit();
                  }
                }}
              >
                {this.props.loading && (
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      fontSize: hp(h(14)),
                      color: "rgb(0,153,204)"
                    }}
                  >
                    LOADING...
                  </Text>
                )}

                {!this.props.loading && (
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      fontSize: hp(h(14)),
                      color: "rgb(0,153,204)"
                    }}
                  >
                    SUBMIT
                  </Text>
                )}
              </TouchableOpacity>
            </View>
            <View>
              <Text
                style={{
                  fontFamily: "Roboto-Regular",
                  fontSize: hp(h(13)),
                  color: themes.white,
                  marginVertical: hp(h(13)),
                  textAlign: "center"
                }}
              >
                Transaction Number : {this.props.orders.id}
              </Text>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  maps: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1
  }
});
