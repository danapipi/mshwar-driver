/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-10-29 12:34:17
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-12-03 16:43:28
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  Switch,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  Animated,
  TextInput,
  StyleSheet,
  Dimensions,
  Keyboard,
  Alert,
  Platform,
  ImageEditor,
  ActivityIndicator
} from "react-native";
import MapView, { Marker } from "react-native-maps";
import Assets from "../../../assets";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h
} from "../../../config/layout";
import Permissions from "react-native-permissions";
import Slider from "react-native-slider";
import ImagePicker from "react-native-image-crop-picker";
import { themes } from "../../../config/themes";
import Communications from "react-native-communications";
import { getData, removeData } from "../../../utils/storage";
import { keys } from "../../../config/keys";
import { name as appName } from "../../../../app/app.json";
import axios from "axios";
import Orientation from "react-native-orientation";
import HeaderCustomer from "../../../components/headerCustomer";

const { width, height } = Dimensions.get("window");
const LATITUDE_DELTA = 0.0222;
const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);
const map = null;

export default class CameraScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: {},
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0
      },
      markerPosition: {
        latitude: 0,
        longitude: 0
      },
      currentAddress: null,
      statusDriver: "inactive",
      location: "",
      cameraStatus: false,
      image: null,
      images: null,
      price: "",
      isLoading: false,
      labelSlide: "READY TO DELIVER",
      showKeyboard: false,
      // restoDestination: {
      //   latitude: 0,
      //   longitude: 0,
      //   latitudeDelta: 0,
      //   longitudeDelta: 0
      // },
      // destination: {
      //   latitude: 0,
      //   longitude: 0
      // },
      sliderMinValue: 3,
      sliderMaxValue: 20,
      dataImage: null,
      loadImageCapture: true
    };
  }

  _handleKeyboardShow = event => {
    // this.txtInputTotalPrice.focus()
    this.setState({
      showKeyboard: true
    });
  };

  _handleKeyboardHide = event => {
    this.setState({
      showKeyboard: false
    });
  };

  // _slideComplete = () => {
  //   const { navigate } = this.props.navigation;
  //   navigate("FinishOrder");
  // };

  _valueChange = value => {
    if (value === this.state.sliderMaxValue) {
      const params = {
        status: "delivering"
      };
      this.props.actions.swipeStatusOrder(
        params,
        this._swipeStatusOrderSuccess,
        this._swipeStatusOrderFailed
      );
      this.setState({
        labelSlide: "READY TO DELIVER"
      });
    } else {
      this.setState({
        labelSlide: ""
      });
      this.props.actions.swipeStatusOrderUpdate(Math.floor(value));
    }
  };

  _completeChange = value => {
    if (Math.floor(value) < this.state.sliderMaxValue) {
      this.setState({
        labelSlide: "READY TO DELIVER"
      });
      this.props.actions.swipeStatusOrderUpdate(this.state.sliderMinValue);
    }
  };

  _swipeStatusOrderSuccess = data => {
    console.log("_swipeStatusOrderSuccess", data);
    this.props.actions.swipeStatusOrderUpdate(this.state.sliderMaxValue);
    try {
      getData(keys.activeOrder).then(res => {
        console.log("res", res);
        if (res !== "") {
          // if not empty order_id in storage
          this.props.actions.fetchOrderbyId(
            res,
            this._fetchOrderByIdSuccess,
            this._fetchOrderByIdFailed
          );
        } else {
          Alert.alert(
            appName,
            "Order has been canceled",
            [
              {
                text: "OK",
                onPress: () => {
                  const { navigate } = this.props.navigation;
                  navigate("Home");
                }
              }
            ],
            { cancelable: false }
          );
        }
      });
    } catch (error) {
      console.log("get data storage :", error);
    }
  };

  _swipeStatusOrderFailed = error => {
    console.log("_swipeStatusOrderFailed", error);
    this.props.actions.swipeStatusOrderUpdate(this.state.sliderMinValue);
  };

  _fetchOrderByIdSuccess = async data => {
    const { navigate } = this.props.navigation;
    if (Object.keys(data).length === 0) {
      await Alert.alert(
        appName,
        "Order has been canceled",
        [
          {
            text: "OK",
            onPress: () => {
              navigate("Home");
            }
          }
        ],
        { cancelable: false }
      );
    } else {
      await this.props.actions.swipeStatusOrderUpdate(
        this.state.sliderMinValue
      );
      await ImagePicker.clean()
        .then(() => {
          console.log("removed all tmp images from tmp directory");
          navigate("FinishOrder");
        })
        .catch(e => {
          alert(e);
        });
    }
  };

  _fetchOrderByIdFailed = error => {
    try {
      removeData(keys.activeOrder).then(res => {
        Alert.alert(
          appName,
          "Order has been canceled",
          [
            {
              text: "OK",
              onPress: () => {
                const { navigate } = this.props.navigation;
                navigate("Home");
              }
            }
          ],
          { cancelable: false }
        );
      });
    } catch (error) {
      console.log("error remove storage", error);
    }
  };

  _updatePrice = text => {
    this.setState({ price: text });
  };

  // _postPhoto = async () => {

  //   let url = `https://admin.mshwarapp.com/api/driver/receipt`;

  //   let formData = new FormData();
  //   formData.append("image", {
  //     uri: this.state.image.uri,
  //     type: this.state.image.mime, // or photo.type
  //     name: "testPhotoName"
  //   });
  //   formData.append("total", this.state.price);

  //   console.log("form:", formData);
  //   const { data } = await axios.post(url, formData, {
  //     headers: {
  //       "Content-Type": "multipart/form-data"
  //     }
  //   });
  //   console.log("data", data, data.data.image);
  //   this.setState({ cameraStatus: true, dataImage: data.data.image });
  // };

  _sendReceipt = () => {
    let formData = new FormData();
    formData.append("image", {
      uri: this.state.image.uri,
      type: this.state.image.mime, // or photo.type
      name: "testPhotoName"
    });
    formData.append("total", this.state.price);

    this.props.actions.sendReceipt(formData, this._onSuccess, this._onError);
  };

  _onSuccess = data => {
    // console.log("send receipt success : ", data);
    this.setState({ cameraStatus: true });
  };

  _onError = error => {
    // console.log("send receipt error : ", error);
    this.setState({ cameraStatus: false });
  };

  _layoutSlideInput = () => {
    return (
      <View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          alignItems: "center"
        }}
      >
        <View
          style={{
            paddingTop: Platform.OS === "ios" ? 24 : 0
          }}
        >
          <HeaderCustomer data={this.props.orders} />
        </View>

        <View
          style={{
            width: wp("90%"),
            height: hp(h(534)),
            // marginHorizontal: wp(w(45)),
            borderRadius: 15,
            backgroundColor: themes.white,
            marginTop: hp(h(100))
            // marginBottom: hp(h(20))
          }}
        >
          <View
            style={{
              // height: hp(h(70)),
              backgroundColor: themes.white_10,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              // justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              flex: 1,
              paddingVertical: hp(h(5))
            }}
          >
            <Text
              style={{
                fontFamily: "Roboto-Medium",
                fontSize: hp(h(18)),
                color: "rgb(74,74,74)",
                marginTop: hp(h(5)),
                flex: 6,
                textAlign: "center",
                marginLeft: wp(w(15)),
                paddingLeft: wp(w(30))
                // justifyContent: 'center'
              }}
            >
              Capture Receipt
            </Text>
            <TouchableOpacity
              style={{ flex: 1 }}
              onPress={() => {
                // const { navigate } = this.props.navigation;
                // navigate("OrderDetail");
                ImagePicker.clean()
                  .then(() => {
                    console.log("removed all tmp images from tmp directory");
                    this.setState({ cameraStatus: false, image: null });
                  })
                  .catch(e => {
                    alert(e);
                  });
              }}
            >
              <Image
                source={Assets.retake}
                style={{
                  height: hp(h(18)),
                  width: wp(w(18)),
                  justifyContent: "center",
                  marginTop: hp(h(5)),
                  resizeMode: "center"
                }}
              />
            </TouchableOpacity>
          </View>
          {/* <View
            style={{
              height: hp(h(359)),
              // width: wp('80%'),
              // backgroundColor: "rgb(74,74,74)",
              // marginHorizontal: wp(w(15)),
              marginTop: hp(h(15)),
              justifyContent: "center",
              alignItems: "center"
              // opacity: 0.1
            }}
          > */}
          {this.props.data.image === "" && (
            <View>
              <ActivityIndicator size="large" color={themes.mainColor} />
            </View>
          )}
          {this.props.data.image !== "" && (
            <View
              style={{
                height: hp(h(359)),
                marginTop: hp(h(15)),
                justifyContent: "center",
                alignItems: "center"
                // opacity: 0.1
              }}
            >
              <Image
                source={
                  this.state.loadImageCapture
                    ? Assets.searchPurple
                    : 
                    {
                        uri: `https://admin.mshwarapp.com/${
                          this.props.data.image
                        }`
                    }
                }
                onLoadStart={() => {
                  // alert('load start')
                }}
                onLoadEnd={() => {
                  this.setState({
                    loadImageCapture: false
                  });
                }}
                style={
                  this.state.loadImageCapture
                    ? {
                        height: hp(h(75)),
                        width: wp(w(75)),
                        // resizeMode: "contain",
                        borderRadius: 15
                      }
                    : {
                        height: hp(h(359)),
                        width: wp("75%"),
                        resizeMode: "cover",
                        borderRadius: 15
                      }
                }
              />
            </View>
          )}

          {/* </View> */}

          <View
            style={{
              marginVertical: hp(h(10))
            }}
          >
            <View
              style={{
                flexDirection: "row",
                // justifyContent: 'center',
                alignItems: "center"
                // marginTop: hp(h(5)),
                // marginBottom: hp(h(8))
              }}
            >
              <View
                style={{
                  flex: 2
                }}
              >
                <Image
                  source={Assets.qrInput}
                  style={{
                    width: wp(w(29)),
                    height: hp(h(22)),
                    marginLeft: wp(w(34)),
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: "Roboto-Medium",
                  fontSize: hp(h(14)),
                  color: "rgb(155,155,155)",
                  paddingLeft: wp(w(10)),
                  flex: 4
                }}
              >
                Total Price
              </Text>
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: hp(h(18)),
                  color: themes.gray,
                  marginRight: wp(w(34)),
                  // marginBottom: hp(h(14)),
                  // marginLeft: wp(w(20))
                  textAlign: "right",
                  // marginRight: wp(w(50)),
                  flex: 4
                }}
              >
                {this.state.price}
              </Text>
            </View>
          </View>

          <View
            style={{
              marginHorizontal: wp(w(15)),
              // marginTop: hp(h(5)),
              marginBottom: hp(h(10)),
              backgroundColor: "rgb(235,235,235)",
              borderRadius: 15
              // height: hp(h(41))
              //   flexDirection: "row"
            }}
          >
            <Slider
              value={this.props.valueSliderAccept}
              thumbImage={Assets.swipeArrow}
              thumbStyle={{ width: 58, height: 39, marginVertical: hp(h(10)) }}
              thumbTintColor="transparent"
              minimumTrackTintColor="transparent"
              maximumTrackTintColor="transparent"
              minimumValue={this.state.sliderMinValue}
              maximumValue={this.state.sliderMaxValue}
              onValueChange={value => {
                // console.log('value accept :', value);
                this._valueChange(value);
              }}
              onSlidingComplete={value => {
                this._completeChange(value);
              }}
              style={{
                marginVertical: hp(h(5)),
                marginHorizontal: wp(w(5)),
                zIndex: 10
              }}
            />
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                height: "100%",
                position: "absolute",
                width: wp(w(280)),
                right: 0
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica-Light",
                  fontSize: hp(h(12)),
                  color: themes.gray_70
                }}
              >
                {this.state.labelSlide}
              </Text>
            </View>
          </View>

          <View
            style={{
              borderWidth: 0.3,
              borderTopColor: themes.border,
              borderLeftColor: "transparent",
              borderRightColor: "transparent",
              borderBottomColor: "transparent",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                fontFamily: "Roboto-Light",
                fontSize: hp(h(12)),
                color: "rgb(74,74,74)",
                marginVertical: hp(h(5))
              }}
            >
              Transaction Number : {this.props.orders.id}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  _layoutTakePicture = () => {
    return (
      <View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          alignItems: "center"
        }}
      >
        <View
          style={{
            paddingTop: Platform.OS === "ios" ? 24 : 0
          }}
        >
          <HeaderCustomer data={this.props.orders} />
        </View>
        <View
          style={{
            width: wp("90%"),
            height: hp(h(544)),
            // marginHorizontal: wp(w(45)),
            borderRadius: 15,
            backgroundColor: themes.white,
            marginTop: hp(h(100))
            // marginBottom: hp(h(20))
          }}
        >
          <View
            style={{
              height: hp(h(50)),
              backgroundColor: themes.white_10,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              // justifyContent: "center",
              alignItems: "center",
              flexDirection: "row"
              // flex: 1,
            }}
          >
            <Text
              style={{
                fontFamily: "Roboto-Medium",
                fontSize: hp(h(18)),
                color: "rgb(74,74,74)",
                marginTop: hp(h(5)),
                flex: 6,
                textAlign: "center",
                marginLeft: wp(w(15)),
                paddingLeft: wp(w(30))
                // justifyContent: 'center'
              }}
            >
              Capture Receipt
            </Text>
            <TouchableOpacity
              onPress={() => {
                const { navigate } = this.props.navigation;
                navigate("OrderDetail");
              }}
              style={{ flex: 1 }}
            >
              <Image
                source={Assets.close}
                style={{
                  height: hp(h(18)),
                  width: wp(w(18)),
                  justifyContent: "center",
                  marginTop: hp(h(5)),
                  resizeMode: "center"
                }}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{
              justifyContent: "center",
              alignItems: "center"
            }}
            onPress={() => this._pickWithCamera()}
          >
            <View
              style={{
                height: hp(h(394)),
                width: wp("80%"),
                backgroundColor: "rgb(74,74,74)",
                marginVertical: hp(h(5)),
                marginHorizontal: wp(w(15)),
                justifyContent: "center",
                borderRadius: 15
                // opacity: 0.1
              }}
            >
              <Image
                source={Assets.noImage}
                // source={Assets.searchPurple}
                style={{
                  justifyContent: "center",
                  alignSelf: "center",
                  height: hp(h(100)),
                  width: wp(w(100)),
                  marginBottom: hp(h(10))
                }}
              />
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: hp(h(18)),
                  color: "rgb(255,255,255)",
                  textAlign: "center"
                }}
              >
                UPLOAD RECEIPT HERE
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this._pickWithCamera()}>
            <Image
              source={Assets.cameraBtn}
              style={{
                marginHorizontal: wp(w(20)),
                marginBottom: hp(h(10)),
                width: wp("80%"),
                borderRadius: 15
              }}
            />
          </TouchableOpacity>

          <View
            style={{
              borderWidth: 0.3,
              borderTopColor: themes.border,
              borderLeftColor: "transparent",
              borderRightColor: "transparent",
              borderBottomColor: "transparent",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                fontFamily: "Roboto-Light",
                fontSize: hp(h(12)),
                color: "rgb(74,74,74)",
                marginVertical: hp(h(5))
              }}
            >
              Transaction Number : {this.props.orders.id}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  _layoutInputPrice = () => {
    return (
      <View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          alignItems: "center"
        }}
      >
        <View
          style={{
            paddingTop: Platform.OS === "ios" ? 24 : 0
          }}
        >
          <HeaderCustomer data={this.props.orders} />
        </View>
        {/* <KeyboardAvoidingView
          behavior="padding" 
          enabled> */}

        <View
          style={[
            {
              width: wp("90%"),
              height: hp(h(288)),
              // marginHorizontal: wp(w(45)),
              borderRadius: 15,
              backgroundColor: "rgb(255,255,255)",
              marginBottom: hp(h(10))
            },
            this.state.showKeyboard
              ? Platform.OS === "ios"
                ? { marginTop: hp(h(130)) }
                : { marginTop: hp(h(180)) }
              : Platform.OS === "ios"
              ? { marginTop: hp(h(330)) }
              : { marginTop: hp(h(380)) }
          ]}
        >
          <View
            style={{
              height: hp(h(50)),
              backgroundColor: "rgb(249,249,249)",
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                fontFamily: "Roboto-Medium",
                fontSize: hp(h(18)),
                color: "rgb(74,74,74)",
                marginTop: hp(h(5))
              }}
            >
              Input total price
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              marginTop: hp(h(17)),
              marginBottom: hp(h(17))
            }}
          >
            <Image
              source={Assets.qrInput}
              style={{
                width: wp(w(30)),
                height: hp(h(23)),
                // tintColor: "rgb(102,0,153)",
                marginHorizontal: wp(w(24)),
                marginTop: hp(h(10)),
                justifyContent: "center",
                alignItems: "center"
              }}
            />
            <View style={{ flexDirection: "column" }}>
              <Text
                style={{
                  fontFamily: "Roboto-Medium",
                  fontSize: hp(h(18)),
                  color: "rgb(155,155,155)"
                }}
              >
                Estimate Price
              </Text>
              <Text
                style={{
                  fontFamily: "Helvetica-Light",
                  fontSize: hp(h(14)),
                  color: "rgb(155,155,155)"
                }}
              >
                {this.props.orders.display_price}
              </Text>
            </View>
          </View>
          <View
            style={{
              borderBottomColor: !this.props.loading
                ? "rgb(151,151,151)"
                : "transparent",
              borderTopColor: "transparent",
              borderLeftColor: "transparent",
              borderRightColor: "transparent",
              borderWidth: 0.3,
              flexDirection: "row",
              marginLeft: wp(w(78)),
              marginRight: wp(w(30)),
              // height: 25
              marginBottom: hp(h(25)),
              flex: 1
            }}
          >
            <Text
              style={{
                fontFamily: "Helvetica",
                fontSize: hp(h(14)),
                marginTop: hp(h(17)),
                flex: 0.6
              }}
            >
              QR
            </Text>
            <TextInput
              ref={ref => {
                this.txtInputTotalPrice = ref;
              }}
              keyboardType="numeric"
              style={{
                fontFamily: "Helvetica",
                fontSize: hp(h(14)),
                marginLeft: wp(w(10)),
                flex: 5,
                marginTop: hp(h(5))
                // width: wp(w(50))
              }}
              onSubmitEditing={event =>
                this._updatePrice(event.nativeEvent.text)
              }
              onChangeText={text => this._updatePrice(text)}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => {
                if (!this.props.loading) {
                  this._sendReceipt();
                }
              }}
            >
              {this.props.loading && (
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <ActivityIndicator size="large" color={themes.mainColor} />
                </View>
              )}

              {!this.props.loading && (
                <Image
                  source={Assets.uploadReceipt}
                  style={{
                    width: wp("80%"),
                    height: hp(h(46)),
                    marginBottom: hp(h(15))
                  }}
                />
              )}
            </TouchableOpacity>
          </View>
          <View
            style={{
              borderWidth: 0.3,
              borderTopColor: themes.border,
              borderLeftColor: "transparent",
              borderRightColor: "transparent",
              borderBottomColor: "transparent",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              paddingVertical: hp(h(5))
            }}
          >
            <Text
              style={{
                fontFamily: "Roboto-Light",
                fontSize: hp(h(12)),
                color: "rgb(74,74,74)"
              }}
            >
              Transaction Number : {this.props.orders.id}
            </Text>
          </View>
        </View>
        {/* </KeyboardAvoidingView>*/}
      </View>
    );
  };

  _pickWithCamera = async () => {
    ImagePicker.openCamera({
      width: 500,
      height: 500,
      // cropperCircleOverlay: circular,
      // compressImageMaxWidth: 640,
      // compressImageMaxHeight: 480,
      compressImageQuality: Platform.OS === "ios" ? 0.8 : 1,
      // compressVideoPreset: "MediumQuality",
      includeExif: true
    })
      .then(image => {
        // console.log("received image", image);
        this.setState({
          image: {
            uri: image.path,
            mime: image.mime,
            width: image.width,
            height: image.height
          }
        });
      })
      .catch(e => {
        console.log(e);
        Alert.alert(e.message ? e.message : e);
      });
  };

  componentWillMount = () => {
    setTimeout(() => {
      if (Platform.OS === "ios") {
        this.keyboardWillShowSub = Keyboard.addListener(
          "keyboardWillShow",
          this._handleKeyboardShow
        );
        this.keyboardWillHideSub = Keyboard.addListener(
          "keyboardWillHide",
          this._handleKeyboardHide
        );
      } else {
        this.keyboardWillShowSub = Keyboard.addListener(
          "keyboardDidShow",
          this._handleKeyboardShow
        );
        this.keyboardWillHideSub = Keyboard.addListener(
          "keyboardDidHide",
          this._handleKeyboardHide
        );
      }
    }, 1000);
  };

  componentDidMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount = () => {
    setTimeout(() => {
      this.keyboardWillShowSub.remove();
      this.keyboardWillHideSub.remove();
    }, 1000);
  };

  _renderLayout = () => {
    // console.log("this.state.cameraStatus : ", this.state.cameraStatus);
    // console.log("this.state.image : ", this.state.image);
    if (this.state.cameraStatus) {
      return this._layoutSlideInput();
    }

    if (this.state.image) {
      return this._layoutInputPrice();
    } else {
      return this._layoutTakePicture();
    }
  };

  render() {
    // console.log("photo", this.state.image);
    // console.log("price", this.state.price);
    // console.log("ini state image", this.state.image);

    return (
      <View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          alignItems: "center"
        }}
      >
        <StatusBar hidden={true} />
        <MapView
          ref={map => (this.map = map)}
          region={{
            latitude: parseFloat(this.props.location.lat),
            longitude: parseFloat(this.props.location.lng),
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          }}
          // region={this.state.restoDestination}
          showsMyLocationButton={false}
          style={styles.maps}
          zoomEnabled={true}
          loadingEnabled={true}
          loadingIndicatorColor="#606060"
          loadingBackgroundColor="#FFFFFF"
          moveOnMarkerPress={false}
          showsUserLocation={false}
          followUserLocation={
            false
          } /* This only works if showsUserLocation is true */
          showsCompass={true}
          showsPointsOfInterest={false}
          showsScale={true}
          minZoomLevel={15}
          maxZoomLevel={20}
          provider="google"
        >
          <MapView.Marker
            image={Assets.pin}
            key="1"
            title=""
            description=""
            // comment for testing
            coordinate={{
              latitude: parseFloat(this.props.location.lat),
              longitude: parseFloat(this.props.location.lng),
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA
            }}
            // coordinate={this.state.restoDestination}
            // onPress={() => this._clickOrder()}
          />
        </MapView>
        <View>
          {this._renderLayout()}
          {/* {this.state.cameraStatus
            ? this._layoutSlideInput()
            : this.state.image
            ? this._layoutInputPrice()
            : this._layoutTakePicture()} */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  maps: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1
  }
});

// {this.state.image && this.state.price
//   ? this._layoutSlideInput()
//   : this.state.price
//   ? this._layoutTakePicture()
//   : this._layoutInputPrice()}
