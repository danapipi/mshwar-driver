/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-10-29 12:27:36
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-29 16:47:34
 */

export default {
  login: {
    userInfo: {},
    email: '',
    password: '',
    loading: false,
    error: ''
  },
  home: {
    data: {},
    successStatusDriver: false,
    loading: false,
    error: '',
    location: {},
    loadingLocation: false,
    activeOrder: null,
    errorLocation: '',
    errorFetchOrder: '',
    order: {},
  },
  orders: {
    valueSliderAccept: 3,
    success: false,
    error: '',
    data: {},
  },
  orderDetail: {
    dataCancel: {},
    loading: false,
    error: '',
    selectedRadio: {
      id: 1,
      name: 'Vendor is closed'
    },
    // success: false
  },
  detail: {

  },
  receipt: {
    // price: "",
    // image: "",
    data: {},
    error: '',
    loading: false
  },
  finishOrder: {
   
  },
  keyFinish: {
    loading: false,
    error: '',
    data: {},
  }
};
