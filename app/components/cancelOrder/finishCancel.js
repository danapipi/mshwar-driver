/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-11-10 22:16:51
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-14 20:39:50
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  Modal,
  Dimensions,
  StyleSheet,
  Platform
} from "react-native";
import Assets from "../../assets";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h
} from "../../config/layout";
import { themes } from "../../config/themes";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import IonIcon from "react-native-vector-icons/Ionicons";
import Slider from "react-native-slider";
import Communications from "react-native-communications";
import HeaderCustomer from "../headerCustomer";


export default class ModalCancelOrder extends Component {
  constructor(props) {
    super(props);
  }

  _clickOk = () => {
    this.props.handleCloseCancelFinish();
  };

  render() {
    return (
      <Modal
        onRequestClose={() => {
          // this.props.handleClose()
        }}
        animationType="slide"
        animationOut="slide"
        transparent={true}
        visible={this.props.modalVisibleCancelFinish}
      >
        <View
          style={{
            backgroundColor: themes.backdrop,
            flex: 1,
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            alignItems: "center"
          }}
        >
          <HeaderCustomer data={this.props.data} />

          <View
            style={{
              position: "absolute",
              bottom: 10
            }}
          >
            <View
              style={{
                width: wp("90%"),
                borderRadius: 15,
                backgroundColor: themes.white
              }}
            >
              <View
                style={{
                  backgroundColor: themes.white_10,
                  paddingHorizontal: wp(w(20)),
                  paddingVertical: hp(h(15)),
                  borderTopLeftRadius: 15,
                  borderTopRightRadius: 15,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: hp(h(18)),
                    fontFamily: "Roboto-Medium",
                    color: themes.gray,
                    fontWeight: "bold"
                  }}
                >
                  Order Has Been Canceled
                </Text>
              </View>

              <View
                style={{
                  flex: 1
                }}
              >
                <View
                  style={{
                    flexDirection: "row"
                  }}
                >
                  <View
                    style={{
                      flex: 2,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <MaterialCommunityIcons
                      name="block-helper"
                      color={themes.red}
                      size={21}
                      style={{
                        position: "absolute",
                        right: 0,
                        transform: [{ rotate: "90deg" }]
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flex: 8,
                      marginLeft: wp(w(30))
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Roboto-Light",
                        fontSize: hp(h(18)),
                        color: themes.red
                      }}
                    >
                      {this.props.selectedRadio.name}
                    </Text>
                  </View>
                </View>
                <TouchableOpacity
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    paddingVertical: hp(h(15))
                  }}
                  onPress={() => {
                    this._clickOk();
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Roboto-Medium",
                      fontSize: hp(h(18)),
                      color: themes.blueSea
                    }}
                  >
                    OK
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  borderTopWidth: 1,
                  borderColor: themes.border,
                  justifyContent: "center",
                  alignItems: "center",
                  paddingVertical: hp(h(5)),
                  paddingHorizontal: wp(w(20))
                }}
              >
                <Text
                  style={{
                    fontSize: hp(h(12)),
                    fontFamily: "Roboto-Light",
                    color: themes.gray
                  }}
                >
                  Transaction Number : {this.props.data.id}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
