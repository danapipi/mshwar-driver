/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-11-09 15:50:34
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-29 12:51:49
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  Modal,
  Dimensions,
  StyleSheet,
  Platform
} from "react-native";
import Assets from "../../assets";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h
} from "../../config/layout";
import { themes } from "../../config/themes";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import IonIcon from "react-native-vector-icons/Ionicons";
import Slider from "react-native-slider";
import RadioButtonGroupCustom from "../../components/radioButonGroup";
import HeaderCustomer from "../headerCustomer";

const { width, height } = Dimensions.get("window");
let listData = {
  id: 1,
  options: [
    {
      id: 1,
      name: "Vendor is closed"
    },
    {
      id: 2,
      name: "Item is unavailable"
    }
  ]
};

export default class ModalCancelOrder extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sliderMinValue: 3,
      sliderMaxValue: 20,
      labelSlide: "SWIPE TO CANCEL ORDER"
      // selectedRadio: 1
    };
  }

  _valueChange = value => {

    if (value === this.state.sliderMaxValue) {
      this.props.handleAcceptCancel(value);
      this.setState({
        labelSlide: "SWIPE TO CANCEL ORDER"
      });
    } else {
      this.setState({
        labelSlide: ""
      });
    }
  };

  _slideComplete = value => {
    this.props.handleChangeCancelValue(value);
  };

  _selectRadioButtonGroup = params => {
    this.props.selectedRadioCancel(params);
  };

  render() {
      console.log('AAAAA', this.props.data)
    return (
      <Modal
        onRequestClose={() => {
          // this.props.handleClose()
        }}
        animationType="slide"
        animationOut="slide"
        transparent={true}
        visible={this.props.modalVisibleCancel}
      >
        <View
          style={{
            backgroundColor: themes.backdrop,
            flex: 1,
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            alignItems: "center"
          }}
        >
          <View style={{
              paddingTop: Platform.OS === 'ios' ? 24 : 0,
            }}>
            <HeaderCustomer data={this.props.data} />
          </View>

          <View
            style={{
              position: "absolute",
              bottom: hp(h(10))
            }}
          >
            <View
              style={{
                width: wp("90%"),
                borderRadius: 15,
                backgroundColor: themes.white
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  backgroundColor: themes.white_10,
                  paddingLeft: wp(w(20)),
                  paddingVertical: hp(h(15)),
                  borderTopLeftRadius: 15,
                  borderTopRightRadius: 15
                }}
              >
                <View
                  style={{
                    flex: 8,
                    marginLeft: wp(w(40)),
                    paddingLeft: wp(w(10))
                  }}
                >
                  <Text
                    style={{
                      fontSize: hp(h(18)),
                      color: themes.gray,
                      fontWeight: "bold"
                    }}
                  >
                    Do you want to cancel this order?
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    this.props.handleCloseCancel();
                  }}
                  style={{
                    flex: 2,
                    alignItems: "center"
                  }}
                >
                  <IonIcon
                    name="md-close-circle"
                    color={themes.gray}
                    borderRadius={30}
                    size={29}
                    style={{}}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  marginRight: hp(h(20)),
                  marginVertical: hp(h(15))
                }}
              >
                <View
                  style={{
                    flex: 4,
                    paddingTop: hp(h(8)),
                    alignItems: "center"
                  }}
                >
                  <MaterialCommunityIcons
                    name="block-helper"
                    color={themes.gray}
                    size={21}
                    style={{
                      transform: [{ rotate: "90deg" }]
                    }}
                  />
                </View>
                <View
                  style={{
                    flex: 16,
                    paddingLeft: wp(w(10))
                  }}
                >
                  <Text
                    style={{
                      fontSize: hp(h(18)),
                      fontFamily: "Roboto-Medium",
                      color: themes.gray_50
                    }}
                  >
                    {this.props.data.merchant_data.name}
                  </Text>
                  <Text
                    style={{
                      fontSize: hp(h(14)),
                      fontFamily: "Helvetica-Light",
                      color: themes.gray_50
                    }}
                  >
                    {this.props.data.merchant_data.address}
                  </Text>
                  <View
                    style={{
                      flex: 1,
                      marginTop: hp(h(20))
                    }}
                  >
                    <RadioButtonGroupCustom
                      data={listData.options}
                      selected={this.props.selectedRadio}
                      handleSelectedRadio={this._selectRadioButtonGroup}
                    />
                  </View>
                </View>
              </View>
              <View>
                <View
                  style={{
                    marginVertical: hp(h(15)),
                    marginHorizontal: wp(w(20)),
                    backgroundColor: themes.gray_30,
                    borderRadius: 15
                    // height: hp(h(50))
                  }}
                >
                  <Slider
                    value={this.props.value}
                    thumbImage={Assets.swipeCancel}
                    thumbStyle={{
                      width: 58,
                      height: 39,
                      marginVertical: hp(h(10))
                    }}
                    thumbTintColor="transparent"
                    minimumTrackTintColor="transparent"
                    maximumTrackTintColor="transparent"
                    minimumValue={this.props.sliderMinValue}
                    maximumValue={this.props.sliderMaxValue}
                    onValueChange={value => {
                      this._valueChange(value);
                    }}
                    onSlidingComplete={value => {
                      this._slideComplete(value);
                    }}
                    style={{
                      marginVertical: hp(h(5)),
                      marginHorizontal: wp(w(5)),
                      zIndex: 10
                    }}
                  />
                  <View
                    style={{
                      justifyContent: "center",
                      alignItems: "center",
                      height: "100%",
                      position: "absolute",
                      width: "100%",
                      right: 0
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Helvetica-Light",
                        fontSize: hp(h(12)),
                        paddingLeft: wp(w(58)),
                        color: themes.red
                      }}
                    >
                      {this.state.labelSlide}
                    </Text>
                  </View>
                </View>
              </View>
              <View
                style={{
                  borderTopWidth: 1,
                  borderColor: themes.border,
                  justifyContent: "center",
                  alignItems: "center",
                  paddingVertical: hp(h(5)),
                  paddingHorizontal: wp(w(20))
                }}
              >
                <Text
                  style={{
                    fontSize: hp(h(12)),
                    fontFamily: "Roboto-Light",
                    color: themes.gray
                  }}
                >
                  Transaction Number : {this.props.data.id}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}