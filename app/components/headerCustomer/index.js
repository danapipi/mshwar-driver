import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import Assets from "../../assets";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h,
  isIphoneX
} from "../../config/layout";
import { themes } from "../../config/themes";
import Communications from "react-native-communications";

export default class HeaderCustomer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
      // console.log('COBA HEADER', this.props)
    return (
      <View
        style={{
          flexDirection: "row",
          backgroundColor: themes.white,
          borderTopRightRadius: 15,
          borderBottomRightRadius: 15,
          borderBottomLeftRadius: 30,
          borderTopLeftRadius: 30,
          marginTop: hp(h(35)),
          // position: "absolute",
          // flex: 1,
          width: wp("90%"),
          height: hp(h(47))
        }}
      >
        {/* <ImageBackground
        source={Assets.customerBackground}
        style={{ width: 325, height: 49, zIndex: -1 }}
      > */}
        <Image
          source={Assets.photoDriver}
          style={
            isIphoneX
              ? { height: hp(h(43)), width: wp(w(52)), alignSelf: 'center', marginHorizontal: wp(w(3)) }
              : { height: hp(h(43)), width: wp(w(52)), alignSelf: 'center', marginHorizontal: wp(w(3))}
          }
        />
        <View style={{ flexDirection: "column", flex: 2 }}>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                fontFamily: "Roboto-Regular",
                fontSize: hp(h(11)),
                color: themes.gray,
                textAlign: "center",
                zIndex: 10,
                marginTop: hp(h(5)),
                paddingRight: wp(w(60))
              }}
            >
              CUSTOMER
            </Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text
              style={{
                fontFamily: "Helvetica-Bold",
                fontSize: hp(h(13)),
                color: themes.gray,
                marginLeft: wp(w(10)),
                textAlign: "left",
                flex: 2
              }}
            >
              {this.props.data.name}
            </Text>
            <TouchableOpacity
              onPress={() =>
                Communications.phonecall(
                    this.props.data.phone
                    , true)
              }
            >
              <Image
                source={Assets.phone}
                style={{ marginHorizontal: wp(w(15)) }}
              />
            </TouchableOpacity>
            <View
              style={{
                borderRightColor: themes.purple_20,
                borderWidth: 0.5
              }}
            />
            <TouchableOpacity
              onPress={() => Communications.text(
                //   this.props.data.phone
                )}
            >
              <Image
                source={Assets.message}
                style={{ marginHorizontal: wp(w(15)) }}
              />
            </TouchableOpacity>
          </View>
        </View>
        {/* </ImageBackground> */}
      </View>
    );
  }
}
