import {Platform} from 'react-native';

export default {
  wrapperContainer: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 24 : 0, 
  },
  container: {
    flex: 1,
    padding: 0,
  },
  contentContainerStyle: {
    flex: 1
  },
  innerContainer: {
    padding: 0,
  },
  modalStyle: {
    padding: 0,
    margin: 0
  },
};
