import { getViewWidth } from "../../utils/device";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h,
  isIphoneX
} from "../../config/layout";
import { themes } from "../../config/themes";

export default {
  wrapper: {
    // justifyContent: "center",
    // alignItems: 'center',
    flexDirection: "row",
    backgroundColor: themes.white,
    width: wp("90%"),
    height: hp(h(30)),
    borderRadius: 15
  },
  barLeft: {
    width: wp(w(41)),
    height: hp(h(41))
  },
  barRight: {
    // backgroundColor: "rgb(255,255,255)",
    width: wp(w(285)),
    paddingLeft: wp(w(20)),
    borderRadius: 15,
    marginRight: wp(w(15)),
    // justifyContent: 'center',
    alignItems: "center",
    flexDirection: "row"
  },
  profile: {
    flexDirection: "row"
  },
  circle: [
    {
      flexDirection: "column",
      overflow: "hidden",
      borderColor: themes.mainColor,
      backgroundColor: "#000",
      borderWidth: 2,
      borderRadius: 50,
      width: 50,
      height: 50,
      marginTop: -hp(h(10))
    },
    isIphoneX ? { marginTop: hp(h(-5)) } : { marginTop: hp(h(-5)) }
  ],
  name: {
    color: themes.gray_50,
    fontFamily: "Helvetica-Bold",
    fontSize: hp(h(18))
  },
  img: {
    marginTop: -3,
    marginLeft: -3,
    width: 51,
    height: 51
  },
  barSwitch: {
    backgroundColor: themes.white_50,
    borderRadius: 15,
    marginRight: wp(w(8)),
    width: wp(w(32)),
    height: hp(h(16))
  },
  status: {
    paddingRight: wp(w(6)),
    fontFamily: "Helvetica-Bold"
  },
  wrapperStatus: {
    flexDirection: "row",
    right: -45,
    position: "absolute",
    justifyContent: "center",
    alignItems: "center"
  },
  barBullet: {
    height: hp(h(16))
  },
  thumbSlider: {
    width: wp(w(16)),
    height: hp(h(16))
  }
};
