/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-11 05:01:06 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-11 20:50:12
 */

import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  Platform
} from "react-native";
import Assets from "../../assets";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { percentageWidth as w, percentageHeight as h } from "../../config/layout";
import { themes } from '../../config/themes';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import IonIcon from "react-native-vector-icons/Ionicons";

export default class RadioButtonGroupCustom extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        if(this.props.data && this.props.data.length) {
            let radio = this.props.data.map((data, index) => {
                return (
                    <TouchableOpacity
                        key={data.id}
                        style={{
                                marginVertical: hp(h(9)),
                        }}
                        onPress={() => {
                            const params = {
                                id      : data.id,
                                name    : data.name
                            }
                            this.props.handleSelectedRadio(params)
                        }}>
                        <View style={{
                                flexDirection: 'row'
                        }}>
                            <View style={[{
                                height: 24,
                                width: 24,
                                borderRadius: 12,
                                borderWidth: 2,
                                borderColor: '#000',
                                alignItems: 'center',
                                justifyContent: 'center',
                                marginRight: wp(w(10))
                            }]}>
                                {this.props.selected.id == data.id ?
                                    <View style={{
                                        height: 12,
                                        width: 12,
                                        borderRadius: 6,
                                        backgroundColor: '#000',
                                        }}/>
                                    : null    
                                }
                                
                                    
                            </View>
                            <Text style={{
                                    fontSize: hp(h(14))
                            }}>{data.name}</Text>
                        </View>
                    </TouchableOpacity>
                )
            })
            return (
                <View>
                    {radio}
                </View>
            );
        } else {
            return (
                <TouchableOpacity
                    onPress={() => {
                        alert('Need custom for single radio')
                    }}>
                    <View style={{
                            flexDirection: 'row'
                    }}>
                        <View style={[{
                            height: 24,
                            width: 24,
                            borderRadius: 12,
                            borderWidth: 2,
                            borderColor: '#000',
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginRight: wp(w(10))
                        }]}>
                            
                            <View style={{
                                height: 12,
                                width: 12,
                                borderRadius: 6,
                                backgroundColor: '#000',
                                }}/>
                                
                        </View>
                        <Text style={{
                                fontSize: hp(h(14))
                        }}>{this.props.data.name}</Text>
                    </View>
                </TouchableOpacity>
            )
        }
    }
}