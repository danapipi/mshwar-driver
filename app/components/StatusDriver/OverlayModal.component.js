import React, {Component} from 'react';
import {Modal, Image, TouchableOpacity, View, ImageBackground, Text} from 'react-native';
import styles from './OverlayModal.styles';
import Assets from "../../assets";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { percentageWidth as w, percentageHeight as h } from "../../config/layout";
import IonIcon from "react-native-vector-icons/Ionicons";
import { themes } from '../../config/themes';
import Slider from "react-native-slider";

export default class OverlayModalComponent extends Component {

  constructor(props) {
    super(props);

    this.state = {
      sliderMinValue: 0,
      sliderMaxValue: 1,
      labelSlide  : 'SWIPE TO TURN ON DRIVER APP'
    }
  }

  // _slideComplete = (value) => {
  //   // console.log('value : ',value)
  //   // if(value > 2) {
  //   //   this.setState({labelSlide: ''})
  //   // }
  //   if(value === this.state.sliderMaxValue) {
  //     this.props.handleAccept();
  //   }
  // };

  _valueChange = (value) => {
    this.props.handleChangeValue(value);
  };

  _slideComplete = (value) => {
    this.props.handleAccept(value);
  }  

  render () {
    
    return (
      <Modal
        onRequestClose={() => {
          this.props.handleClose()
        }}
        animationType="slide"
        animationOut="slide"
        transparent={true}
        visible={this.props.modalVisible}>
        <ImageBackground
          source={Assets.background}
          style={{
            width: wp("100%"),
            height: hp("100%")
          }}
          resizeMode="cover">
          <View style={styles.wrapperContainer}>
            <TouchableOpacity 
              onPress={() => {
                this.props.handleClose()
              }}
              style={{
                    marginTop: hp(h(17)),
                    marginRight: wp(w(13)),
                    flexDirection: 'row',
                    right: 0,
                    position: 'absolute',
                    alignItems: 'center'
            }}>
              <Text style={{
                      fontFamily: 'Helvetica-Light',
                      fontSize: hp(h(12)),
                      color: themes.white,
                      marginRight: wp(w(8))
              }}>SKIP</Text>
              <IonIcon
                    name="md-close-circle"
                    color={themes.white}
                    borderRadius={30}
                    size={29}
                    style={{}}
                  />  
            </TouchableOpacity>
            <View style={{
                    width: '100%',
                    height: hp(h(166)),
                    marginTop: hp(h(55)),
                    justifyContent: 'center',
                    alignItems: 'center'
            }}>
              <Image source={Assets.logo} style={{ height: 22, width: 141 }} />
            </View>
            <View style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
            }}>
              <Image
                source={Assets.photoDriver}
                style={{ width: 39, height: 39 }}
              />
              <Text style={{
                      fontFamily: 'Helvetica-Bold',
                      fontSize: hp(h(24)),
                      color: themes.white,
                      marginLeft: wp(w(15))
              }}>Hey {this.props.userInfo.name} !</Text>
            </View>
            <View style={{
                    justifyContent: 'center',
                    marginTop: hp(h(12)),
            }}>
              <Text style={{
                      fontFamily: 'Helvetica',
                      fontSize: hp(h(20)),
                      color: themes.white,
                      textAlign: 'center'
              }}>Ready to work today ?</Text>
            </View>
            <View
              style={{
                marginTop: hp(h(219)),
                marginHorizontal: wp(w(35)),
                backgroundColor: themes.white,
                borderRadius: 15,
                // height: hp(h(50))
              }}
            >
              <Slider
                value={this.props.value}
                thumbImage={Assets.swipeArrow}
                thumbStyle={{ width: 58, height: 39, marginVertical: hp(h(10)) }}
                thumbTintColor="transparent"
                minimumTrackTintColor="transparent"
                maximumTrackTintColor="transparent"
                minimumValue={this.props.sliderMinValue}
                maximumValue={this.props.sliderMaxValue}
                onValueChange={(value) => {
                  this._valueChange(value)
                }}
                onSlidingComplete={(value) => {
                  this._slideComplete(value)
                }}
                // onValueChange={(value) => {
                //   this._slideComplete(value)
                // }}
                // onSlidingComplete={() => this._slideComplete()}
                style={{
                  marginVertical: hp(h(5)),
                  marginLeft: wp(w(5)),
                  zIndex: 10
                }}
              />
              <View style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: '100%',
                position: 'absolute',
                width: wp(w(280)),
                right: 0,
              }}>
                <Text style={{
                  fontFamily: 'Helvetica-Light',
                  fontSize: hp(h(12)),
                  color: themes.gray_70,
                }}>
                  {this.state.labelSlide}
                </Text>
              </View>
            </View>
          </View>
        </ImageBackground>
      </Modal>
    );
  }
}
