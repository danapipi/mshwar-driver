import React, { Component } from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import IonIcon from "react-native-vector-icons/Ionicons";
import styles from './DriverStatus.styles';
import OverlayModal from './OverlayModal.component';
import Assets from "../../assets";
import Slider from "react-native-slider";
import { themes } from '../../config/themes'

export default class DriverStatus extends Component {

    constructor(props){
        super(props)

        this.state = {
            modalVisible: false,
            sliderMinValue: 3,
            sliderMaxValue: 20,
            sliderStatusMinValue: 0,
            sliderStatusMaxValue: 1,
            value: 0, 
        }
    }

    _closeModal = () => {
        this.setState({
            modalVisible: false,
            value: this.state.sliderMinValue});
    };

    _acceptActive = (value) => {
        if(Math.floor(value) < this.state.sliderMaxValue) {
            this.props.swipeStatusOrderUpdate(this.state.sliderMinValue);
          }
    };
    
    _changeModalValue = (value) => {
        if(value === this.state.sliderMaxValue) {
            const params = {
                status: this.state.sliderStatusMaxValue // set to active
            }
            this.props.handleSetActive(params)
            this.setState({
                modalVisible: false});    
        } else {
            this.props.swipeStatusOrderUpdate(Math.floor(value));
        }
    }

    _changeStatusDriver = () => {
        if(this.props.userInfo.status === this.state.sliderStatusMinValue) { // check if inactive
            this.props.swipeStatusOrderUpdate(this.state.sliderMinValue);
            this.setState({
                modalVisible: true,
            })
        } else {
            const params = {
                status: this.state.sliderStatusMinValue // set to inactive
            }
            this.props.handleSetActive(params)
        }
    }  

    componentDidMount = () => {
        
        if(this.props.data && this.props.data.length) {
            if(this.props.userInfo.status == '1') {
                this.setState({
                    value: this.state.sliderMaxValue});
            }
        } else {
            if(this.props.userInfo.status == '1') {
                this.setState({
                    value: this.state.sliderMaxValue});
            }
        }
        
    }  

    render () {
        
        return (
            <View style={{
                    justifyContent: 'center',
                    alignItems: 'center'
            }}>
                <View style={styles.wrapperStatus}>
                    <Text style={[styles.status, (this.props.userInfo.status == '0') ? { color: themes.gray_50 } : { color: themes.green }]}>{this.props.userInfo.status == '0' ? 'INACTIVE' : 'ACTIVE'}</Text>
                    <View style={styles.barSwitch}>
                        <Slider
                            ref={slider => this.slider = slider}
                            value={this.props.userInfo.status}
                            thumbStyle={[styles.thumbSlider, (this.props.userInfo.status == '0') ? { backgroundColor: themes.gray } : { backgroundColor: themes.green }]}
                            thumbTintColor="transparent"
                            minimumTrackTintColor="transparent"
                            maximumTrackTintColor="transparent"
                            minimumValue={this.state.sliderStatusMinValue}
                            maximumValue={this.state.sliderStatusMaxValue}
                            onSlidingComplete={(value) => {
                                this._changeStatusDriver();
                            }}
                            onSlidingStart={(value) => {

                            }}
                            style={styles.barBullet}
                        />
                    </View>
                </View>
                <OverlayModal 
                    userInfo={this.props.userInfo}
                    modalVisible={this.state.modalVisible} 
                    handleClose={this._closeModal}
                    handleAccept={this._acceptActive}
                    handleChangeValue={this._changeModalValue}
                    value={this.props.valueSliderAccept}
                    sliderMaxValue={this.state.sliderMaxValue}
                    sliderMinValue={this.state.sliderMinValue}
                    />
            </View>
        );
    }
}