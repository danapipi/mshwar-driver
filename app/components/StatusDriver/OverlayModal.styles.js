export default {
  wrapperContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    padding: 0,
  },
  contentContainerStyle: {
    flex: 1
  },
  innerContainer: {
    padding: 0,
  },
  modalStyle: {
    padding: 0,
    margin: 0
  },
};
