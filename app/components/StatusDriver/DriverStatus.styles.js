import {getViewWidth} from '../../utils/device';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { percentageWidth as w, percentageHeight as h } from "../../config/layout";
import { themes } from '../../config/themes';

export default {
  wrapper: {
    // justifyContent: "center",
    // alignItems: 'center',
    flexDirection: "row",
    backgroundColor: themes.white,
    width: wp('50%'),
    height: hp(h(30)),
    borderRadius: 15,
  },
  barRight: {
    // backgroundColor: "rgb(255,255,255)",
    width: wp(w(285)),
    paddingLeft: wp(w(20)),
    borderRadius: 15,
    marginRight: wp(w(15)),
    // justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  profile: {
    flexDirection: "row", 
  },
  name: {
    color: themes.gray_50,
    fontFamily: "Helvetica-Bold",
    fontSize: hp(h(18)),
  },
  barSwitch: {
    backgroundColor: themes.white_50,
    borderRadius: 15,
    marginRight: wp(w(8)),
    width: wp(w(32)),
    height: hp(h(16)),
  },
  status: {
    paddingRight: wp(w(6)),
    fontFamily: 'Helvetica-Bold',
  },
  wrapperStatus: {
    flexDirection: 'row',
    right: 0,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
  barBullet: {
    height: hp(h(16)),
  },
  thumbSlider: {
    width: wp(w(16)), 
    height: hp(h(16)),
  }
};
