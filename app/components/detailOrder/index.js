/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-11-09 13:23:30
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-17 17:41:22
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Modal,
  Dimensions,
  StyleSheet,
  Platform
} from "react-native";
import Assets from "../../assets";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h
} from "../../config/layout";
import { themes } from "../../config/themes";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import FlatListItem from "./FlatListItem";
import IonIcon from "react-native-vector-icons/Ionicons";
import Communications from "react-native-communications";

const { width, height } = Dimensions.get("window");
let listData = [
  {
    id: 1,
    quantity: 1,
    name: "Big Mac",
    product_id: 12,
    price: "QR. 14.00",
    notes:
      "medium rare fried egg, extra spicy, no veggies, no fried onions. add spoon and fork, no chopstick",
    detail: [
      {
        id: 1,
        name: "Iced Tea"
      },
      {
        id: 2,
        name: "Chips"
      }
    ]
  },
  {
    id: 2,
    quantity: 2,
    name: "Fish & Fries",
    product_id: 15,
    price: "QR. 14.00",
    notes: "",
    detail: [
      {
        id: 1,
        name: "Iced Tea"
      }
    ]
  }
];
export default class ModalDetail extends Component {
  constructor(props) {
    super(props);
  }

  _renderFlatList = datas => {
    // console.log('datas : ',datas)
    return (
      <FlatList
        //   data={listData}
        data={datas.product_data}
        numColumns={1}
        extraData={this.state}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => {
          return <FlatListItem items={item} indexs={index} />;
        }}
      />
    );
  };

  render() {
      console.log("AAAAAA", this.props.data)
    return (
      <Modal
        onRequestClose={() => {
          // this.props.handleClose()
        }}
        animationType="slide"
        animationOut="slide"
        transparent={false}
        visible={this.props.modalVisible}
        // visible={true}
      >
        <View
          style={{
            backgroundColor: themes.gray,
            paddingBottom: hp(h(25)),
            paddingTop: hp(h(55)),
            paddingHorizontal: wp(w(18)),
            flex: 1,
            height
            // paddingVertical: hp(h(20)),
          }}
        >
          <View
            style={{
              backgroundColor: themes.blueSea,
              paddingVertical: hp(h(12)),
              paddingHorizontal: wp(w(14)),
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15
            }}
          >
            <View
              style={{
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  flex: 9
                }}
              >
                <Text
                  style={{
                    fontSize: hp(h(14)),
                    color: themes.white
                  }}
                >
                  DETAIL ORDER
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.props.handleClose();
                }}
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <IonIcon
                  name="md-close-circle"
                  color={themes.white}
                  size={16}
                  style={{}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                borderRadius: 12,
                borderColor: themes.white,
                borderWidth: 1,
                marginTop: hp(h(20)),
                paddingVertical: hp(h(5))
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontSize: hp(h(13)),
                  color: themes.white
                }}
              >
                STATUS : {this.props.data.status_text.toUpperCase()}
              </Text>
            </View>
          </View>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View
              style={{
                backgroundColor: themes.white,
                paddingVertical: hp(h(20))
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginHorizontal: wp(w(20))
                }}
              >
                <Image
                  source={Assets.photoDriver}
                  style={{ width: 39, height: 39 }}
                />
                <View>
                  <Text
                    style={{
                      fontSize: hp(h(13)),
                      color: themes.gray,
                      fontFamily: "Helvetica-Bold",
                      paddingLeft: wp(w(10)),
                      fontWeight: "bold"
                    }}
                  >
                    {this.props.data.name} 
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    right: 0,
                    position: "absolute"
                  }}
                >
                  <TouchableOpacity
                    onPress={() =>
                      Communications.phonecall(this.props.data.phone, true)
                    }
                    style={{
                      paddingHorizontal: wp(w(16)),
                      borderRightWidth: 1,
                      borderColor: themes.border
                    }}
                  >
                    <MaterialCommunityIcons
                      name="phone"
                      color={themes.mainColor}
                      size={20}
                      style={{}}
                    />
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => Communications.text(this.props.data.phone)}
                    style={{
                      paddingHorizontal: wp(w(16))
                    }}
                  >
                    <MaterialCommunityIcons
                      name="message-text"
                      color={themes.mainColor}
                      size={20}
                      style={{}}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View
              style={{
                paddingVertical: hp(h(20)),
                backgroundColor: themes.white_10
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  marginHorizontal: wp(w(20)),
                  alignItems: "center"
                }}
              >
                <Image
                  source={Assets.restoPurple}
                  style={{ width: 21, height: 21 }}
                />
                <View
                  style={{
                    marginLeft: wp(w(24))
                  }}
                >
                  <Text
                    style={{
                      fontSize: hp(h(18)),
                      color: themes.gray,
                      fontWeight: "bold"
                    }}
                  >
                    {this.props.data.merchant_data.name}
                  </Text>
                  <Text
                    style={{
                      fontSize: hp(h(14)),
                      color: themes.gray_50
                    }}
                  >
                    {this.props.data.merchant_data.address}
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      Communications.phonecall(
                        this.props.orders.merchant_data.phone,
                        true
                      )
                    }
                  >
                    <Text
                      style={{
                        fontSize: hp(h(10)),
                        color: themes.blueSea
                      }}
                    >
                      CALL VENDOR
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View
              style={{
                paddingVertical: hp(h(20)),
                backgroundColor: themes.white
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  marginHorizontal: wp(w(20)),
                  alignItems: "center"
                }}
              >
                {this._renderFlatList(this.props.data)}
              </View>
            </View>
            <View
              style={{
                paddingVertical: hp(h(20)),
                backgroundColor: themes.white_10
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  marginHorizontal: wp(w(20)),
                  alignItems: "center"
                }}
              >
                <Image
                  source={Assets.location}
                  style={{ width: 21, height: 21 }}
                />
                <View
                  style={{
                    marginLeft: wp(w(24))
                  }}
                >
                  <Text
                    style={{
                      fontSize: hp(h(18)),
                      color: themes.gray,
                      fontWeight: "bold"
                    }}
                  >
                    {this.props.data.address}
                  </Text>
                  {/* <Text style={{
                                        fontSize: hp(h(14)),
                                        color: themes.gray_50
                                    }}>Doha, Qatar</Text> */}
                </View>
              </View>
            </View>
            <View
              style={{
                paddingVertical: hp(h(20)),
                backgroundColor: themes.white
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  marginHorizontal: wp(w(20)),
                  alignItems: "center",
                  marginBottom: hp(h(20))
                }}
              >
                <View>
                  <Text
                    style={{
                      fontSize: hp(h(12)),
                      color: themes.gray
                    }}
                  >
                    PAYMENT
                  </Text>
                </View>
                <View
                  style={{
                    position: "absolute",
                    right: 0
                  }}
                >
                  <Text
                    style={{
                      fontSize: hp(h(12)),
                      color: themes.blueSea
                    }}
                  >
                    {this.props.data.payment_method.toUpperCase()}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  marginHorizontal: wp(w(20)),
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    flex: 1,
                    borderBottomWidth: 1,
                    borderColor: themes.border,
                    paddingVertical: hp(h(3))
                  }}
                >
                  <View>
                    <Text
                      style={{
                        fontSize: hp(h(13)),
                        color: themes.gray_50
                      }}
                    >
                      Estimate Price
                    </Text>
                  </View>
                  <View
                    style={{
                      position: "absolute",
                      right: 0
                    }}
                  >
                    <Text
                      style={{
                        fontSize: hp(h(12)),
                        color: themes.gray_50
                      }}
                    >
                      {this.props.data.display_price}
                    </Text>
                  </View>
                </View>
              </View>
              {/* <View style={{
                                flexDirection: 'row',
                                marginHorizontal: wp(w(20)),
                                alignItems: 'center'
                            }}>
                                <View style={{
                                    flexDirection: 'row',
                                    flex: 1,
                                    borderBottomWidth: 1,
                                    borderColor: themes.border,
                                    paddingVertical: hp(h(3))
                                }}>
                                    <View>
                                        <Text style={{
                                            fontSize: hp(h(13)),
                                            color: themes.gray_50
                                        }}>Net Price</Text>
                                    </View>
                                    <View style={{
                                        position: 'absolute',
                                        right: 0
                                    }}>
                                        <Text style={{
                                            fontSize: hp(h(12)),
                                            color: themes.gray_50
                                        }}>QR. 20.000.00</Text>
                                    </View>
                                </View>
                            </View> */}
              <View
                style={{
                  flexDirection: "row",
                  marginHorizontal: wp(w(20)),
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    flex: 1,
                    paddingVertical: hp(h(3))
                  }}
                >
                  <View>
                    <Text
                      style={{
                        fontSize: hp(h(13)),
                        color: themes.gray_50
                      }}
                    >
                      Delivery
                    </Text>
                  </View>
                  <View
                    style={{
                      position: "absolute",
                      right: 0
                    }}
                  >
                    <Text
                      style={{
                        fontSize: hp(h(12)),
                        color: themes.gray_50
                      }}
                    >
                      QR. {this.props.data.delivery_price}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <View
              style={{
                paddingVertical: hp(h(20)),
                backgroundColor: themes.white_10
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  marginHorizontal: wp(w(20)),
                  alignItems: "center"
                }}
              >
                <View>
                  <Text
                    style={{
                      fontSize: hp(h(14)),
                      color: themes.gray,
                      fontWeight: "bold"
                    }}
                  >
                    Total
                  </Text>
                </View>
                <View
                  style={{
                    position: "absolute",
                    right: 0,
                    alignItems: "center",
                    flexDirection: "row"
                  }}
                >
                  <Text
                    style={{
                      fontSize: hp(h(12)),
                      color: themes.gray,
                      paddingRight: wp(w(5)),
                      fontWeight: "bold"
                    }}
                  >
                    QR.
                  </Text>
                  <Text
                    style={{
                      fontSize: hp(h(18)),
                      color: themes.gray,
                      fontWeight: "bold"
                    }}
                  >
                    {this.props.data.total}
                  </Text>
                </View>
              </View>
            </View>
          </ScrollView>
          <View
            style={{
              backgroundColor: themes.white,
              paddingVertical: hp(h(12)),
              paddingHorizontal: wp(w(14)),
              borderBottomLeftRadius: 15,
              borderBottomRightRadius: 15,
              borderTopWidth: 1,
              borderColor: themes.border,
              justifyContent: "center",
              alignItems: "center",
              width: "100%"
            }}
          >
            <Text
              style={{
                fontSize: hp(h(12)),
                color: themes.gray
              }}
            >
              Transaction Number : {this.props.data.id}
            </Text>
          </View>
        </View>
      </Modal>
    );
  }
}
