/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-11-09 11:01:37
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-17 18:53:24
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity
} from "react-native";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  percentageWidth as w,
  percentageHeight as h,
  isIphoneX
} from "../../config/layout";
import { themes } from "../../config/themes";

export default class FlatListItem extends Component {
  constructor(props) {
    super(props);
  }

  _renderAdditionalItem = items => {
    if (items.product_data === "") {
      console.log("additional item none");
    } else {
      const dataParse = JSON.parse(items.product_data.replace("/", "")).data;
      const addItem = dataParse.split(",").map((val, i) => {
        if (val === "" || val === null) {
          return <View key={i} />;
        } else {
          return (
            <View key={i}>
              <Text>+ {val}</Text>
            </View>
          );
        }
      });
      return addItem;
    }

    // if (productData && productData.length) {
    //   const detailProduct = productData.data.split(",");
    //   // console.log('productData : detail');
    //   const additionalItem = detailProduct.map((val, i) => {
    //     return (
    //       <View key={i}>
    //         <Text>+ {val}</Text>
    //       </View>
    //     );
    //   });
    //   return additionalItem;
    // } else {
    //   return (
    //     <View key={items.id}>
    //       <Text>+ {productData.data}</Text>
    //     </View>
    //   );
    // }
    // console.log('productData : ',items)
    // return null
  };

  _renderNotes = items => {
    if (items.product_data === "") {
      console.log("there is not notes");
    } else {
      const dataParse = JSON.parse(items.product_data.replace("/", "")).notes;
      if (dataParse !== "" && dataParse !== null ) {
        return (
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: hp(h(15))
            }}
          >
            <Text
              style={{
                color: themes.red,
                fontSize: hp(h(11)),
                marginRight: wp(w(12))
              }}
            >
              NOTE
            </Text>
            <Text
              style={{
                color: themes.gray,
                fontSize: hp(h(13))
              }}
            >
              {dataParse}
            </Text>
          </View>
        );
      } else {
        return null;
      }
    }

    // if (productData && productData.length) {
    //   if (productData.notes !== "") {
    //     return (
    //       <View
    //         style={{
    //           flexDirection: "row",
    //           alignItems: "center",
    //           marginVertical: hp(h(15))
    //         }}
    //       >
    //         <Text
    //           style={{
    //             color: themes.red,
    //             fontSize: hp(h(11)),
    //             marginRight: wp(w(12))
    //           }}
    //         >
    //           NOTE
    //         </Text>
    //         <Text
    //           style={{
    //             color: themes.gray,
    //             fontSize: hp(h(13))
    //           }}
    //         >
    //           {productData.notes}
    //         </Text>
    //       </View>
    //     );
    //   } else {
    //     return null;
    //   }
    // } else {
    //   return null;
    // }
  };

  render() {
    return (
      <View
        style={{
          flex: 1
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: "row"
          }}
        >
          <View
            style={{
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                fontSize: hp(h(13)),
                paddingRight: wp(w(5))
              }}
            >
              {this.props.items.quantity}
            </Text>
            <Text
              style={{
                marginRight: wp(w(25))
              }}
            >
              X
            </Text>
          </View>
          <View>
            <Text
              style={{
                fontSize: hp(h(12)),
                fontWeight: "bold"
              }}
            >
              {this.props.items.name}
            </Text>
            <View
              style={{
                marginTop: hp(h(10))
              }}
            >
              {this._renderAdditionalItem(this.props.items)}
            </View>
          </View>
          <View
            style={{
              position: "absolute",
              right: 0
            }}
          >
            <Text
              style={{
                fontSize: hp(h(13)),
                fontWeight: "bold"
              }}
            >
              {this.props.items.price}
            </Text>
          </View>
        </View>

        {this._renderNotes(this.props.items)}

        {/* {(productData.notes.trim() !== '') && 
                    <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginVertical: hp(h(15))
                    }}>
                        <Text style={{
                                color: themes.red,
                                fontSize: hp(h(11)),
                                marginRight: wp(w(12))
                        }}>NOTE</Text>
                        <Text style={{
                                color: themes.gray,
                                fontSize: hp(h(13))
                        }}>{productData.notes}</Text>
                    </View>
                } */}
      </View>
    );
  }
}
