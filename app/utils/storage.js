/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-10-29 12:29:24 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-12-05 14:04:42
 */

import { AsyncStorage } from 'react-native'
import NavigationService  from '../services/navServices'

const USER_TOKEN = 'USER_TOKEN'

export const setData = async (key, value) => {
    try {
       await AsyncStorage.setItem(`@${key}:key`, `${value}`);
       return true;
    } catch (error){
        return false;
    }
}

export const getData = async (key) => {
    try {
       const value = await AsyncStorage.getItem(`@${key}:key`);
       if(value !== null) {
           return value;
       }
       return '';
    } catch(error){
        return '';
    }
}

export const removeData = async (key) => {
    try {
        await AsyncStorage.removeItem(`@${key}:key`);
        return true;
    } catch(error) {
        return false;
    }
}

export const saveToken = async (value) => {
    try {
       await AsyncStorage.setItem(`@${USER_TOKEN}:key`, `${value}`);
       return true;
    } catch(error){
       return false;
    }
}

export const clearToken = async () => {
    try{
        await saveToken('');
        return NavigationService.navigate('Login'); // redirect to login if token has been clear
    } catch (error) {
        return false;
    }
};

export const getToken = async () => {
    try {
        const value = await AsyncStorage.getItem(`@${USER_TOKEN}:key`);
        if(value !== null){
            return value;
        }
        return '';
    } catch(error){
       return '';
    }
}