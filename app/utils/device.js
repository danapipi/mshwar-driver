import {Dimensions, Platform} from 'react-native';

const HEADER_MAX_HEIGHT = 191;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 64 : 56;
const PLATFORM = Platform.OS;

const {
  width: viewportWidth,
  height: viewportHeight,
} = Dimensions.get('window');

const getViewWidth = (percentage) => {
  const value = percentage * viewportWidth;
  return Math.round(value);
};
const getViewHeight = (percentage) => {
  const value = percentage * viewportHeight;
  return Math.round(value);
};

module.exports = {
  getViewWidth,
  getViewHeight,
  viewportWidth,
  viewportHeight,
  getPlatform: PLATFORM,
  currentPlatform: Platform.OS,
  getHeaderMinHeight: HEADER_MIN_HEIGHT,
  getHeaderMaxHeight: HEADER_MAX_HEIGHT,
  getHeaderScrollDistance: HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT,
};
