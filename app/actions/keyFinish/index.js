/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-11-12 06:58:06
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-15 21:35:36
 */

import {
  SUBMIT_PIN_CUSTOMER,
  SUBMIT_PIN_CUSTOMER_SUCCESS,
  SUBMIT_PIN_CUSTOMER_FAILED,
  WATCH_POSITION,
  WATCH_POSITION_SUCCESS,
  WATCH_POSITION_FAILED
} from "../types";

export const submitPinCustomer = (params, onSuccess, onError) => ({
  type: SUBMIT_PIN_CUSTOMER,
  params,
  onSuccess,
  onError
});

export const submitPinCustomerSuccess = data => ({
  type: SUBMIT_PIN_CUSTOMER_SUCCESS,
  data
});

export const submitPinCustomerFailed = error => ({
  type: SUBMIT_PIN_CUSTOMER_FAILED,
  error
});

export const watchLocation = (params, onSuccess, onError) => ({
  type: WATCH_POSITION,
  params,
  onSuccess,
  onError
});

export const watchLocationSuccess = data => ({
  type: WATCH_POSITION_SUCCESS,
  data
});

export const watchLocationFailed = error => ({
  type: WATCH_POSITION_FAILED,
  error
});
