/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-07 16:26:27 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-16 11:02:30
 */

import { SWIPE_STATUS_ORDER, SWIPE_STATUS_ORDER_SUCCESS, SWIPE_STATUS_ORDER_FAILED,
            FETCH_ORDER_BY_ID,  SWIPE_STATUS_ORDER_UPDATE, CLICK_TO_CANCEL_ORDER } from '../types'

export const swipeStatusOrder = (params, onSuccess, onError) => ({ // to update state order
    type: SWIPE_STATUS_ORDER,
    params,
    onSuccess,
    onError
})

export const swipeStatusOrderSuccess = (data) => ({
    type: SWIPE_STATUS_ORDER_SUCCESS,
    data
})

export const swipeStatusOrderFailed = (error) => ({
    type: SWIPE_STATUS_ORDER_FAILED,
    error
})

export const swipeStatusOrderUpdate = (params) => ({
    type: SWIPE_STATUS_ORDER_UPDATE,
    params
})

export const fetchOrderbyId = (params, onSuccess, onError) => ({
    type: FETCH_ORDER_BY_ID,
    params,
    onSuccess,
    onError
})

export const clickToCancelOrder = (params, onSuccess, onError) => ({
    type: CLICK_TO_CANCEL_ORDER,
    params,
    onSuccess,
    onError
})