/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 21:23:53 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-20 06:37:24
 */

import { LOGIN, LOGIN_SUCCESS, LOGIN_FAILED, SET_USER_INFO, SET_USER_OTP, 
        FETCH_USER_INFO, FETCH_USER_INFO_SUCCESS, FETCH_USER_INFO_FAILED,
        SET_ACTIVE } from '../../types'

export const login = (params, onSuccess, onError) => ({
    type: LOGIN,
    params,
    onSuccess,
    onError
})

export const loginSuccess = (data) => ({
    type: LOGIN_SUCCESS,
    data
})

export const loginFailed = (error) => ({
    type: LOGIN_FAILED,
    error
})

export const setUserInfo = (data) => ({
    type: SET_USER_INFO,
    data
})

export const setUserOtp = (data) => ({
    type: SET_USER_OTP,
    data
})

export const fetchUserInfo = (params, onSuccess, onError) => ({
    type: FETCH_USER_INFO,
    params,
    onSuccess,
    onError
})

export const fetchUserInfoSuccess = (data) => ({
    type: FETCH_USER_INFO_SUCCESS,
    data
})

export const fetchUserInfoFailed = (error) => ({
    type: FETCH_USER_INFO_FAILED,
    error
})

export const setActive = (params, onSuccess, onError) => ({
    type: SET_ACTIVE,
    params,
    onSuccess,
    onError
})