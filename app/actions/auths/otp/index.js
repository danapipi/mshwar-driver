/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 21:23:57 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-05 21:26:44
 */

import { OTP } from '../../types'

export const otp = (params, onSuccess, onError) => ({
    type: OTP,
    params,
    onSuccess,
    onError
})