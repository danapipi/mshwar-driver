/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-11-12 06:58:19
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-29 16:41:00
 */

import {
  RECEIPT,
  RECEIPT_SUCCESS,
  RECEIPT_ERROR,
  FETCH_ORDER_BY_ID,
  SWIPE_STATUS_ORDER,
  SWIPE_STATUS_ORDER_UPDATE,
  WATCH_POSITION,
  WATCH_POSITION_SUCCESS,
  WATCH_POSITION_FAILED
} from "../types";

export const sendReceipt = (params, onSuccess, onError) => ({
  type: RECEIPT,
  params,
  onSuccess,
  onError
});

export const sendReceiptSuccess = (data) => ({
  type: RECEIPT_SUCCESS,
  data
});

export const sendReceiptError = (error) => ({
  type: RECEIPT_ERROR,
  error
});

export const swipeStatusOrder = (params, onSuccess, onError) => ({
  // to update state order
  type: SWIPE_STATUS_ORDER,
  params,
  onSuccess,
  onError
});

export const swipeStatusOrderUpdate = params => ({
  type: SWIPE_STATUS_ORDER_UPDATE,
  params
});

export const fetchOrderbyId = (params, onSuccess, onError) => ({
  type: FETCH_ORDER_BY_ID,
  params,
  onSuccess,
  onError
});

export const watchLocation = (params, onSuccess, onError) => ({
  type: WATCH_POSITION,
  params,
  onSuccess,
  onError
});

export const watchLocationSuccess = data => ({
  type: WATCH_POSITION_SUCCESS,
  data
});

export const watchLocationFailed = error => ({
  type: WATCH_POSITION_FAILED,
  error
});

