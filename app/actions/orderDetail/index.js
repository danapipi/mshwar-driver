/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-11-07 16:35:05
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-16 05:39:28
 */

import {
  SWIPE_TO_CANCEL,
  SWIPE_TO_CANCEL_SUCCESS,
  SWIPE_TO_CANCEL_FALIED,
  SELECTED_RADIO_CANCEL,
  FETCH_ORDER_BY_ID,
  SWIPE_STATUS_ORDER,
  SWIPE_STATUS_ORDER_UPDATE,
  CLICK_TO_CANCEL_ORDER,
  CLICK_TO_CANCEL_ORDER_SUCCESS,
  CLICK_TO_CANCEL_ORDER_FAILED,
  WATCH_POSITION,
  WATCH_POSITION_SUCCESS,
  WATCH_POSITION_FAILED
} from "../types";

export const swipeToCancel = params => ({
  type: SWIPE_TO_CANCEL,
  params
});

export const swipeToCancelSuccess = data => ({
  type: SWIPE_TO_CANCEL_SUCCESS,
  data
});

export const swipeToCancelFailed = error => ({
  type: SWIPE_TO_CANCEL_FALIED,
  error
});

export const selectedRadioCancel = data => ({
  type: SELECTED_RADIO_CANCEL,
  data
});

export const swipeStatusOrder = (params, onSuccess, onError) => ({
  // to update state order
  type: SWIPE_STATUS_ORDER,
  params,
  onSuccess,
  onError
});

export const swipeStatusOrderUpdate = params => ({
  type: SWIPE_STATUS_ORDER_UPDATE,
  params
});

export const fetchOrderbyId = (params, onSuccess, onError) => ({
  type: FETCH_ORDER_BY_ID,
  params,
  onSuccess,
  onError
});

export const clickToCancelOrder = (params, onSuccess, onError) => ({
  type: CLICK_TO_CANCEL_ORDER,
  params,
  onSuccess,
  onError
});

export const clickToCancelOrderSuccess = data => ({
  type: CLICK_TO_CANCEL_ORDER_SUCCESS,
  data
});

export const clickToCancelOrderFailed = error => ({
  type: CLICK_TO_CANCEL_ORDER_FAILED,
  error
});

export const watchLocation = (params, onSuccess, onError) => ({
  type: WATCH_POSITION,
  params,
  onSuccess,
  onError
});

export const watchLocationSuccess = data => ({
  type: WATCH_POSITION_SUCCESS,
  data
});

export const watchLocationFailed = error => ({
  type: WATCH_POSITION_FAILED,
  error
});
