/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-19 22:06:28 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-19 22:07:43
 */

import { SET_ACTIVE, SWIPE_STATUS_ORDER_UPDATE } from '../types'

export const setActive = (params, onSuccess, onError) => ({
    type: SET_ACTIVE,
    params,
    onSuccess,
    onError
})

export const swipeStatusOrderUpdate = (params) => ({
    type: SWIPE_STATUS_ORDER_UPDATE,
    params
})
