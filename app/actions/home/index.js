/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 21:53:57 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-12-05 14:40:02
 */

import { SET_ACTIVE, SET_ACTIVE_SUCCESS, SET_ACTIVE_FAILED,
        WATCH_POSITION, WATCH_POSITION_SUCCESS, WATCH_POSITION_FAILED,
        FETCH_ORDER_BY_ID, FETCH_ORDER_BY_ID_SUCCESS, FETCH_ORDER_BY_ID_FAILED,
        SWIPE_STATUS_ORDER_UPDATE } from '../types'

export const setActive = (params, onSuccess, onError) => ({
    type: SET_ACTIVE,
    params,
    onSuccess,
    onError
})

export const setActiveSuccess = (data) => ({
    type: SET_ACTIVE_SUCCESS,
    data
})

export const setActiveFailed = (error) => ({
    type: SET_ACTIVE_FAILED,
    error
})

export const watchLocation = (params, onSuccess, onError) => ({
    type: WATCH_POSITION,
    params,
    onSuccess,
    onError
})

export const watchLocationSuccess = (data) => ({
    type: WATCH_POSITION_SUCCESS,
    data
})

export const watchLocationFailed = (error) => ({
    type: WATCH_POSITION_FAILED,
    error
})

export const fetchOrderbyId = (params, onSuccess, onError) => ({
    type: FETCH_ORDER_BY_ID,
    params,
    onSuccess,
    onError
})

export const fetchOrderbyIdSuccess = (data) => ({
    type: FETCH_ORDER_BY_ID_SUCCESS,
    data
})

export const fetchOrderbyIdFailed = (error) => ({
    type: FETCH_ORDER_BY_ID_FAILED,
    error
})

export const swipeStatusOrderUpdate = (params) => ({
    type: SWIPE_STATUS_ORDER_UPDATE,
    params
})

