/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react'
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux'
import { store, persistor } from './config/redux'
import NavigationApp from './config/navigator'
import Nav from '../app/src/screens/profile'
import NavigationService from './services/navServices'


const App = () => {
  return (
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <NavigationApp 
                ref={navigatorRef => {
                  NavigationService.setTopLevelNavigator(navigatorRef);
                }}
            />
        </PersistGate>
    </Provider>
  )
}

export default App;