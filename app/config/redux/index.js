/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-10-29 12:26:54 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-05 21:30:03
 */

import { createStore, applyMiddleware } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' 
import createSagaMiddleware from 'redux-saga'
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension'

import allReducers from '../../reducers'
import rootSaga from '../../sagas/rootSaga';

const middlewares = [];
const sagaMiddleware = createSagaMiddleware();
const persistConfig = {
    key: 'root',
    storage,
}
const persistedReducer = persistReducer(persistConfig, allReducers)

middlewares.push(sagaMiddleware);
if (__DEV__) {
    middlewares.push(createLogger());
}

export const store = createStore(
        persistedReducer, 
        composeWithDevTools(applyMiddleware(...middlewares))
);
export const persistor = persistStore(store)

sagaMiddleware.run(rootSaga)