/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-10-29 12:26:01 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-19 20:10:46
 */

export const themes = {
    purple_70       : 'rgb(51, 0, 77)',
    purple_50       : 'rgba(102, 0, 153, 0.35)',
    purple_20       : 'rgb(240,230,245)',
    white           : 'rgb(255, 255, 255)',
    white_10        : 'rgb(249, 249, 249)',
    white_50        : 'rgba(155, 155, 155, 0.49)',
    mainColor       : 'rgb(102, 0, 153)',
    gray            : 'rgb(74, 74, 74)',
    gray_70         : 'rgba(74, 74, 74, 0.78)',
    gray_50         : 'rgb(155, 155, 155)',
    gray_30         : 'rgb(235, 235, 235)',
    blueSea         : 'rgb(74, 144, 226)',
    red             : 'rgb(255, 0, 51)',
    green           : 'rgb(0, 204, 0)',
    border          : 'rgb(240, 230, 245)',
    backdrop        : 'rgba(52, 52, 52, 0.8)',
    transparent     : 'transparent'
}