/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-07 10:32:22 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-07 14:51:50
 */

import React from 'react';
import { PixelRatio, Dimensions, Platform } from 'react-native';

/* Width and Height for Nexus 5X */
const width = 411;
const height = 683;

export const percentageWidth = (originalSize) => {
  return originalSize*100/width;
}

export const percentageHeight = (originalSize) => {
 return originalSize*100/height;
}

export const isIphoneX = () => {
  const { height, width } = Dimensions.get('window');
  return (
    Platform.OS === 'ios' && (height === 812 || width === 812)
  )
 }


