/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-10-29 12:30:27 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-19 22:10:48
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  Easing,
  TouchableOpacity
} from "react-native";
import { createStackNavigator } from "react-navigation";
import HomeContainer from "../../containers/home";
import LoginContainer from "../../containers/auths/login";
import OtpContainer from "../../containers/auths/otp";
import homeTab from "../../src/screens/home/homeTab";
import OrderContainer from "../../containers/orders";
import OrderDetailContainer from "../../containers/orderDetail";
import CameraScreen from "../../containers/receiptOrder";
// import CameraScreen from "../../src/screens/photoOrder";
import FinishOrderContainer from '../../containers/finishOrder'
import KeyFinishOrderContainer from '../../containers/keyFinish'
import SplashContainer from '../../containers/splash'
import ProfileContainer from '../../containers/profile'
// import DetailContainer from '../../containers/detail'

const stack = createStackNavigator({
  Splash: {
    screen: SplashContainer,
    navigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  },
  Login: {
    screen: LoginContainer,
    navigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  },
  Otp: {
    screen: OtpContainer,
    navigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  },
  Home: {
    screen: HomeContainer,
    navigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  },
  HomeTab: {
    screen: homeTab,
    navigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  },
  Order: {
    screen: OrderContainer,
    navigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  },
  OrderDetail: {
    screen: OrderDetailContainer,
    navigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  },
  Camera: {
    screen: CameraScreen,
    navigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  },
  FinishOrder: {
    screen: FinishOrderContainer,
    navigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  },
  KeyCustomer: {
    screen: KeyFinishOrderContainer,
    navigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  },
  Profile: {
    screen: ProfileContainer,
    navigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  },
});

const NavigationApp = stack;

export default NavigationApp;
