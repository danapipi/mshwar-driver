import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  Easing,
  TouchableOpacity
} from "react-native";
import { createBottomTabNavigator, StackNavigator } from "react-navigation";

import HelpNav from "./HelpNav";
import HomeNav from "./HomeNav";
import PerformanceNav from "./PerformanceNav";

const bottomTab = createBottomTabNavigator(
  {
    Home: {
      screen: HomeNav,
      navigationOptions: ({ navigation, screenProps }) => {
        return {
          tabBarLabel: ({ focused }) => {
            return (
              <Text
                style={[
                  style.tabBarLabel,
                  focused ? style.tabBarLabelActive : {}
                ]}
              >
                Home
              </Text>
            );
          },
          tabBarOptions: {
            style: {
              backgroundColor: "#660099"
            }
          },
          tabBarIcon: ({ focused, tintColor }) => {
            // return (
            //   <Image source={focused ? require('../../assets/img/icons/png/footer/white/home.png') : require('../../assets/img/icons/png/footer/gray/home.png')} />
            // )
          },
          tabBarVisible: navigation.state.index > 0 ? false : true
        };
      }
    },
    Performance: {
      screen: PerformanceNav,
      navigationOptions: ({ navigation, screenProps }) => {
        return {
          tabBarLabel: ({ focused }) => {
            return (
              <Text
                style={[
                  style.tabBarLabel,
                  focused ? style.tabBarLabelActive : {}
                ]}
              >
                Orders
              </Text>
            );
          },
          tabBarOptions: {
            style: {
              backgroundColor: "#660099"
            }
          },
          tabBarIcon: ({ focused, tintColor }) => {
            // return (
            //   <Image source={focused ? require('../../assets/img/icons/png/footer/white/orders.png') : require('../../assets/img/icons/png/footer/gray/orders.png')} />
            // )
          },
          tabBarVisible: navigation.state.index > 0 ? false : true
        };
      }
    },
    Help: {
      screen: HelpNav,
      navigationOptions: {
        tabBarLabel: ({ focused }) => {
          return (
            <Text
              style={[
                style.tabBarLabel,
                focused ? style.tabBarLabelActive : {}
              ]}
            >
              Help
            </Text>
          );
        },
        tabBarOptions: {
          style: {
            backgroundColor: "#660099"
          }
        },
        tabBarIcon: ({ focused, tintColor }) => {
          //   return (
          //     <Image source={focused ? require('../../assets/img/icons/png/footer/white/help.png') : require('../../assets/img/icons/png/footer/gray/help.png')} />
          //   )
        }
      }
    }
  },
  {
    initialRouteName: "Home"
    // navigationOptions: {
    //     headerStyle: {
    //       backgroundColor: '#f4511e',
    //     },
    //     headerTintColor: '#fff',
    //     headerTitleStyle: {
    //       fontWeight: 'bold',
    //     },
    //   },
  }
);

export default bottomTab;
