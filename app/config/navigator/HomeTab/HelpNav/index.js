import React, { Component } from 'react';
import {
   Platform,
   StyleSheet,
   Text,
   View} from 'react-native';
import { StackNavigator } from 'react-navigation';

import HelpTab from '../../../../src/screens/home/helpTab';


const HelpNav = StackNavigator({
 Helps: {
   screen: HelpTab,
   navigationOptions: ({ navigation }) => {
     return {
       header: null
     }
   }
 }
}, {
   // headerMode: 'none'
 });

export default HelpNav;
