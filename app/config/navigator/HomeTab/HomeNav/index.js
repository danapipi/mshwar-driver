import React, { Component } from 'react';
import {
   Platform,
   StyleSheet,
   Text,
   View} from 'react-native';
import { StackNavigator } from 'react-navigation';

import HomeTab from '../../../../src/screens/home/homeTab';


const HomeNav = StackNavigator({
 Helps: {
   screen: HomeTab,
   navigationOptions: ({ navigation }) => {
     return {
       header: null
     }
   }
 }
}, {
   // headerMode: 'none'
 });

export default HomeNav;
