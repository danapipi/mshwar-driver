import React, { Component } from 'react';
import {
   Platform,
   StyleSheet,
   Text,
   View} from 'react-native';
import { StackNavigator } from 'react-navigation';

import PerformanceTab from '../../../../src/screens/home/performanceTab';


const PerformanceNav = StackNavigator({
 Helps: {
   screen: PerformanceTab,
   navigationOptions: ({ navigation }) => {
     return {
       header: null
     }
   }
 }
}, {
   // headerMode: 'none'
 });

export default PerformanceNav;
