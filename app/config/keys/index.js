/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-10-29 12:26:33 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-20 22:15:56
 */

export const keys = {
    // googleApi: 'AIzaSyBz654UprldOvg2JxZ4d-rpwFtl7A9Ysqg',
    googleApi: 'AIzaSyBT4j6z1eJn_uVhkxtYC45gqMQ9LSz0nno',   // for production
    userInfoStorage: 'SET_USER_INFO',
    activeOrder: 'Act1v30rd3r_DrIvEr'
}