/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-10-29 12:24:41 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-29 11:23:52
 */

const env = {
    dev        : 'dev',
    stage      : 'stage',
    production : 'production',
    local      : 'local'
}

const API_URL = {
       dev        : '',
       stage      : 'https://admin.mshwarapp.com/api/driver',
       production : '',
       local      : ''
}

const ROOT_API_URL = {
    dev        : '',
    stage      : 'https://admin.mshwarapp.com/api',
    production : '',
    local      : ''
}

const API_MAPS_URL = {
   dev        : '',
   stage      : 'https://maps.googleapis.com/maps/api',
   production : '',
   local      : ''
}

const currentEnv = env.stage;

export const BASE_API_URL = API_URL[currentEnv];
export const BASE_ROOT_API_URL = ROOT_API_URL[currentEnv];
export const BASE_API_MAPS_URL = API_MAPS_URL[currentEnv];
export const USER_TOKEN = 'USER_TOKEN';
export const STATUS_ORDER = {
    new         : 'new',
    asign       : 'asign',
    accept      : 'accepted',
    ready       : 'ready',
    delivering  : 'delivering',
    delivered   : 'delivered',
    finish      : 'finish',
    cancel      : 'cancel'
};