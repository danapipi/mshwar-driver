/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-07 16:27:17 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-15 20:30:48
 */

import * as types from '../../actions/types'
import appState from '../../constants/initialState'

const ordersReducer = (state = appState.orders, action) => {
    switch (action.type){
        case types.SWIPE_STATUS_ORDER:
            return {
                ...state,
                error: '',
            } 
        case types.SWIPE_STATUS_ORDER_SUCCESS:
            return {
                ...state,
                success: true,
                error: '',
                data: action.data,
            }
        case types.SWIPE_STATUS_ORDER_FAILED:
            return {
                ...state,
                success: false,
                error: action.error,
                data: {},
        }
        case types.SWIPE_STATUS_ORDER_UPDATE:
            return {
                ...state,
                valueSliderAccept: action.params
        } 
        default:
            return state;
    }
}

export default ordersReducer;