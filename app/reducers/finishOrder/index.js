/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-12 05:48:25 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-12 05:49:21
 */

import * as types from '../../actions/types'
import appState from '../../constants/initialState'

const finishOrderReducer = (state = appState.finishOrder, action) => {
    switch (action.type){  
        default:
            return state;
    }
}

export default finishOrderReducer;