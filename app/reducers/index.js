/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-10-29 12:28:09 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-12 07:04:23
 */

import { combineReducers } from "redux"
import homeReducers from '../reducers/home'
import loginReducers from '../reducers/auths/login'
import ordersReducers from '../reducers/orders'
import orderDetailReducers from '../reducers/orderDetail'
import finishOrderReducer from '../reducers/finishOrder'
import receiptReducer from '../reducers/receiptOrder'
import keyFinishOrderReducer from '../reducers/keyFinish'

const allReducers = combineReducers({
    home            : homeReducers,
    login           : loginReducers,
    order           : ordersReducers,
    orderDetail     : orderDetailReducers,
    finishOrder     : finishOrderReducer,
    receipt         : receiptReducer,
    keyFinishOrder  : keyFinishOrderReducer
});

export default allReducers;