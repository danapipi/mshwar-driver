/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-11-05 21:45:40
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-29 17:03:01
 */

import * as types from "../../actions/types";
import appState from "../../constants/initialState";

const receiptReducer = (state = appState.receipt, action) => {
  switch (action.type) {
    case types.RECEIPT:
      return { 
        ...state, 
        loading: true
      };
    case types.RECEIPT_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        data: action.data 
      };
    case types.RECEIPT_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error,
        data: {}
      };
    default:
      return state;
  }
};

export default receiptReducer;
