/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-10-29 12:45:08 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-18 22:52:54
 */

import * as types from '../../actions/types'
import appState from '../../constants/initialState'

const homeReducer = (state = appState.home, action) => {
    switch (action.type){
        case types.SET_ACTIVE:
            return {
                ...state,
                loading: true,
                error: '',
                successStatusDriver: false
            } 
        case types.SET_ACTIVE_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                successStatusDriver: true,
                // data: action.data, //set to call set_user_info to update state userInfo
            }
        case types.SET_ACTIVE_FAILED:
            return {
                ...state,
                loading: false,
                error: action.error,
                successStatusDriver: false
            }
        case types.WATCH_POSITION:
            return {
                ...state,
                loadingLocation: true,
                errorLocation: '',
            } 
        case types.WATCH_POSITION_SUCCESS:
            return {
                ...state,
                loadingLocation: false,
                errorLocation: '',
                location: {
                    'lat'   : action.data.lat,
                    'lng'   : action.data.lng
                },
                activeOrder: action.data.active_order // comment this line for testing
                // activeOrder: 2 // for testing
            }
        case types.WATCH_POSITION_FAILED:
            return {
                ...state,
                loadingLocation: false,
                errorLocation: action.error,
            }
        case types.FETCH_ORDER_BY_ID:
            return {
                ...state,
                errorFetchOrder: '',
            }     
        case types.FETCH_ORDER_BY_ID_SUCCESS:
            return {
                ...state,
                errorFetchOrder: '',
                order: action.data
            }
        case types.FETCH_ORDER_BY_ID_FAILED:
            return {
                ...state,
                errorFetchOrder: action.error,
            }                 
        default:
            return state;
    }
}

export default homeReducer;
