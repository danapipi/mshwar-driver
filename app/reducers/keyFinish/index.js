/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-12 07:03:00 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-15 21:54:01
 */

import * as types from '../../actions/types'
import appState from '../../constants/initialState'

const keyFinishOrderReducer = (state = appState.keyFinish, action) => {
    switch (action.type){  
        case types.SUBMIT_PIN_CUSTOMER:
            return {
                ...state,
                error: '',
                loading: true,
            } 
        case types.SUBMIT_PIN_CUSTOMER_SUCCESS:
            return {
                ...state,
                error: '',
                data: action.data,
                loading: false,
            }
        case types.SUBMIT_PIN_CUSTOMER_FAILED:
            return {
                ...state,
                error: action.error,
                data: {},
                loading: false,
        }
        default:
            return state;
    }
}

export default keyFinishOrderReducer;