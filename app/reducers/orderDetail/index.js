/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-07 16:35:27 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-16 06:00:34
 */

import * as types from '../../actions/types'
import appState from '../../constants/initialState'

const orderDetailReducer = (state = appState.orderDetail, action) => {
    switch (action.type){
        case types.SWIPE_TO_CANCEL:
            return {
                ...state,
                loading: true,
                error: '',
                dataCancel: {},
            } 
        case types.SWIPE_TO_CANCEL_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                dataCancel: action.data,
            }
        case types.SWIPE_TO_CANCEL_FALIED:
            return {
                ...state,
                loading: false,
                error: action.error,
                dataCancel: {},
            }
        case types.SELECTED_RADIO_CANCEL:
            return {
                ...state,
                selectedRadio: action.data,
        }
        case types.CLICK_TO_CANCEL_ORDER:
            return {
                ...state,
                error: '',
            } 
        case types.CLICK_TO_CANCEL_ORDER_SUCCESS:
            return {
                ...state,
                // success: true,
                error: '',
                dataCancel: action.data,
            }
        case types.CLICK_TO_CANCEL_ORDER_FAILED:
            return {
                ...state,
                // success: false,
                error: action.error,
                dataCancel: {},
        }     
        default:
            return state;
    }
}

export default orderDetailReducer;