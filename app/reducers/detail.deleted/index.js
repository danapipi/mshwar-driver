/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-08 16:11:28 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-08 16:14:06
 */

import * as types from '../../actions/types'
import appState from '../../constants/initialState'

const detailReducer = (state = appState.detail, action) => {
    switch (action.type){
        default:
            return state;
    }
}

export default detailReducer;