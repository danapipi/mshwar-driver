/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 21:45:40 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-19 05:08:48
 */

import * as types from '../../../actions/types'
import appState from '../../../constants/initialState'
import { setData, getData } from '../../../utils/storage'
import { keys } from '../../../config/keys'

const loginReducer = (state = appState.login, action) => {
    switch (action.type){
        case types.LOGIN:
            return {
                ...state,
                loading: true,
                error: '',
            } 
        case types.LOGIN_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
            }
        case types.LOGIN_FAILED:
            return {
                ...state,
                loading: false,
                error: action.error,
        }
        case types.SET_USER_INFO:
            // console.log('SET_USER_INFO', JSON.stringify(action.data));
            setData(keys.userInfoStorage, JSON.stringify(action.data))
            return { ...state, ...{userInfo: action.data}}
        case types.SET_USER_OTP:
            return { ...state, ...action.data}
        default:
            return state;
    }
}

export default loginReducer;