import { put, call, fork, takeLatest, takeEvery, select } from "redux-saga/effects";
import * as types from "../../actions/types";
import * as postRequest from "../../services/postRequest";
import { sendReceiptError, sendReceiptSuccess } from "../../actions/receiptOrder";


export function* sendReceipt(action) {
    try {
        const data = yield call(postRequest.receipt, action.params)
        yield put(sendReceiptSuccess(data.data.data))
        action.onSuccess(data.data.data)
    } catch (error) {
        yield put(sendReceiptError(error))
        action.onError(error)
    }
}

export function* watchSetReceipt() {
    yield takeLatest(types.RECEIPT, sendReceipt);
}
