/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-09 17:33:27 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-12-05 14:40:35
 */

import { put, call, fork, takeLatest, takeEvery } from "redux-saga/effects";
import * as types from "../../actions/types";
import * as postRequest from "../../services/postRequest";
import * as getRequest from "../../services/getRequest";
import { setActiveSuccess, setActiveFailed, 
        watchLocationSuccess, watchLocationFailed,
        fetchOrderbyIdSuccess, fetchOrderbyIdFailed } from "../../actions/home";
import { setUserInfo } from '../../actions/auths/login'   ;     
import { setToken } from "../../services/api";
import { getData, setData } from '../../utils/storage'
import { keys } from '../../config/keys'

export function* setActive(action) {
    try {
        const data = yield call(postRequest.setActive, action.params);
        action.onSuccess(data.data);
        yield put(setActiveSuccess(data.data));
        yield put(setUserInfo(data.data));
    } catch (error) {       
        action.onError(error);
        yield put(setActiveFailed(error));
    }
}

export function* setLocation(action) {
    try {
        const data = yield call(postRequest.setLocation, action.params);
        
        action.onSuccess(data.data)
        yield put(watchLocationSuccess(data.data));

        // move to screen
        // // const active_order = data.data.active_order; // comment this line for testing
        // const active_order = 2; // for testing purpose
        // if(active_order != null) {
        //     try{
        //         const order = yield call(postRequest.fetchOrderById, active_order);

        //         yield put(fetchOrderbyIdSuccess(order.data))
        //         yield setData(keys.activeOrder, active_order)
        //     } catch(err) {
        //         yield put(fetchOrderbyIdFailed(err))
        //     }
        // }
        
    } catch (error) {
        action.onError(error)
        yield put(watchLocationFailed(error))
    }
}

export function* fetchOrderById(action) {
    try {
        const order = yield call(getRequest.fetchOrderById, action.params);
        
        action.onSuccess(order.data)
        yield put(fetchOrderbyIdSuccess(order.data))
        
    } catch (error) {
        action.onError(error)
        yield put(fetchOrderbyIdFailed(error))
    }
}

export function* watchSetActive() {
    yield takeLatest(types.SET_ACTIVE, setActive);
}

export function* watchLocation() {
    yield takeLatest(types.WATCH_POSITION, setLocation);
}

export function* watchFetchOrderById() {
    yield takeLatest(types.FETCH_ORDER_BY_ID, fetchOrderById);
}