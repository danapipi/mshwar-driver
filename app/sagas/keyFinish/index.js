/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-15 21:40:24 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-15 21:45:29
 */

import { put, call, fork, takeLatest, takeEvery } from "redux-saga/effects";
import * as types from "../../actions/types";
import * as postRequest from "../../services/postRequest";
import { submitPinCustomerSuccess, submitPinCustomerFailed } from "../../actions/keyFinish";


export function* actionSubmitPinCustomer(action) {
    try {
        const data = yield call(postRequest.checkPinCustomer, action.params);
        action.onSuccess(data.data)
        yield put(submitPinCustomerSuccess(data.data));
        
    } catch (error) {
        action.onError(error)
        yield put(submitPinCustomerFailed(error))
    }
}

export function* watchSubmitPinCustomer() {
    yield takeLatest(types.SUBMIT_PIN_CUSTOMER, actionSubmitPinCustomer);
}