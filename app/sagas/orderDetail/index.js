/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-11 19:51:26 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-16 06:01:00
 */

import { put, call, fork, takeLatest, takeEvery } from "redux-saga/effects";
import * as types from "../../actions/types";
import * as postRequest from "../../services/postRequest";
import { swipeToCancelSuccess, swipeToCancelFailed,
        clickToCancelOrderSuccess, clickToCancelOrderFailed } from "../../actions/orderDetail";


export function* swipeToCancel(action) {
    try {
        const data = yield call(postRequest.cancelOrder, action.params);
        
        yield put(swipeToCancelSuccess(data.data));
        
    } catch (error) {
        yield put(swipeToCancelFailed(error))
    }
}

export function* watchSwipeToCancel() {
    yield takeLatest(types.SWIPE_TO_CANCEL, swipeToCancel);
}

export function* actionClickToCancelOrder(action) {
    try {
        const data = yield call(postRequest.cancelOrder, action.params);
        yield put(clickToCancelOrderSuccess(data.data));
        action.onSuccess(data.data);
    } catch (error) {
        yield put(clickToCancelOrderFailed(error));
        action.onError(error);
    }
}

export function* watchClickToCancelOrder() {
    yield takeLatest(types.CLICK_TO_CANCEL_ORDER, actionClickToCancelOrder);
}