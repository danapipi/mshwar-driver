/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-15 12:40:20 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-15 20:30:50
 */

import { put, call, fork, takeLatest, takeEvery } from "redux-saga/effects";
import * as types from "../../actions/types";
import * as postRequest from "../../services/postRequest";
import { swipeStatusOrderSuccess, swipeStatusOrderFailed } from "../../actions/orders";


export function* actionSwipeStatusOrder(action) {
    try {
        const data = yield call(postRequest.setStatusOrder, action.params);
        yield put(swipeStatusOrderSuccess(data.data));
        action.onSuccess(data.data);
    } catch (error) {
        yield put(swipeStatusOrderFailed(error));
        action.onError(error);
    }
}

export function* watchSwipeStatusOrder() {
    yield takeLatest(types.SWIPE_STATUS_ORDER, actionSwipeStatusOrder);
}