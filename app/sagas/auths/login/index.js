/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 17:15:18 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-19 05:11:27
 */

import { put, call, fork, takeLatest, takeEvery } from "redux-saga/effects";
import * as types from "../../../actions/types";
import * as postRequest from "../../../services/postRequest";
import * as getRequest from "../../../services/getRequest";
import { loginSuccess, loginFailed, setUserInfo, setUserOtp, 
        fetchUserInfoSuccess, fetchUserInfoFailed } from "../../../actions/auths/login";
import { setToken } from "../../../services/api";
import { getData } from '../../../utils/storage'
import { keys } from '../../../config/keys'

export function* login(action) {
  try { 
    const data = yield call(postRequest.login, action.params);
    // yield put(setUserOtp(action.params));
    action.onSuccess(data);
    if(data.data.success) {
        if(data.status === 200) {
            setToken(data.data.token || "");
            yield put(loginSuccess(data.data.driver));
            yield put(setUserInfo(data.data.driver));
        } else if(data.status === 201) {
          yield put(setUserOtp(action.params));
        }
        
    } 
    
  } catch (error) {
    yield put(loginFailed(error));
    action.onError(error);
  }
}

export function* watchLogin() {
  yield takeLatest(types.LOGIN, login);
}

export function* fetchUserInfo(action) {
  console.log('action : ', action)
  try {
    const data = yield call(getRequest.fetchUserInfo, action.params);
    action.onSuccess(data);
    yield put(fetchUserInfoSuccess(data.data));
    yield put(setUserInfo(data.data));
  } catch (error) {
    action.onError(error);
    yield put(fetchUserInfoFailed(error));
  }
  // try{
  //   getData(keys.userInfoStorage)
  //     .then((res) => {
  //       const result = JSON.parse(res);
  //       // console.log('fetchUserInfo', action)
  //       if(result) {
  //         action.onSuccess(result);
  //       }
  //     })
  // } catch(error){
  //   alert('asasas')
  //   action.onError(error);
  // }
}

export function* watchFetchUserInfo() {
  yield takeLatest(types.FETCH_USER_INFO, fetchUserInfo)
}