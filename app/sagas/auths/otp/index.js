/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 17:15:15 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-05 22:24:25
 */

import { put, call, fork, takeLatest } from "redux-saga/effects";
import * as types from "../../../actions/types";
import * as postRequest from "../../../services/postRequest";
import { setUserInfo, setUserOtp } from "../../../actions/auths/login";
import { setToken } from "../../../services/api";

export function* otp(action) {
    
  try { 
    const data = yield call(postRequest.otp, action.params);
    
    action.onSuccess(data);
    if (data.data.success) {
      setToken(data.data.token || "");
      yield put(setUserInfo(data.data.driver));
      yield put(setUserOtp({})); // reset email and password
    }
  } catch (error) {
    action.onError(error);
  }
}

export function* watchOtp() {
  yield takeLatest(types.OTP, otp);
}