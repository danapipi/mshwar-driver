/*
 * @Author: Iwan Susanto
 * @Email: iwandevapps@gmail.com
 * @Project: MSHWAR
 * @Date: 2018-10-29 12:28:42
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-12-05 14:40:44
 */

import { all } from 'redux-saga/effects'
import { watchLogin, watchFetchUserInfo } from './auths/login'
import { watchOtp } from './auths/otp'
import { watchSetActive, watchLocation, watchFetchOrderById } from './home'
import { watchSwipeToCancel, watchClickToCancelOrder } from './orderDetail'
import { watchSetReceipt } from "./receiptOrder"
import { watchSwipeStatusOrder } from './orders'
import { watchSubmitPinCustomer } from './keyFinish'

function* rootSaga(){
    yield all([
        watchLogin(),
        watchFetchUserInfo(),
        watchOtp(),
        watchSetActive(),
        watchLocation(),
        watchFetchOrderById(),
        watchSwipeToCancel(),
        watchSetReceipt(),
        watchSwipeStatusOrder(),
        watchSubmitPinCustomer(),
        watchClickToCancelOrder()
    ])
}

export default rootSaga;
