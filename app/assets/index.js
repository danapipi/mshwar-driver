// Background
import background from "./background/Rectangle_Copy.png";

// Logo
import logo from "./images/LandingPage/Logo/logo-higres.png";

// Help white
import helpWhite from "./images/HomeGotOrder/HelpWhite/help_white_icon.png";

// Help white
import closeWhite from "./images/HomeGotOrder/CloseWhite/close_white.png";

// Cancel login
import cancelLogin from "./images/LandingPage/Cancel/ic-cancel-48px.png";

// Driver photo
import photoDriver from "./images/LandingPage/ProfilePic/unnamed.png";

// Help button
import helpBtn from "./images/MshwarHome/HelpInactive/HelpInactive.png";

// Performance button
import performanceBtn from "./images/MshwarHome/PerformanceInactive/PerformanceInactive.png";

// Home button
import homeBtn from "./images//MshwarHome/Home/home.png";

// Home Purple
import home from "./images/HomeGotOrder/PurpleHome/homepurple.png";

//  Pin
import pin from "./images/MshwarHome/Pin/pin.png";

// Resto symbol
import restoSymbol from "./images/HomeGotOrder/RestaurantSymbol/restaurantsymbol.png";

// Resto symbol purple
import restoPurple from "./images/HomeGotOrder/RestaurantSymbolMed/restaurantsymbolmed.png";

// Line
import line from "./images/HomeGotOrder/Line/Line.png";

// Location
import location from "./images/HomeGotOrder/LocationMedPurple/locationmed.png";

// Swipe arrow
import swipeArrow from "./images/HomeGotOrder/SwipeArrow/swipearrow.png";

// Customer background
import customerBackground from "./images/HomeGotOrder/CustomerBackground/customerbackgroundbanner.png";

// Phone
import phone from "./images/HomeGotOrder/Phone/phone.png";

// Message
import message from "./images/HomeGotOrder/Message/message.png";

// Arrow up
import arrowUp from "./images/HomeGotOrder/ArrowUpSwipe/arrowupswipe.png";

// Arrow down
import arrowDown from "./images/HomeGotOrder/DownArrowGrey/downarrowgrey.png";

// Cancel
import cancel from "./images/HomeGotOrder/Cancel/cancel.png";

// Delayed
import delay from "./images/HomeGotOrder/Delayed/delayed.png";

// QR input
import qrInput from "./images/HomeGotOrder/QRInput/qrinput.png";

// Upload receipt
import uploadReceipt from "./images/HomeGotOrder/UploadReceiptButton/UploadReceiptButton.png";

// Camera button
import cameraBtn from "./images/HomeGotOrder/CameraBigPurpleButton/camera.png";

// Swipe inactive
import swipeInActive from "./images/HomeGotOrder/OvalGreySymbol/oval_grey_symbol.png";

// Swipe active
import swipeActive from "./images/HomeGotOrder/ToggleGreenButton/green_toggle.png";

// Close Grey
import close from "./images/HomeGotOrder/CloseGrey/close.png";

// Reload
import retake from "./images/HomeGotOrder/Retake/retake.png";
// Swipe cancel
import swipeCancel from "./images/HomeGotOrder/SwipeToCancel/swipe_to_cancel.png";

// No image
import noImage from './images/HomeGotOrder/NoImage/Shape.png'

// Search magnifier purple
import searchPurple from './images/HomeGotOrder/SearchMagnifyingIcon/searchmagnifyingicon.png'

export default {
  background,
  logo,
  helpWhite,
  closeWhite,
  cancelLogin,
  photoDriver,
  helpBtn,
  performanceBtn,
  homeBtn,
  pin,
  restoSymbol,
  line,
  location,
  swipeArrow,
  customerBackground,
  phone,
  message,
  arrowUp,
  restoPurple,
  arrowDown,
  cancel,
  delay,
  qrInput,
  uploadReceipt,
  cameraBtn,
  swipeInActive,
  swipeActive,
  close,
  retake,
  swipeCancel,
  home,
  noImage,
  searchPurple
};
