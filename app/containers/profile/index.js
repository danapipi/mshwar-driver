/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-19 22:08:08 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-19 22:08:52
 */

import ProfileScreen from '../../src/screens/profile'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as profileActions from '../../actions/profile'

const mapStateToProps = ({ login, home, order }) => {
    const { userInfo } = login;
    const { data } = home;
    const { valueSliderAccept } = order;
    return { userInfo, data, valueSliderAccept }  
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(profileActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)