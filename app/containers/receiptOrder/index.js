import CameraScreen from "../../src/screens/photoOrder";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as receiptActions from "../../actions/receiptOrder";

const mapStateToProps = ({ receipt, login, home, order }) => {
  const { userInfo } = login;
  const { data, loading } = receipt;
  const { valueSliderAccept } = order;
  const { location } = home;

  return { data, loading, userInfo, orders: home.order, valueSliderAccept, location }
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(receiptActions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CameraScreen);
