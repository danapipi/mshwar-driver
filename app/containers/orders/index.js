/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-07 16:25:27 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-22 17:29:24
 */

import OrdersScreen from '../../src/screens/orders'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as ordersActions from '../../actions/orders'

const mapStateToProps = ({ login, home, order }) => {
    const { userInfo } = login;
    const { valueSliderAccept } = order;
    const { location } = home;
    return { userInfo, orders: home.order, valueSliderAccept, location }  
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(ordersActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrdersScreen)