/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-12 06:58:48 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-15 21:36:50
 */


import KeyFinishOrderScreen from '../../src/screens/keyFinish'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as keyFinishOrderActions from '../../actions/keyFinish'

const mapStateToProps = ({ login, home }) => {
    const { userInfo } = login;
    const { location } = home;

    return { userInfo, orders: home.order, location }  
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(keyFinishOrderActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(KeyFinishOrderScreen)