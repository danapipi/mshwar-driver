/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 21:54:51 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-18 22:52:49
 */


import HomeScreen from '../../src/screens/home'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as homeActions from '../../actions/home'

const mapStateToProps = ({ login, home, order }) => {
    const { userInfo } = login;
    const { data } = home;
    const { valueSliderAccept } = order;
    return { userInfo, data, valueSliderAccept }  
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(homeActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)