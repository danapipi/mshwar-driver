/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-12 05:44:22 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-17 13:29:07
 */

import FinishOrderScreen from '../../src/screens/finishOrder'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as finishOrderActions from '../../actions/finishOrder'

const mapStateToProps = ({ login, home, order }) => {
    const { userInfo } = login;
    // const { order } = home;
    const { valueSliderAccept } = order;
    const { location } = home;
    return { userInfo, orders: home.order, valueSliderAccept, location }  
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(finishOrderActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FinishOrderScreen)