/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 21:35:38 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-05 21:43:54
 */

import OtpScreen from '../../../src/screens/auths/otp'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as otpActions from '../../../actions/auths/otp'

const mapStateToProps = ({login}) => {
    const { email, password } = login;
    return { email, password }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(otpActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OtpScreen)