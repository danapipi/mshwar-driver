/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 21:35:35 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-05 22:55:05
 */

import LoginScreen from '../../../src/screens/auths/login'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as loginActions from '../../../actions/auths/login'

const mapStateToProps = (state) => {
    return {
        
    }       
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(loginActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)