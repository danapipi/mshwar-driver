/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-07 16:37:17 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-15 20:59:43
 */

import OrderDetailScreen from '../../src/screens/orderDetail'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as orderDetailActions from '../../actions/orderDetail'

const mapStateToProps = ({ login, home, orderDetail, order }) => {
    const { userInfo } = login;
    // const { order } = home;
    
    const { selectedRadio, dataCancel } = orderDetail;
    const { valueSliderAccept } = order;
    const { location } = home;
    // alert(JSON.stringify(location))
    return { userInfo, orders: home.order, selectedRadio, 
            dataCancel, valueSliderAccept,  location}  
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(orderDetailActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetailScreen)