/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-06 16:12:30 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-11-16 06:53:49
 */


import SplashScreen from '../../src/screens/splash' 
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as loginActions from '../../actions/auths/login'

const mapStateToProps = ({ login, home }) => {
    const { userInfo } = login;
    return { userInfo, orders: home.order }  
}

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(loginActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen)