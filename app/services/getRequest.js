/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 17:21:08 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-12-05 14:43:53
 */

import { api, rootApi } from './api'

export const fetchOrderById = (params) =>{
    return api.get('/order/'+params);
}

export const fetchUserInfo = (params) => {
    console.log('Param : ', params);
    return api.get('/info');
}