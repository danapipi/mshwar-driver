/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 17:21:04 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-12-05 14:46:17
 */

// import { api, rootApi, apiFormData, setHeaderFormData } from './api'
import { api, rootApi, setHeaderFormData } from './api'

export const login = (params) => { 
    return api.post('/login', params);
}

export const otp = (params) => {
    return api.post('/login', params);
}

export const setActive = (params) => {
    return api.post('/status', params);
}

export const setLocation = (params) => {
    return api.post('/location', params);
}

export const receipt = (params) => {
    setHeaderFormData();
    return api.post('/receipt', params);
}

export const cancelOrder = (params) =>{
    console.log('params cancelOrder : ', params)
    return rootApi.post('/order/cancel', params);
}

export const setStatusOrder = (params) => {
    return rootApi.post('/order/status', params);
}

export const checkPinCustomer = (params) => {
    return api.post('/pin', params);
}