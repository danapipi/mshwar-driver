/*
 * @Author: Iwan Susanto 
 * @Email: iwandevapps@gmail.com 
 * @Project: MSHWAR 
 * @Date: 2018-11-05 17:15:21 
 * @Last Modified by: Iwan
 * @Last Modified time: 2018-12-05 14:46:45
 */

import axios from 'axios'
import { getToken, clearToken } from '../utils/storage'
import { BASE_API_URL, BASE_API_MAPS_URL, BASE_ROOT_API_URL } from '../config'

export const api = axios.create({
    baseURL: BASE_API_URL,
    timeout: 12000,
    headers: {
        'Content-Type' :   'application/json'
    }
});

export const rootApi = axios.create({
    baseURL: BASE_ROOT_API_URL,
    timeout: 12000,
    headers: {
        'Content-Type' :   'application/json'
    }
});

api.interceptors.response.use(function(response) {
    return response;
}, function(error) {
    if(error.response.status === 401) {
        return clearToken();
    };
    return Promise.reject(error);
})

rootApi.interceptors.response.use(function(response) {
    return response;
}, function(error) {
    if(error.response.status === 401) {
        return clearToken();
    };
    return Promise.reject(error);
})

export function setToken(token) {
    api.defaults.headers.common.Authorization = `Bearer ${token}`;
    api.defaults.headers.common.token = `${token}`;

    rootApi.defaults.headers.common.Authorization = `Bearer ${token}`;
    rootApi.defaults.headers.common.token = `${token}`;
}

export function setHeaderFormData() {
    api.defaults.headers.common['Content-Type'] = 'multipart/form-data';
    rootApi.defaults.headers.common['Content-Type'] = 'multipart/form-data';
}

getToken().then((token) => {
    setToken(token);
})